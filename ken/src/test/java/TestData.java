
import com.ken.sys.common.entity.CalendarData;
import com.ken.sys.common.entity.CalendarMain;
import com.ken.sys.common.entity.KData;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * <ul>
 * <li>Title: TestData</li>
 * <li>Description: TODO </li>
 * <li>Copyright: Copyright (c) 2018</li>
 * </ul>
 *
 * @author swc
 * @date 2020/7/2 0002 上午 11:08
 */
public class TestData {

    //区域仓储数据
    public final static String areaStorage="[{\n" +
            "\t\"id\": \"8a0af66a6ac47e4f016ad94c37700e81\",\n" +
            "\t\"warehouse_id\": \"8a0a854b63d2b8bf0163d2c89d2b0009\",\n" +
            "\t\"area_code\": \"c01\",\n" +
            "\t\"area_type\": \"c\",\n" +
            "\t\"area_name\": \"普通货架区_搁板式货架区1\",\n" +
            "\t\"max_row\": 6,\n" +
            "\t\"max_col\": 4,\n" +
            "\t\"max_layer\": 4,\n" +
            "\t\"area_length\": null,\n" +
            "\t\"area_width\": null,\n" +
            "\t\"coordinate_x\": null,\n" +
            "\t\"coordinate_y\": null,\n" +
            "\t\"stock_in_location\": null,\n" +
            "\t\"stock_out_location\": null,\n" +
            "\t\"pick_location\": null,\n" +
            "\t\"storages\": [{\n" +
            "\t\t\"id\": \"8a0af66a6ac47e4f016ad95ae7750f65\",\n" +
            "\t\t\"code\": \"c01-010101\",\n" +
            "\t\t\"area_type_id\": \"8a0af66a6ac47e4f016ad94c37700e81\",\n" +
            "\t\t\"stock_area\": 9,\n" +
            "\t\t\"length\": 999,\n" +
            "\t\t\"width\": 999,\n" +
            "\t\t\"height\": 999,\n" +
            "\t\t\"total_capaicty\": 999,\n" +
            "\t\t\"cur_capacity\": 0,\n" +
            "\t\t\"capaicty_unit\": 1,\n" +
            "\t\t\"state\": \"1\",\n" +
            "\t\t\"is_froze\": 0,\n" +
            "\t\t\"warehouse_id\": \"8a0a854b63d2b8bf0163d2c89d2b0009\",\n" +
            "\t\t\"deep_pos_type\": null,\n" +
            "\t\t\"opposite_position\": null,\n" +
            "\t\t\"container_id\": null,\n" +
            "\t\t\"row\": \"01\",\n" +
            "\t\t\"col\": \"01\",\n" +
            "\t\t\"layer\": \"01\"\n" +
            "\t}, {\n" +
            "\t\t\"id\": \"8a0af66a6ac47e4f016ad95ae7940f66\",\n" +
            "\t\t\"code\": \"c01-010102\",\n" +
            "\t\t\"area_type_id\": \"8a0af66a6ac47e4f016ad94c37700e81\",\n" +
            "\t\t\"stock_area\": 9,\n" +
            "\t\t\"length\": 999,\n" +
            "\t\t\"width\": 999,\n" +
            "\t\t\"height\": 999,\n" +
            "\t\t\"total_capaicty\": 999,\n" +
            "\t\t\"cur_capacity\": 0,\n" +
            "\t\t\"capaicty_unit\": 1,\n" +
            "\t\t\"state\": \"1\",\n" +
            "\t\t\"is_froze\": 0,\n" +
            "\t\t\"warehouse_id\": \"8a0a854b63d2b8bf0163d2c89d2b0009\",\n" +
            "\t\t\"deep_pos_type\": null,\n" +
            "\t\t\"opposite_position\": null,\n" +
            "\t\t\"container_id\": null,\n" +
            "\t\t\"row\": \"01\",\n" +
            "\t\t\"col\": \"01\",\n" +
            "\t\t\"layer\": \"02\"\n" +
            "\t}, {\n" +
            "\t\t\"id\": \"8a0af66a6ac47e4f016ad95ae7b40f67\",\n" +
            "\t\t\"code\": \"c01-010103\",\n" +
            "\t\t\"area_type_id\": \"8a0af66a6ac47e4f016ad94c37700e81\",\n" +
            "\t\t\"stock_area\": 9,\n" +
            "\t\t\"length\": 999,\n" +
            "\t\t\"width\": 999,\n" +
            "\t\t\"height\": 999,\n" +
            "\t\t\"total_capaicty\": 999,\n" +
            "\t\t\"cur_capacity\": 0,\n" +
            "\t\t\"capaicty_unit\": 1,\n" +
            "\t\t\"state\": \"1\",\n" +
            "\t\t\"is_froze\": 0,\n" +
            "\t\t\"warehouse_id\": \"8a0a854b63d2b8bf0163d2c89d2b0009\",\n" +
            "\t\t\"deep_pos_type\": null,\n" +
            "\t\t\"opposite_position\": null,\n" +
            "\t\t\"container_id\": null,\n" +
            "\t\t\"row\": \"01\",\n" +
            "\t\t\"col\": \"01\",\n" +
            "\t\t\"layer\": \"03\"\n" +
            "\t}, {\n" +
            "\t\t\"id\": \"8a0af66a6ac47e4f016ad95ae7d30f68\",\n" +
            "\t\t\"code\": \"c01-010104\",\n" +
            "\t\t\"area_type_id\": \"8a0af66a6ac47e4f016ad94c37700e81\",\n" +
            "\t\t\"stock_area\": 9,\n" +
            "\t\t\"length\": 999,\n" +
            "\t\t\"width\": 999,\n" +
            "\t\t\"height\": 999,\n" +
            "\t\t\"total_capaicty\": 999,\n" +
            "\t\t\"cur_capacity\": 5,\n" +
            "\t\t\"capaicty_unit\": 1,\n" +
            "\t\t\"state\": \"2\",\n" +
            "\t\t\"is_froze\": 0,\n" +
            "\t\t\"warehouse_id\": \"8a0a854b63d2b8bf0163d2c89d2b0009\",\n" +
            "\t\t\"deep_pos_type\": null,\n" +
            "\t\t\"opposite_position\": null,\n" +
            "\t\t\"container_id\": null,\n" +
            "\t\t\"row\": \"01\",\n" +
            "\t\t\"col\": \"01\",\n" +
            "\t\t\"layer\": \"04\",\n" +
            "\t\t\"materiel_no\": \"500022148\",\n" +
            "\t\t\"materiel_name\": \"光缆接头盒,opgw光缆用,48\"\n" +
            "\t}, {\n" +
            "\t\t\"id\": \"8a0af66a6ac47e4f016ad95ae7d30f68\",\n" +
            "\t\t\"code\": \"c01-010104\",\n" +
            "\t\t\"area_type_id\": \"8a0af66a6ac47e4f016ad94c37700e81\",\n" +
            "\t\t\"stock_area\": 9,\n" +
            "\t\t\"length\": 999,\n" +
            "\t\t\"width\": 999,\n" +
            "\t\t\"height\": 999,\n" +
            "\t\t\"total_capaicty\": 999,\n" +
            "\t\t\"cur_capacity\": 5,\n" +
            "\t\t\"capaicty_unit\": 1,\n" +
            "\t\t\"state\": \"2\",\n" +
            "\t\t\"is_froze\": 0,\n" +
            "\t\t\"warehouse_id\": \"8a0a854b63d2b8bf0163d2c89d2b0009\",\n" +
            "\t\t\"deep_pos_type\": null,\n" +
            "\t\t\"opposite_position\": null,\n" +
            "\t\t\"container_id\": null,\n" +
            "\t\t\"row\": \"01\",\n" +
            "\t\t\"col\": \"01\",\n" +
            "\t\t\"layer\": \"04\",\n" +
            "\t\t\"materiel_no\": \"500022148\",\n" +
            "\t\t\"materiel_name\": \"光缆接头盒,opgw光缆用,48\"\n" +
            "\t}, {\n" +
            "\t\t\"id\": \"8a0af66a6ac47e4f016ad95ae7d30f68\",\n" +
            "\t\t\"code\": \"c01-010104\",\n" +
            "\t\t\"area_type_id\": \"8a0af66a6ac47e4f016ad94c37700e81\",\n" +
            "\t\t\"stock_area\": 9,\n" +
            "\t\t\"length\": 999,\n" +
            "\t\t\"width\": 999,\n" +
            "\t\t\"height\": 999,\n" +
            "\t\t\"total_capaicty\": 999,\n" +
            "\t\t\"cur_capacity\": 5,\n" +
            "\t\t\"capaicty_unit\": 1,\n" +
            "\t\t\"state\": \"2\",\n" +
            "\t\t\"is_froze\": 0,\n" +
            "\t\t\"warehouse_id\": \"8a0a854b63d2b8bf0163d2c89d2b0009\",\n" +
            "\t\t\"deep_pos_type\": null,\n" +
            "\t\t\"opposite_position\": null,\n" +
            "\t\t\"container_id\": null,\n" +
            "\t\t\"row\": \"01\",\n" +
            "\t\t\"col\": \"01\",\n" +
            "\t\t\"layer\": \"04\",\n" +
            "\t\t\"materiel_no\": \"500022148\",\n" +
            "\t\t\"materiel_name\": \"光缆接头盒,opgw光缆用,48\"\n" +
            "\t}, {\n" +
            "\t\t\"id\": \"8a0af66a6ac47e4f016ad95ae7d30f68\",\n" +
            "\t\t\"code\": \"c01-010104\",\n" +
            "\t\t\"area_type_id\": \"8a0af66a6ac47e4f016ad94c37700e81\",\n" +
            "\t\t\"stock_area\": 9,\n" +
            "\t\t\"length\": 999,\n" +
            "\t\t\"width\": 999,\n" +
            "\t\t\"height\": 999,\n" +
            "\t\t\"total_capaicty\": 999,\n" +
            "\t\t\"cur_capacity\": 5,\n" +
            "\t\t\"capaicty_unit\": 1,\n" +
            "\t\t\"state\": \"2\",\n" +
            "\t\t\"is_froze\": 0,\n" +
            "\t\t\"warehouse_id\": \"8a0a854b63d2b8bf0163d2c89d2b0009\",\n" +
            "\t\t\"deep_pos_type\": null,\n" +
            "\t\t\"opposite_position\": null,\n" +
            "\t\t\"container_id\": null,\n" +
            "\t\t\"row\": \"01\",\n" +
            "\t\t\"col\": \"01\",\n" +
            "\t\t\"layer\": \"04\",\n" +
            "\t\t\"materiel_no\": \"500022148\",\n" +
            "\t\t\"materiel_name\": \"光缆接头盒,opgw光缆用,48\"\n" +
            "\t}, {\n" +
            "\t\t\"id\": \"8a0af66a6ac47e4f016ad95ae7d30f68\",\n" +
            "\t\t\"code\": \"c01-010104\",\n" +
            "\t\t\"area_type_id\": \"8a0af66a6ac47e4f016ad94c37700e81\",\n" +
            "\t\t\"stock_area\": 9,\n" +
            "\t\t\"length\": 999,\n" +
            "\t\t\"width\": 999,\n" +
            "\t\t\"height\": 999,\n" +
            "\t\t\"total_capaicty\": 999,\n" +
            "\t\t\"cur_capacity\": 5,\n" +
            "\t\t\"capaicty_unit\": 1,\n" +
            "\t\t\"state\": \"2\",\n" +
            "\t\t\"is_froze\": 0,\n" +
            "\t\t\"warehouse_id\": \"8a0a854b63d2b8bf0163d2c89d2b0009\",\n" +
            "\t\t\"deep_pos_type\": null,\n" +
            "\t\t\"opposite_position\": null,\n" +
            "\t\t\"container_id\": null,\n" +
            "\t\t\"row\": \"01\",\n" +
            "\t\t\"col\": \"01\",\n" +
            "\t\t\"layer\": \"04\",\n" +
            "\t\t\"materiel_no\": \"500022148\",\n" +
            "\t\t\"materiel_name\": \"光缆接头盒,opgw光缆用,48\"\n" +
            "\t}, {\n" +
            "\t\t\"id\": \"8a0af66a6ac47e4f016ad95ae8020f69\",\n" +
            "\t\t\"code\": \"c01-010105\",\n" +
            "\t\t\"area_type_id\": \"8a0af66a6ac47e4f016ad94c37700e81\",\n" +
            "\t\t\"stock_area\": 9,\n" +
            "\t\t\"length\": 999,\n" +
            "\t\t\"width\": 999,\n" +
            "\t\t\"height\": 999,\n" +
            "\t\t\"total_capaicty\": 999,\n" +
            "\t\t\"cur_capacity\": 0,\n" +
            "\t\t\"capaicty_unit\": 1,\n" +
            "\t\t\"state\": \"1\",\n" +
            "\t\t\"is_froze\": 0,\n" +
            "\t\t\"warehouse_id\": \"8a0a854b63d2b8bf0163d2c89d2b0009\",\n" +
            "\t\t\"deep_pos_type\": null,\n" +
            "\t\t\"opposite_position\": null,\n" +
            "\t\t\"container_id\": null,\n" +
            "\t\t\"row\": \"01\",\n" +
            "\t\t\"col\": \"01\",\n" +
            "\t\t\"layer\": \"05\"\n" +
            "\t}, {\n" +
            "\t\t\"id\": \"8a0af66a6ac47e4f016ad95ae8210f6a\",\n" +
            "\t\t\"code\": \"c01-010106\",\n" +
            "\t\t\"area_type_id\": \"8a0af66a6ac47e4f016ad94c37700e81\",\n" +
            "\t\t\"stock_area\": 9,\n" +
            "\t\t\"length\": 999,\n" +
            "\t\t\"width\": 999,\n" +
            "\t\t\"height\": 999,\n" +
            "\t\t\"total_capaicty\": 999,\n" +
            "\t\t\"cur_capacity\": 0,\n" +
            "\t\t\"capaicty_unit\": 1,\n" +
            "\t\t\"state\": \"1\",\n" +
            "\t\t\"is_froze\": 0,\n" +
            "\t\t\"warehouse_id\": \"8a0a854b63d2b8bf0163d2c89d2b0009\",\n" +
            "\t\t\"deep_pos_type\": null,\n" +
            "\t\t\"opposite_position\": null,\n" +
            "\t\t\"container_id\": null,\n" +
            "\t\t\"row\": \"01\",\n" +
            "\t\t\"col\": \"01\",\n" +
            "\t\t\"layer\": \"06\"\n" +
            "\t}, {\n" +
            "\t\t\"id\": \"8a0af66a6ac47e4f016ad95ae8400f6b\",\n" +
            "\t\t\"code\": \"c01-010107\",\n" +
            "\t\t\"area_type_id\": \"8a0af66a6ac47e4f016ad94c37700e81\",\n" +
            "\t\t\"stock_area\": 9,\n" +
            "\t\t\"length\": 999,\n" +
            "\t\t\"width\": 999,\n" +
            "\t\t\"height\": 999,\n" +
            "\t\t\"total_capaicty\": 999,\n" +
            "\t\t\"cur_capacity\": 0,\n" +
            "\t\t\"capaicty_unit\": 1,\n" +
            "\t\t\"state\": \"1\",\n" +
            "\t\t\"is_froze\": 0,\n" +
            "\t\t\"warehouse_id\": \"8a0a854b63d2b8bf0163d2c89d2b0009\",\n" +
            "\t\t\"deep_pos_type\": null,\n" +
            "\t\t\"opposite_position\": null,\n" +
            "\t\t\"container_id\": null,\n" +
            "\t\t\"row\": \"01\",\n" +
            "\t\t\"col\": \"01\",\n" +
            "\t\t\"layer\": \"07\"\n" +
            "\t}, {\n" +
            "\t\t\"id\": \"8a0af66a6ac47e4f016ad95ae85f0f6c\",\n" +
            "\t\t\"code\": \"c01-010108\",\n" +
            "\t\t\"area_type_id\": \"8a0af66a6ac47e4f016ad94c37700e81\",\n" +
            "\t\t\"stock_area\": 9,\n" +
            "\t\t\"length\": 999,\n" +
            "\t\t\"width\": 999,\n" +
            "\t\t\"height\": 999,\n" +
            "\t\t\"total_capaicty\": 999,\n" +
            "\t\t\"cur_capacity\": 0,\n" +
            "\t\t\"capaicty_unit\": 1,\n" +
            "\t\t\"state\": \"1\",\n" +
            "\t\t\"is_froze\": 0,\n" +
            "\t\t\"warehouse_id\": \"8a0a854b63d2b8bf0163d2c89d2b0009\",\n" +
            "\t\t\"deep_pos_type\": null,\n" +
            "\t\t\"opposite_position\": null,\n" +
            "\t\t\"container_id\": null,\n" +
            "\t\t\"row\": \"01\",\n" +
            "\t\t\"col\": \"01\",\n" +
            "\t\t\"layer\": \"08\"\n" +
            "\t}, {\n" +
            "\t\t\"id\": \"8a0af66a6ac47e4f016ad95ae87f0f6d\",\n" +
            "\t\t\"code\": \"c01-010109\",\n" +
            "\t\t\"area_type_id\": \"8a0af66a6ac47e4f016ad94c37700e81\",\n" +
            "\t\t\"stock_area\": 9,\n" +
            "\t\t\"length\": 999,\n" +
            "\t\t\"width\": 999,\n" +
            "\t\t\"height\": 999,\n" +
            "\t\t\"total_capaicty\": 999,\n" +
            "\t\t\"cur_capacity\": 0,\n" +
            "\t\t\"capaicty_unit\": 1,\n" +
            "\t\t\"state\": \"1\",\n" +
            "\t\t\"is_froze\": 0,\n" +
            "\t\t\"warehouse_id\": \"8a0a854b63d2b8bf0163d2c89d2b0009\",\n" +
            "\t\t\"deep_pos_type\": null,\n" +
            "\t\t\"opposite_position\": null,\n" +
            "\t\t\"container_id\": null,\n" +
            "\t\t\"row\": \"01\",\n" +
            "\t\t\"col\": \"01\",\n" +
            "\t\t\"layer\": \"09\"\n" +
            "\t}, {\n" +
            "\t\t\"id\": \"8a0af66a6ac47e4f016ad95ae8ae0f6e\",\n" +
            "\t\t\"code\": \"c01-010110\",\n" +
            "\t\t\"area_type_id\": \"8a0af66a6ac47e4f016ad94c37700e81\",\n" +
            "\t\t\"stock_area\": 9,\n" +
            "\t\t\"length\": 999,\n" +
            "\t\t\"width\": 999,\n" +
            "\t\t\"height\": 999,\n" +
            "\t\t\"total_capaicty\": 999,\n" +
            "\t\t\"cur_capacity\": 0,\n" +
            "\t\t\"capaicty_unit\": 1,\n" +
            "\t\t\"state\": \"1\",\n" +
            "\t\t\"is_froze\": 0,\n" +
            "\t\t\"warehouse_id\": \"8a0a854b63d2b8bf0163d2c89d2b0009\",\n" +
            "\t\t\"deep_pos_type\": null,\n" +
            "\t\t\"opposite_position\": null,\n" +
            "\t\t\"container_id\": null,\n" +
            "\t\t\"row\": \"01\",\n" +
            "\t\t\"col\": \"01\",\n" +
            "\t\t\"layer\": \"10\"\n" +
            "\t}, {\n" +
            "\t\t\"id\": \"8a0af66a6ac47e4f016ad95ae8cd0f6f\",\n" +
            "\t\t\"code\": \"c01-010111\",\n" +
            "\t\t\"area_type_id\": \"8a0af66a6ac47e4f016ad94c37700e81\",\n" +
            "\t\t\"stock_area\": 9,\n" +
            "\t\t\"length\": 999,\n" +
            "\t\t\"width\": 999,\n" +
            "\t\t\"height\": 999,\n" +
            "\t\t\"total_capaicty\": 999,\n" +
            "\t\t\"cur_capacity\": 0,\n" +
            "\t\t\"capaicty_unit\": 1,\n" +
            "\t\t\"state\": \"1\",\n" +
            "\t\t\"is_froze\": 0,\n" +
            "\t\t\"warehouse_id\": \"8a0a854b63d2b8bf0163d2c89d2b0009\",\n" +
            "\t\t\"deep_pos_type\": null,\n" +
            "\t\t\"opposite_position\": null,\n" +
            "\t\t\"container_id\": null,\n" +
            "\t\t\"row\": \"01\",\n" +
            "\t\t\"col\": \"01\",\n" +
            "\t\t\"layer\": \"11\"\n" +
            "\t}, {\n" +
            "\t\t\"id\": \"8a0af66a6ac47e4f016ad95ae8fc0f70\",\n" +
            "\t\t\"code\": \"c01-010112\",\n" +
            "\t\t\"area_type_id\": \"8a0af66a6ac47e4f016ad94c37700e81\",\n" +
            "\t\t\"stock_area\": 9,\n" +
            "\t\t\"length\": 999,\n" +
            "\t\t\"width\": 999,\n" +
            "\t\t\"height\": 999,\n" +
            "\t\t\"total_capaicty\": 999,\n" +
            "\t\t\"cur_capacity\": 0,\n" +
            "\t\t\"capaicty_unit\": 1,\n" +
            "\t\t\"state\": \"1\",\n" +
            "\t\t\"is_froze\": 0,\n" +
            "\t\t\"warehouse_id\": \"8a0a854b63d2b8bf0163d2c89d2b0009\",\n" +
            "\t\t\"deep_pos_type\": null,\n" +
            "\t\t\"opposite_position\": null,\n" +
            "\t\t\"container_id\": null,\n" +
            "\t\t\"row\": \"01\",\n" +
            "\t\t\"col\": \"01\",\n" +
            "\t\t\"layer\": \"12\"\n" +
            "\t}, {\n" +
            "\t\t\"id\": \"8a0af66a6ac47e4f016ad95ae92b0f71\",\n" +
            "\t\t\"code\": \"c01-010113\",\n" +
            "\t\t\"area_type_id\": \"8a0af66a6ac47e4f016ad94c37700e81\",\n" +
            "\t\t\"stock_area\": 9,\n" +
            "\t\t\"length\": 999,\n" +
            "\t\t\"width\": 999,\n" +
            "\t\t\"height\": 999,\n" +
            "\t\t\"total_capaicty\": 999,\n" +
            "\t\t\"cur_capacity\": 0,\n" +
            "\t\t\"capaicty_unit\": 1,\n" +
            "\t\t\"state\": \"1\",\n" +
            "\t\t\"is_froze\": 0,\n" +
            "\t\t\"warehouse_id\": \"8a0a854b63d2b8bf0163d2c89d2b0009\",\n" +
            "\t\t\"deep_pos_type\": null,\n" +
            "\t\t\"opposite_position\": null,\n" +
            "\t\t\"container_id\": null,\n" +
            "\t\t\"row\": \"01\",\n" +
            "\t\t\"col\": \"01\",\n" +
            "\t\t\"layer\": \"13\"\n" +
            "\t}, {\n" +
            "\t\t\"id\": \"8a0af66a6ac47e4f016ad95ae94a0f72\",\n" +
            "\t\t\"code\": \"c01-010114\",\n" +
            "\t\t\"area_type_id\": \"8a0af66a6ac47e4f016ad94c37700e81\",\n" +
            "\t\t\"stock_area\": 9,\n" +
            "\t\t\"length\": 999,\n" +
            "\t\t\"width\": 999,\n" +
            "\t\t\"height\": 999,\n" +
            "\t\t\"total_capaicty\": 999,\n" +
            "\t\t\"cur_capacity\": 0,\n" +
            "\t\t\"capaicty_unit\": 1,\n" +
            "\t\t\"state\": \"1\",\n" +
            "\t\t\"is_froze\": 0,\n" +
            "\t\t\"warehouse_id\": \"8a0a854b63d2b8bf0163d2c89d2b0009\",\n" +
            "\t\t\"deep_pos_type\": null,\n" +
            "\t\t\"opposite_position\": null,\n" +
            "\t\t\"container_id\": null,\n" +
            "\t\t\"row\": \"01\",\n" +
            "\t\t\"col\": \"01\",\n" +
            "\t\t\"layer\": \"14\"\n" +
            "\t}, {\n" +
            "\t\t\"id\": \"8a0af66a6ac47e4f016ad95ae9790f73\",\n" +
            "\t\t\"code\": \"c01-010115\",\n" +
            "\t\t\"area_type_id\": \"8a0af66a6ac47e4f016ad94c37700e81\",\n" +
            "\t\t\"stock_area\": 9,\n" +
            "\t\t\"length\": 999,\n" +
            "\t\t\"width\": 999,\n" +
            "\t\t\"height\": 999,\n" +
            "\t\t\"total_capaicty\": 999,\n" +
            "\t\t\"cur_capacity\": 0,\n" +
            "\t\t\"capaicty_unit\": 1,\n" +
            "\t\t\"state\": \"1\",\n" +
            "\t\t\"is_froze\": 0,\n" +
            "\t\t\"warehouse_id\": \"8a0a854b63d2b8bf0163d2c89d2b0009\",\n" +
            "\t\t\"deep_pos_type\": null,\n" +
            "\t\t\"opposite_position\": null,\n" +
            "\t\t\"container_id\": null,\n" +
            "\t\t\"row\": \"01\",\n" +
            "\t\t\"col\": \"01\",\n" +
            "\t\t\"layer\": \"15\"\n" +
            "\t}, {\n" +
            "\t\t\"id\": \"8a0af66a6ac47e4f016ad95ae9980f74\",\n" +
            "\t\t\"code\": \"c01-010116\",\n" +
            "\t\t\"area_type_id\": \"8a0af66a6ac47e4f016ad94c37700e81\",\n" +
            "\t\t\"stock_area\": 9,\n" +
            "\t\t\"length\": 999,\n" +
            "\t\t\"width\": 999,\n" +
            "\t\t\"height\": 999,\n" +
            "\t\t\"total_capaicty\": 999,\n" +
            "\t\t\"cur_capacity\": 0,\n" +
            "\t\t\"capaicty_unit\": 1,\n" +
            "\t\t\"state\": \"1\",\n" +
            "\t\t\"is_froze\": 0,\n" +
            "\t\t\"warehouse_id\": \"8a0a854b63d2b8bf0163d2c89d2b0009\",\n" +
            "\t\t\"deep_pos_type\": null,\n" +
            "\t\t\"opposite_position\": null,\n" +
            "\t\t\"container_id\": null,\n" +
            "\t\t\"row\": \"01\",\n" +
            "\t\t\"col\": \"01\",\n" +
            "\t\t\"layer\": \"16\"\n" +
            "\t}, {\n" +
            "\t\t\"id\": \"8a0af66a6ac47e4f016ad95ae9b70f75\",\n" +
            "\t\t\"code\": \"c01-010117\",\n" +
            "\t\t\"area_type_id\": \"8a0af66a6ac47e4f016ad94c37700e81\",\n" +
            "\t\t\"stock_area\": 9,\n" +
            "\t\t\"length\": 999,\n" +
            "\t\t\"width\": 999,\n" +
            "\t\t\"height\": 999,\n" +
            "\t\t\"total_capaicty\": 999,\n" +
            "\t\t\"cur_capacity\": 0,\n" +
            "\t\t\"capaicty_unit\": 1,\n" +
            "\t\t\"state\": \"1\",\n" +
            "\t\t\"is_froze\": 0,\n" +
            "\t\t\"warehouse_id\": \"8a0a854b63d2b8bf0163d2c89d2b0009\",\n" +
            "\t\t\"deep_pos_type\": null,\n" +
            "\t\t\"opposite_position\": null,\n" +
            "\t\t\"container_id\": null,\n" +
            "\t\t\"row\": \"01\",\n" +
            "\t\t\"col\": \"01\",\n" +
            "\t\t\"layer\": \"17\"\n" +
            "\t}, {\n" +
            "\t\t\"id\": \"8a0af66a6ac47e4f016ad95ae9e60f76\",\n" +
            "\t\t\"code\": \"c01-010118\",\n" +
            "\t\t\"area_type_id\": \"8a0af66a6ac47e4f016ad94c37700e81\",\n" +
            "\t\t\"stock_area\": 9,\n" +
            "\t\t\"length\": 999,\n" +
            "\t\t\"width\": 999,\n" +
            "\t\t\"height\": 999,\n" +
            "\t\t\"total_capaicty\": 999,\n" +
            "\t\t\"cur_capacity\": 0,\n" +
            "\t\t\"capaicty_unit\": 1,\n" +
            "\t\t\"state\": \"1\",\n" +
            "\t\t\"is_froze\": 0,\n" +
            "\t\t\"warehouse_id\": \"8a0a854b63d2b8bf0163d2c89d2b0009\",\n" +
            "\t\t\"deep_pos_type\": null,\n" +
            "\t\t\"opposite_position\": null,\n" +
            "\t\t\"container_id\": null,\n" +
            "\t\t\"row\": \"01\",\n" +
            "\t\t\"col\": \"01\",\n" +
            "\t\t\"layer\": \"18\"\n" +
            "\t}, {\n" +
            "\t\t\"id\": \"8a0af66a6ac47e4f016ad95aea050f77\",\n" +
            "\t\t\"code\": \"c01-010119\",\n" +
            "\t\t\"area_type_id\": \"8a0af66a6ac47e4f016ad94c37700e81\",\n" +
            "\t\t\"stock_area\": 9,\n" +
            "\t\t\"length\": 999,\n" +
            "\t\t\"width\": 999,\n" +
            "\t\t\"height\": 999,\n" +
            "\t\t\"total_capaicty\": 999,\n" +
            "\t\t\"cur_capacity\": 0,\n" +
            "\t\t\"capaicty_unit\": 1,\n" +
            "\t\t\"state\": \"1\",\n" +
            "\t\t\"is_froze\": 0,\n" +
            "\t\t\"warehouse_id\": \"8a0a854b63d2b8bf0163d2c89d2b0009\",\n" +
            "\t\t\"deep_pos_type\": null,\n" +
            "\t\t\"opposite_position\": null,\n" +
            "\t\t\"container_id\": null,\n" +
            "\t\t\"row\": \"01\",\n" +
            "\t\t\"col\": \"01\",\n" +
            "\t\t\"layer\": \"19\"\n" +
            "\t}, {\n" +
            "\t\t\"id\": \"8a0af66a6ac47e4f016ad95aea250f78\",\n" +
            "\t\t\"code\": \"c01-010120\",\n" +
            "\t\t\"area_type_id\": \"8a0af66a6ac47e4f016ad94c37700e81\",\n" +
            "\t\t\"stock_area\": 9,\n" +
            "\t\t\"length\": 999,\n" +
            "\t\t\"width\": 999,\n" +
            "\t\t\"height\": 999,\n" +
            "\t\t\"total_capaicty\": 999,\n" +
            "\t\t\"cur_capacity\": 0,\n" +
            "\t\t\"capaicty_unit\": 1,\n" +
            "\t\t\"state\": \"1\",\n" +
            "\t\t\"is_froze\": 0,\n" +
            "\t\t\"warehouse_id\": \"8a0a854b63d2b8bf0163d2c89d2b0009\",\n" +
            "\t\t\"deep_pos_type\": null,\n" +
            "\t\t\"opposite_position\": null,\n" +
            "\t\t\"container_id\": null,\n" +
            "\t\t\"row\": \"01\",\n" +
            "\t\t\"col\": \"01\",\n" +
            "\t\t\"layer\": \"20\"\n" +
            "\t}, {\n" +
            "\t\t\"id\": \"8a0af66a6ac47e4f016ad95aea630f79\",\n" +
            "\t\t\"code\": \"c01-010201\",\n" +
            "\t\t\"area_type_id\": \"8a0af66a6ac47e4f016ad94c37700e81\",\n" +
            "\t\t\"stock_area\": 9,\n" +
            "\t\t\"length\": 999,\n" +
            "\t\t\"width\": 999,\n" +
            "\t\t\"height\": 999,\n" +
            "\t\t\"total_capaicty\": 999,\n" +
            "\t\t\"cur_capacity\": 0,\n" +
            "\t\t\"capaicty_unit\": 1,\n" +
            "\t\t\"state\": \"1\",\n" +
            "\t\t\"is_froze\": 0,\n" +
            "\t\t\"warehouse_id\": \"8a0a854b63d2b8bf0163d2c89d2b0009\",\n" +
            "\t\t\"deep_pos_type\": null,\n" +
            "\t\t\"opposite_position\": null,\n" +
            "\t\t\"container_id\": null,\n" +
            "\t\t\"row\": \"01\",\n" +
            "\t\t\"col\": \"02\",\n" +
            "\t\t\"layer\": \"01\"\n" +
            "\t}, {\n" +
            "\t\t\"id\": \"8a0af66a6ac47e4f016ad95aea820f7a\",\n" +
            "\t\t\"code\": \"c01-010202\",\n" +
            "\t\t\"area_type_id\": \"8a0af66a6ac47e4f016ad94c37700e81\",\n" +
            "\t\t\"stock_area\": 9,\n" +
            "\t\t\"length\": 999,\n" +
            "\t\t\"width\": 999,\n" +
            "\t\t\"height\": 999,\n" +
            "\t\t\"total_capaicty\": 999,\n" +
            "\t\t\"cur_capacity\": 0,\n" +
            "\t\t\"capaicty_unit\": 1,\n" +
            "\t\t\"state\": \"1\",\n" +
            "\t\t\"is_froze\": 0,\n" +
            "\t\t\"warehouse_id\": \"8a0a854b63d2b8bf0163d2c89d2b0009\",\n" +
            "\t\t\"deep_pos_type\": null,\n" +
            "\t\t\"opposite_position\": null,\n" +
            "\t\t\"container_id\": null,\n" +
            "\t\t\"row\": \"01\",\n" +
            "\t\t\"col\": \"02\",\n" +
            "\t\t\"layer\": \"02\"\n" +
            "\t}, {\n" +
            "\t\t\"id\": \"8a0af66a6ac47e4f016ad95aead00f7b\",\n" +
            "\t\t\"code\": \"c01-010203\",\n" +
            "\t\t\"area_type_id\": \"8a0af66a6ac47e4f016ad94c37700e81\",\n" +
            "\t\t\"stock_area\": 9,\n" +
            "\t\t\"length\": 999,\n" +
            "\t\t\"width\": 999,\n" +
            "\t\t\"height\": 999,\n" +
            "\t\t\"total_capaicty\": 999,\n" +
            "\t\t\"cur_capacity\": 0,\n" +
            "\t\t\"capaicty_unit\": 1,\n" +
            "\t\t\"state\": \"1\",\n" +
            "\t\t\"is_froze\": 0,\n" +
            "\t\t\"warehouse_id\": \"8a0a854b63d2b8bf0163d2c89d2b0009\",\n" +
            "\t\t\"deep_pos_type\": null,\n" +
            "\t\t\"opposite_position\": null,\n" +
            "\t\t\"container_id\": null,\n" +
            "\t\t\"row\": \"01\",\n" +
            "\t\t\"col\": \"02\",\n" +
            "\t\t\"layer\": \"03\"\n" +
            "\t}, {\n" +
            "\t\t\"id\": \"8a0af66a6ac47e4f016ad95aeaff0f7c\",\n" +
            "\t\t\"code\": \"c01-010204\",\n" +
            "\t\t\"area_type_id\": \"8a0af66a6ac47e4f016ad94c37700e81\",\n" +
            "\t\t\"stock_area\": 9,\n" +
            "\t\t\"length\": 999,\n" +
            "\t\t\"width\": 999,\n" +
            "\t\t\"height\": 999,\n" +
            "\t\t\"total_capaicty\": 999,\n" +
            "\t\t\"cur_capacity\": 2,\n" +
            "\t\t\"capaicty_unit\": 1,\n" +
            "\t\t\"state\": \"2\",\n" +
            "\t\t\"is_froze\": 0,\n" +
            "\t\t\"warehouse_id\": \"8a0a854b63d2b8bf0163d2c89d2b0009\",\n" +
            "\t\t\"deep_pos_type\": null,\n" +
            "\t\t\"opposite_position\": null,\n" +
            "\t\t\"container_id\": null,\n" +
            "\t\t\"row\": \"01\",\n" +
            "\t\t\"col\": \"02\",\n" +
            "\t\t\"layer\": \"04\",\n" +
            "\t\t\"materiel_no\": \"500004651\",\n" +
            "\t\t\"materiel_name\": \"交流避雷器,ac35kv,51kv,硅橡胶,134kv,不带间隙\"\n" +
            "\t}, {\n" +
            "\t\t\"id\": \"8a0af66a6ac47e4f016ad95aeaff0f7c\",\n" +
            "\t\t\"code\": \"c01-010204\",\n" +
            "\t\t\"area_type_id\": \"8a0af66a6ac47e4f016ad94c37700e81\",\n" +
            "\t\t\"stock_area\": 9,\n" +
            "\t\t\"length\": 999,\n" +
            "\t\t\"width\": 999,\n" +
            "\t\t\"height\": 999,\n" +
            "\t\t\"total_capaicty\": 999,\n" +
            "\t\t\"cur_capacity\": 2,\n" +
            "\t\t\"capaicty_unit\": 1,\n" +
            "\t\t\"state\": \"2\",\n" +
            "\t\t\"is_froze\": 0,\n" +
            "\t\t\"warehouse_id\": \"8a0a854b63d2b8bf0163d2c89d2b0009\",\n" +
            "\t\t\"deep_pos_type\": null,\n" +
            "\t\t\"opposite_position\": null,\n" +
            "\t\t\"container_id\": null,\n" +
            "\t\t\"row\": \"01\",\n" +
            "\t\t\"col\": \"02\",\n" +
            "\t\t\"layer\": \"04\",\n" +
            "\t\t\"materiel_no\": \"500004651\",\n" +
            "\t\t\"materiel_name\": \"交流避雷器,ac35kv,51kv,硅橡胶,134kv,不带间隙\"\n" +
            "\t}, {\n" +
            "\t\t\"id\": \"8a0af66a6ac47e4f016ad95aeb2e0f7d\",\n" +
            "\t\t\"code\": \"c01-010205\",\n" +
            "\t\t\"area_type_id\": \"8a0af66a6ac47e4f016ad94c37700e81\",\n" +
            "\t\t\"stock_area\": 9,\n" +
            "\t\t\"length\": 999,\n" +
            "\t\t\"width\": 999,\n" +
            "\t\t\"height\": 999,\n" +
            "\t\t\"total_capaicty\": 999,\n" +
            "\t\t\"cur_capacity\": 0,\n" +
            "\t\t\"capaicty_unit\": 1,\n" +
            "\t\t\"state\": \"1\",\n" +
            "\t\t\"is_froze\": 0,\n" +
            "\t\t\"warehouse_id\": \"8a0a854b63d2b8bf0163d2c89d2b0009\",\n" +
            "\t\t\"deep_pos_type\": null,\n" +
            "\t\t\"opposite_position\": null,\n" +
            "\t\t\"container_id\": null,\n" +
            "\t\t\"row\": \"01\",\n" +
            "\t\t\"col\": \"02\",\n" +
            "\t\t\"layer\": \"05\"\n" +
            "\t}, {\n" +
            "\t\t\"id\": \"8a0af66a6ac47e4f016ad95aeb5d0f7e\",\n" +
            "\t\t\"code\": \"c01-010206\",\n" +
            "\t\t\"area_type_id\": \"8a0af66a6ac47e4f016ad94c37700e81\",\n" +
            "\t\t\"stock_area\": 9,\n" +
            "\t\t\"length\": 999,\n" +
            "\t\t\"width\": 999,\n" +
            "\t\t\"height\": 999,\n" +
            "\t\t\"total_capaicty\": 999,\n" +
            "\t\t\"cur_capacity\": 0,\n" +
            "\t\t\"capaicty_unit\": 1,\n" +
            "\t\t\"state\": \"1\",\n" +
            "\t\t\"is_froze\": 0,\n" +
            "\t\t\"warehouse_id\": \"8a0a854b63d2b8bf0163d2c89d2b0009\",\n" +
            "\t\t\"deep_pos_type\": null,\n" +
            "\t\t\"opposite_position\": null,\n" +
            "\t\t\"container_id\": null,\n" +
            "\t\t\"row\": \"01\",\n" +
            "\t\t\"col\": \"02\",\n" +
            "\t\t\"layer\": \"06\"\n" +
            "\t}, {\n" +
            "\t\t\"id\": \"8a0af66a6ac47e4f016ad95aeb8c0f7f\",\n" +
            "\t\t\"code\": \"c01-010207\",\n" +
            "\t\t\"area_type_id\": \"8a0af66a6ac47e4f016ad94c37700e81\",\n" +
            "\t\t\"stock_area\": 9,\n" +
            "\t\t\"length\": 999,\n" +
            "\t\t\"width\": 999,\n" +
            "\t\t\"height\": 999,\n" +
            "\t\t\"total_capaicty\": 999,\n" +
            "\t\t\"cur_capacity\": 0,\n" +
            "\t\t\"capaicty_unit\": 1,\n" +
            "\t\t\"state\": \"1\",\n" +
            "\t\t\"is_froze\": 0,\n" +
            "\t\t\"warehouse_id\": \"8a0a854b63d2b8bf0163d2c89d2b0009\",\n" +
            "\t\t\"deep_pos_type\": null,\n" +
            "\t\t\"opposite_position\": null,\n" +
            "\t\t\"container_id\": null,\n" +
            "\t\t\"row\": \"01\",\n" +
            "\t\t\"col\": \"02\",\n" +
            "\t\t\"layer\": \"07\"\n" +
            "\t}, {\n" +
            "\t\t\"id\": \"8a0af66a6ac47e4f016ad95aebab0f80\",\n" +
            "\t\t\"code\": \"c01-010208\",\n" +
            "\t\t\"area_type_id\": \"8a0af66a6ac47e4f016ad94c37700e81\",\n" +
            "\t\t\"stock_area\": 9,\n" +
            "\t\t\"length\": 999,\n" +
            "\t\t\"width\": 999,\n" +
            "\t\t\"height\": 999,\n" +
            "\t\t\"total_capaicty\": 999,\n" +
            "\t\t\"cur_capacity\": 0,\n" +
            "\t\t\"capaicty_unit\": 1,\n" +
            "\t\t\"state\": \"1\",\n" +
            "\t\t\"is_froze\": 0,\n" +
            "\t\t\"warehouse_id\": \"8a0a854b63d2b8bf0163d2c89d2b0009\",\n" +
            "\t\t\"deep_pos_type\": null,\n" +
            "\t\t\"opposite_position\": null,\n" +
            "\t\t\"container_id\": null,\n" +
            "\t\t\"row\": \"01\",\n" +
            "\t\t\"col\": \"02\",\n" +
            "\t\t\"layer\": \"08\"\n" +
            "\t}, {\n" +
            "\t\t\"id\": \"8a0af66a6ac47e4f016ad95aebda0f81\",\n" +
            "\t\t\"code\": \"c01-010209\",\n" +
            "\t\t\"area_type_id\": \"8a0af66a6ac47e4f016ad94c37700e81\",\n" +
            "\t\t\"stock_area\": 9,\n" +
            "\t\t\"length\": 999,\n" +
            "\t\t\"width\": 999,\n" +
            "\t\t\"height\": 999,\n" +
            "\t\t\"total_capaicty\": 999,\n" +
            "\t\t\"cur_capacity\": 0,\n" +
            "\t\t\"capaicty_unit\": 1,\n" +
            "\t\t\"state\": \"1\",\n" +
            "\t\t\"is_froze\": 0,\n" +
            "\t\t\"warehouse_id\": \"8a0a854b63d2b8bf0163d2c89d2b0009\",\n" +
            "\t\t\"deep_pos_type\": null,\n" +
            "\t\t\"opposite_position\": null,\n" +
            "\t\t\"container_id\": null,\n" +
            "\t\t\"row\": \"01\",\n" +
            "\t\t\"col\": \"02\",\n" +
            "\t\t\"layer\": \"09\"\n" +
            "\t}, {\n" +
            "\t\t\"id\": \"8a0af66a6ac47e4f016ad95aebf90f82\",\n" +
            "\t\t\"code\": \"c01-010210\",\n" +
            "\t\t\"area_type_id\": \"8a0af66a6ac47e4f016ad94c37700e81\",\n" +
            "\t\t\"stock_area\": 9,\n" +
            "\t\t\"length\": 999,\n" +
            "\t\t\"width\": 999,\n" +
            "\t\t\"height\": 999,\n" +
            "\t\t\"total_capaicty\": 999,\n" +
            "\t\t\"cur_capacity\": 0,\n" +
            "\t\t\"capaicty_unit\": 1,\n" +
            "\t\t\"state\": \"1\",\n" +
            "\t\t\"is_froze\": 0,\n" +
            "\t\t\"warehouse_id\": \"8a0a854b63d2b8bf0163d2c89d2b0009\",\n" +
            "\t\t\"deep_pos_type\": null,\n" +
            "\t\t\"opposite_position\": null,\n" +
            "\t\t\"container_id\": null,\n" +
            "\t\t\"row\": \"01\",\n" +
            "\t\t\"col\": \"02\",\n" +
            "\t\t\"layer\": \"10\"\n" +
            "\t}, {\n" +
            "\t\t\"id\": \"8a0af66a6ac47e4f016ad95aec190f83\",\n" +
            "\t\t\"code\": \"c01-010211\",\n" +
            "\t\t\"area_type_id\": \"8a0af66a6ac47e4f016ad94c37700e81\",\n" +
            "\t\t\"stock_area\": 9,\n" +
            "\t\t\"length\": 999,\n" +
            "\t\t\"width\": 999,\n" +
            "\t\t\"height\": 999,\n" +
            "\t\t\"total_capaicty\": 999,\n" +
            "\t\t\"cur_capacity\": 0,\n" +
            "\t\t\"capaicty_unit\": 1,\n" +
            "\t\t\"state\": \"1\",\n" +
            "\t\t\"is_froze\": 0,\n" +
            "\t\t\"warehouse_id\": \"8a0a854b63d2b8bf0163d2c89d2b0009\",\n" +
            "\t\t\"deep_pos_type\": null,\n" +
            "\t\t\"opposite_position\": null,\n" +
            "\t\t\"container_id\": null,\n" +
            "\t\t\"row\": \"01\",\n" +
            "\t\t\"col\": \"02\",\n" +
            "\t\t\"layer\": \"11\"\n" +
            "\t}, {\n" +
            "\t\t\"id\": \"8a0af66a6ac47e4f016ad95aec470f84\",\n" +
            "\t\t\"code\": \"c01-010212\",\n" +
            "\t\t\"area_type_id\": \"8a0af66a6ac47e4f016ad94c37700e81\",\n" +
            "\t\t\"stock_area\": 9,\n" +
            "\t\t\"length\": 999,\n" +
            "\t\t\"width\": 999,\n" +
            "\t\t\"height\": 999,\n" +
            "\t\t\"total_capaicty\": 999,\n" +
            "\t\t\"cur_capacity\": 0,\n" +
            "\t\t\"capaicty_unit\": 1,\n" +
            "\t\t\"state\": \"1\",\n" +
            "\t\t\"is_froze\": 0,\n" +
            "\t\t\"warehouse_id\": \"8a0a854b63d2b8bf0163d2c89d2b0009\",\n" +
            "\t\t\"deep_pos_type\": null,\n" +
            "\t\t\"opposite_position\": null,\n" +
            "\t\t\"container_id\": null,\n" +
            "\t\t\"row\": \"01\",\n" +
            "\t\t\"col\": \"02\",\n" +
            "\t\t\"layer\": \"12\"\n" +
            "\t}, {\n" +
            "\t\t\"id\": \"8a0af66a6ac47e4f016ad95aec670f85\",\n" +
            "\t\t\"code\": \"c01-010213\",\n" +
            "\t\t\"area_type_id\": \"8a0af66a6ac47e4f016ad94c37700e81\",\n" +
            "\t\t\"stock_area\": 9,\n" +
            "\t\t\"length\": 999,\n" +
            "\t\t\"width\": 999,\n" +
            "\t\t\"height\": 999,\n" +
            "\t\t\"total_capaicty\": 999,\n" +
            "\t\t\"cur_capacity\": 0,\n" +
            "\t\t\"capaicty_unit\": 1,\n" +
            "\t\t\"state\": \"1\",\n" +
            "\t\t\"is_froze\": 0,\n" +
            "\t\t\"warehouse_id\": \"8a0a854b63d2b8bf0163d2c89d2b0009\",\n" +
            "\t\t\"deep_pos_type\": null,\n" +
            "\t\t\"opposite_position\": null,\n" +
            "\t\t\"container_id\": null,\n" +
            "\t\t\"row\": \"01\",\n" +
            "\t\t\"col\": \"02\",\n" +
            "\t\t\"layer\": \"13\"\n" +
            "\t}, {\n" +
            "\t\t\"id\": \"8a0af66a6ac47e4f016ad95aec860f86\",\n" +
            "\t\t\"code\": \"c01-010214\",\n" +
            "\t\t\"area_type_id\": \"8a0af66a6ac47e4f016ad94c37700e81\",\n" +
            "\t\t\"stock_area\": 9,\n" +
            "\t\t\"length\": 999,\n" +
            "\t\t\"width\": 999,\n" +
            "\t\t\"height\": 999,\n" +
            "\t\t\"total_capaicty\": 999,\n" +
            "\t\t\"cur_capacity\": 0,\n" +
            "\t\t\"capaicty_unit\": 1,\n" +
            "\t\t\"state\": \"1\",\n" +
            "\t\t\"is_froze\": 0,\n" +
            "\t\t\"warehouse_id\": \"8a0a854b63d2b8bf0163d2c89d2b0009\",\n" +
            "\t\t\"deep_pos_type\": null,\n" +
            "\t\t\"opposite_position\": null,\n" +
            "\t\t\"container_id\": null,\n" +
            "\t\t\"row\": \"01\",\n" +
            "\t\t\"col\": \"02\",\n" +
            "\t\t\"layer\": \"14\"\n" +
            "\t}, {\n" +
            "\t\t\"id\": \"8a0af66a6ac47e4f016ad95aecb50f87\",\n" +
            "\t\t\"code\": \"c01-010215\",\n" +
            "\t\t\"area_type_id\": \"8a0af66a6ac47e4f016ad94c37700e81\",\n" +
            "\t\t\"stock_area\": 9,\n" +
            "\t\t\"length\": 999,\n" +
            "\t\t\"width\": 999,\n" +
            "\t\t\"height\": 999,\n" +
            "\t\t\"total_capaicty\": 999,\n" +
            "\t\t\"cur_capacity\": 0,\n" +
            "\t\t\"capaicty_unit\": 1,\n" +
            "\t\t\"state\": \"1\",\n" +
            "\t\t\"is_froze\": 0,\n" +
            "\t\t\"warehouse_id\": \"8a0a854b63d2b8bf0163d2c89d2b0009\",\n" +
            "\t\t\"deep_pos_type\": null,\n" +
            "\t\t\"opposite_position\": null,\n" +
            "\t\t\"container_id\": null,\n" +
            "\t\t\"row\": \"01\",\n" +
            "\t\t\"col\": \"02\",\n" +
            "\t\t\"layer\": \"15\"\n" +
            "\t}, {\n" +
            "\t\t\"id\": \"8a0af66a6ac47e4f016ad95aece40f88\",\n" +
            "\t\t\"code\": \"c01-010216\",\n" +
            "\t\t\"area_type_id\": \"8a0af66a6ac47e4f016ad94c37700e81\",\n" +
            "\t\t\"stock_area\": 9,\n" +
            "\t\t\"length\": 999,\n" +
            "\t\t\"width\": 999,\n" +
            "\t\t\"height\": 999,\n" +
            "\t\t\"total_capaicty\": 999,\n" +
            "\t\t\"cur_capacity\": 0,\n" +
            "\t\t\"capaicty_unit\": 1,\n" +
            "\t\t\"state\": \"1\",\n" +
            "\t\t\"is_froze\": 0,\n" +
            "\t\t\"warehouse_id\": \"8a0a854b63d2b8bf0163d2c89d2b0009\",\n" +
            "\t\t\"deep_pos_type\": null,\n" +
            "\t\t\"opposite_position\": null,\n" +
            "\t\t\"container_id\": null,\n" +
            "\t\t\"row\": \"01\",\n" +
            "\t\t\"col\": \"02\",\n" +
            "\t\t\"layer\": \"16\"\n" +
            "\t}, {\n" +
            "\t\t\"id\": \"8a0af66a6ac47e4f016ad95aed130f89\",\n" +
            "\t\t\"code\": \"c01-010217\",\n" +
            "\t\t\"area_type_id\": \"8a0af66a6ac47e4f016ad94c37700e81\",\n" +
            "\t\t\"stock_area\": 9,\n" +
            "\t\t\"length\": 999,\n" +
            "\t\t\"width\": 999,\n" +
            "\t\t\"height\": 999,\n" +
            "\t\t\"total_capaicty\": 999,\n" +
            "\t\t\"cur_capacity\": 0,\n" +
            "\t\t\"capaicty_unit\": 1,\n" +
            "\t\t\"state\": \"1\",\n" +
            "\t\t\"is_froze\": 0,\n" +
            "\t\t\"warehouse_id\": \"8a0a854b63d2b8bf0163d2c89d2b0009\",\n" +
            "\t\t\"deep_pos_type\": null,\n" +
            "\t\t\"opposite_position\": null,\n" +
            "\t\t\"container_id\": null,\n" +
            "\t\t\"row\": \"01\",\n" +
            "\t\t\"col\": \"02\",\n" +
            "\t\t\"layer\": \"17\"\n" +
            "\t}, {\n" +
            "\t\t\"id\": \"8a0af66a6ac47e4f016ad95aed510f8a\",\n" +
            "\t\t\"code\": \"c01-010218\",\n" +
            "\t\t\"area_type_id\": \"8a0af66a6ac47e4f016ad94c37700e81\",\n" +
            "\t\t\"stock_area\": 9,\n" +
            "\t\t\"length\": 999,\n" +
            "\t\t\"width\": 999,\n" +
            "\t\t\"height\": 999,\n" +
            "\t\t\"total_capaicty\": 999,\n" +
            "\t\t\"cur_capacity\": 0,\n" +
            "\t\t\"capaicty_unit\": 1,\n" +
            "\t\t\"state\": \"1\",\n" +
            "\t\t\"is_froze\": 0,\n" +
            "\t\t\"warehouse_id\": \"8a0a854b63d2b8bf0163d2c89d2b0009\",\n" +
            "\t\t\"deep_pos_type\": null,\n" +
            "\t\t\"opposite_position\": null,\n" +
            "\t\t\"container_id\": null,\n" +
            "\t\t\"row\": \"01\",\n" +
            "\t\t\"col\": \"02\",\n" +
            "\t\t\"layer\": \"18\"\n" +
            "\t}, {\n" +
            "\t\t\"id\": \"8a0af66a6ac47e4f016ad95aed700f8b\",\n" +
            "\t\t\"code\": \"c01-010219\",\n" +
            "\t\t\"area_type_id\": \"8a0af66a6ac47e4f016ad94c37700e81\",\n" +
            "\t\t\"stock_area\": 9,\n" +
            "\t\t\"length\": 999,\n" +
            "\t\t\"width\": 999,\n" +
            "\t\t\"height\": 999,\n" +
            "\t\t\"total_capaicty\": 999,\n" +
            "\t\t\"cur_capacity\": 0,\n" +
            "\t\t\"capaicty_unit\": 1,\n" +
            "\t\t\"state\": \"1\",\n" +
            "\t\t\"is_froze\": 0,\n" +
            "\t\t\"warehouse_id\": \"8a0a854b63d2b8bf0163d2c89d2b0009\",\n" +
            "\t\t\"deep_pos_type\": null,\n" +
            "\t\t\"opposite_position\": null,\n" +
            "\t\t\"container_id\": null,\n" +
            "\t\t\"row\": \"01\",\n" +
            "\t\t\"col\": \"02\",\n" +
            "\t\t\"layer\": \"19\"\n" +
            "\t}, {\n" +
            "\t\t\"id\": \"8a0af66a6ac47e4f016ad95aed9f0f8c\",\n" +
            "\t\t\"code\": \"c01-010220\",\n" +
            "\t\t\"area_type_id\": \"8a0af66a6ac47e4f016ad94c37700e81\",\n" +
            "\t\t\"stock_area\": 9,\n" +
            "\t\t\"length\": 999,\n" +
            "\t\t\"width\": 999,\n" +
            "\t\t\"height\": 999,\n" +
            "\t\t\"total_capaicty\": 999,\n" +
            "\t\t\"cur_capacity\": 0,\n" +
            "\t\t\"capaicty_unit\": 1,\n" +
            "\t\t\"state\": \"1\",\n" +
            "\t\t\"is_froze\": 0,\n" +
            "\t\t\"warehouse_id\": \"8a0a854b63d2b8bf0163d2c89d2b0009\",\n" +
            "\t\t\"deep_pos_type\": null,\n" +
            "\t\t\"opposite_position\": null,\n" +
            "\t\t\"container_id\": null,\n" +
            "\t\t\"row\": \"01\",\n" +
            "\t\t\"col\": \"02\",\n" +
            "\t\t\"layer\": \"20\"\n" +
            "\t}, {\n" +
            "\t\t\"id\": \"8a0af66a6ac47e4f016ad95aedce0f8d\",\n" +
            "\t\t\"code\": \"c01-010301\",\n" +
            "\t\t\"area_type_id\": \"8a0af66a6ac47e4f016ad94c37700e81\",\n" +
            "\t\t\"stock_area\": 9,\n" +
            "\t\t\"length\": 999,\n" +
            "\t\t\"width\": 999,\n" +
            "\t\t\"height\": 999,\n" +
            "\t\t\"total_capaicty\": 999,\n" +
            "\t\t\"cur_capacity\": 0,\n" +
            "\t\t\"capaicty_unit\": 1,\n" +
            "\t\t\"state\": \"1\",\n" +
            "\t\t\"is_froze\": 0,\n" +
            "\t\t\"warehouse_id\": \"8a0a854b63d2b8bf0163d2c89d2b0009\",\n" +
            "\t\t\"deep_pos_type\": null,\n" +
            "\t\t\"opposite_position\": null,\n" +
            "\t\t\"container_id\": null,\n" +
            "\t\t\"row\": \"01\",\n" +
            "\t\t\"col\": \"03\",\n" +
            "\t\t\"layer\": \"01\"\n" +
            "\t}, {\n" +
            "\t\t\"id\": \"8a0af66a6ac47e4f016ad95aedfd0f8e\",\n" +
            "\t\t\"code\": \"c01-010302\",\n" +
            "\t\t\"area_type_id\": \"8a0af66a6ac47e4f016ad94c37700e81\",\n" +
            "\t\t\"stock_area\": 9,\n" +
            "\t\t\"length\": 999,\n" +
            "\t\t\"width\": 999,\n" +
            "\t\t\"height\": 999,\n" +
            "\t\t\"total_capaicty\": 999,\n" +
            "\t\t\"cur_capacity\": 0,\n" +
            "\t\t\"capaicty_unit\": 1,\n" +
            "\t\t\"state\": \"1\",\n" +
            "\t\t\"is_froze\": 0,\n" +
            "\t\t\"warehouse_id\": \"8a0a854b63d2b8bf0163d2c89d2b0009\",\n" +
            "\t\t\"deep_pos_type\": null,\n" +
            "\t\t\"opposite_position\": null,\n" +
            "\t\t\"container_id\": null,\n" +
            "\t\t\"row\": \"01\",\n" +
            "\t\t\"col\": \"03\",\n" +
            "\t\t\"layer\": \"02\"\n" +
            "\t}, {\n" +
            "\t\t\"id\": \"8a0af66a6ac47e4f016ad95aee2c0f8f\",\n" +
            "\t\t\"code\": \"c01-010303\",\n" +
            "\t\t\"area_type_id\": \"8a0af66a6ac47e4f016ad94c37700e81\",\n" +
            "\t\t\"stock_area\": 9,\n" +
            "\t\t\"length\": 999,\n" +
            "\t\t\"width\": 999,\n" +
            "\t\t\"height\": 999,\n" +
            "\t\t\"total_capaicty\": 999,\n" +
            "\t\t\"cur_capacity\": 0,\n" +
            "\t\t\"capaicty_unit\": 1,\n" +
            "\t\t\"state\": \"1\",\n" +
            "\t\t\"is_froze\": 0,\n" +
            "\t\t\"warehouse_id\": \"8a0a854b63d2b8bf0163d2c89d2b0009\",\n" +
            "\t\t\"deep_pos_type\": null,\n" +
            "\t\t\"opposite_position\": null,\n" +
            "\t\t\"container_id\": null,\n" +
            "\t\t\"row\": \"01\",\n" +
            "\t\t\"col\": \"03\",\n" +
            "\t\t\"layer\": \"03\"\n" +
            "\t}, {\n" +
            "\t\t\"id\": \"8a0af66a6ac47e4f016ad95aee4b0f90\",\n" +
            "\t\t\"code\": \"c01-010304\",\n" +
            "\t\t\"area_type_id\": \"8a0af66a6ac47e4f016ad94c37700e81\",\n" +
            "\t\t\"stock_area\": 9,\n" +
            "\t\t\"length\": 999,\n" +
            "\t\t\"width\": 999,\n" +
            "\t\t\"height\": 999,\n" +
            "\t\t\"total_capaicty\": 999,\n" +
            "\t\t\"cur_capacity\": 0,\n" +
            "\t\t\"capaicty_unit\": 1,\n" +
            "\t\t\"state\": \"1\",\n" +
            "\t\t\"is_froze\": 0,\n" +
            "\t\t\"warehouse_id\": \"8a0a854b63d2b8bf0163d2c89d2b0009\",\n" +
            "\t\t\"deep_pos_type\": null,\n" +
            "\t\t\"opposite_position\": null,\n" +
            "\t\t\"container_id\": null,\n" +
            "\t\t\"row\": \"01\",\n" +
            "\t\t\"col\": \"03\",\n" +
            "\t\t\"layer\": \"04\"\n" +
            "\t}, {\n" +
            "\t\t\"id\": \"8a0af66a6ac47e4f016ad95aee7a0f91\",\n" +
            "\t\t\"code\": \"c01-010305\",\n" +
            "\t\t\"area_type_id\": \"8a0af66a6ac47e4f016ad94c37700e81\",\n" +
            "\t\t\"stock_area\": 9,\n" +
            "\t\t\"length\": 999,\n" +
            "\t\t\"width\": 999,\n" +
            "\t\t\"height\": 999,\n" +
            "\t\t\"total_capaicty\": 999,\n" +
            "\t\t\"cur_capacity\": 0,\n" +
            "\t\t\"capaicty_unit\": 1,\n" +
            "\t\t\"state\": \"1\",\n" +
            "\t\t\"is_froze\": 0,\n" +
            "\t\t\"warehouse_id\": \"8a0a854b63d2b8bf0163d2c89d2b0009\",\n" +
            "\t\t\"deep_pos_type\": null,\n" +
            "\t\t\"opposite_position\": null,\n" +
            "\t\t\"container_id\": null,\n" +
            "\t\t\"row\": \"01\",\n" +
            "\t\t\"col\": \"03\",\n" +
            "\t\t\"layer\": \"05\"\n" +
            "\t}, {\n" +
            "\t\t\"id\": \"8a0af66a6ac47e4f016ad95aee990f92\",\n" +
            "\t\t\"code\": \"c01-010306\",\n" +
            "\t\t\"area_type_id\": \"8a0af66a6ac47e4f016ad94c37700e81\",\n" +
            "\t\t\"stock_area\": 9,\n" +
            "\t\t\"length\": 999,\n" +
            "\t\t\"width\": 999,\n" +
            "\t\t\"height\": 999,\n" +
            "\t\t\"total_capaicty\": 999,\n" +
            "\t\t\"cur_capacity\": 0,\n" +
            "\t\t\"capaicty_unit\": 1,\n" +
            "\t\t\"state\": \"1\",\n" +
            "\t\t\"is_froze\": 0,\n" +
            "\t\t\"warehouse_id\": \"8a0a854b63d2b8bf0163d2c89d2b0009\",\n" +
            "\t\t\"deep_pos_type\": null,\n" +
            "\t\t\"opposite_position\": null,\n" +
            "\t\t\"container_id\": null,\n" +
            "\t\t\"row\": \"01\",\n" +
            "\t\t\"col\": \"03\",\n" +
            "\t\t\"layer\": \"06\"\n" +
            "\t}, {\n" +
            "\t\t\"id\": \"8a0af66a6ac47e4f016ad95aeec80f93\",\n" +
            "\t\t\"code\": \"c01-010307\",\n" +
            "\t\t\"area_type_id\": \"8a0af66a6ac47e4f016ad94c37700e81\",\n" +
            "\t\t\"stock_area\": 9,\n" +
            "\t\t\"length\": 999,\n" +
            "\t\t\"width\": 999,\n" +
            "\t\t\"height\": 999,\n" +
            "\t\t\"total_capaicty\": 999,\n" +
            "\t\t\"cur_capacity\": 0,\n" +
            "\t\t\"capaicty_unit\": 1,\n" +
            "\t\t\"state\": \"1\",\n" +
            "\t\t\"is_froze\": 0,\n" +
            "\t\t\"warehouse_id\": \"8a0a854b63d2b8bf0163d2c89d2b0009\",\n" +
            "\t\t\"deep_pos_type\": null,\n" +
            "\t\t\"opposite_position\": null,\n" +
            "\t\t\"container_id\": null,\n" +
            "\t\t\"row\": \"01\",\n" +
            "\t\t\"col\": \"03\",\n" +
            "\t\t\"layer\": \"07\"\n" +
            "\t}, {\n" +
            "\t\t\"id\": \"8a0af66a6ac47e4f016ad95aeef70f94\",\n" +
            "\t\t\"code\": \"c01-010308\",\n" +
            "\t\t\"area_type_id\": \"8a0af66a6ac47e4f016ad94c37700e81\",\n" +
            "\t\t\"stock_area\": 9,\n" +
            "\t\t\"length\": 999,\n" +
            "\t\t\"width\": 999,\n" +
            "\t\t\"height\": 999,\n" +
            "\t\t\"total_capaicty\": 999,\n" +
            "\t\t\"cur_capacity\": 0,\n" +
            "\t\t\"capaicty_unit\": 1,\n" +
            "\t\t\"state\": \"1\",\n" +
            "\t\t\"is_froze\": 0,\n" +
            "\t\t\"warehouse_id\": \"8a0a854b63d2b8bf0163d2c89d2b0009\",\n" +
            "\t\t\"deep_pos_type\": null,\n" +
            "\t\t\"opposite_position\": null,\n" +
            "\t\t\"container_id\": null,\n" +
            "\t\t\"row\": \"01\",\n" +
            "\t\t\"col\": \"03\",\n" +
            "\t\t\"layer\": \"08\"\n" +
            "\t}, {\n" +
            "\t\t\"id\": \"8a0af66a6ac47e4f016ad95aef160f95\",\n" +
            "\t\t\"code\": \"c01-010309\",\n" +
            "\t\t\"area_type_id\": \"8a0af66a6ac47e4f016ad94c37700e81\",\n" +
            "\t\t\"stock_area\": 9,\n" +
            "\t\t\"length\": 999,\n" +
            "\t\t\"width\": 999,\n" +
            "\t\t\"height\": 999,\n" +
            "\t\t\"total_capaicty\": 999,\n" +
            "\t\t\"cur_capacity\": 0,\n" +
            "\t\t\"capaicty_unit\": 1,\n" +
            "\t\t\"state\": \"1\",\n" +
            "\t\t\"is_froze\": 0,\n" +
            "\t\t\"warehouse_id\": \"8a0a854b63d2b8bf0163d2c89d2b0009\",\n" +
            "\t\t\"deep_pos_type\": null,\n" +
            "\t\t\"opposite_position\": null,\n" +
            "\t\t\"container_id\": null,\n" +
            "\t\t\"row\": \"01\",\n" +
            "\t\t\"col\": \"03\",\n" +
            "\t\t\"layer\": \"09\"\n" +
            "\t}, {\n" +
            "\t\t\"id\": \"8a0af66a6ac47e4f016ad95aef450f96\",\n" +
            "\t\t\"code\": \"c01-010310\",\n" +
            "\t\t\"area_type_id\": \"8a0af66a6ac47e4f016ad94c37700e81\",\n" +
            "\t\t\"stock_area\": 9,\n" +
            "\t\t\"length\": 999,\n" +
            "\t\t\"width\": 999,\n" +
            "\t\t\"height\": 999,\n" +
            "\t\t\"total_capaicty\": 999,\n" +
            "\t\t\"cur_capacity\": 0,\n" +
            "\t\t\"capaicty_unit\": 1,\n" +
            "\t\t\"state\": \"1\",\n" +
            "\t\t\"is_froze\": 0,\n" +
            "\t\t\"warehouse_id\": \"8a0a854b63d2b8bf0163d2c89d2b0009\",\n" +
            "\t\t\"deep_pos_type\": null,\n" +
            "\t\t\"opposite_position\": null,\n" +
            "\t\t\"container_id\": null,\n" +
            "\t\t\"row\": \"01\",\n" +
            "\t\t\"col\": \"03\",\n" +
            "\t\t\"layer\": \"10\"\n" +
            "\t}, {\n" +
            "\t\t\"id\": \"8a0af66a6ac47e4f016ad95aef640f97\",\n" +
            "\t\t\"code\": \"c01-010311\",\n" +
            "\t\t\"area_type_id\": \"8a0af66a6ac47e4f016ad94c37700e81\",\n" +
            "\t\t\"stock_area\": 9,\n" +
            "\t\t\"length\": 999,\n" +
            "\t\t\"width\": 999,\n" +
            "\t\t\"height\": 999,\n" +
            "\t\t\"total_capaicty\": 999,\n" +
            "\t\t\"cur_capacity\": 0,\n" +
            "\t\t\"capaicty_unit\": 1,\n" +
            "\t\t\"state\": \"1\",\n" +
            "\t\t\"is_froze\": 0,\n" +
            "\t\t\"warehouse_id\": \"8a0a854b63d2b8bf0163d2c89d2b0009\",\n" +
            "\t\t\"deep_pos_type\": null,\n" +
            "\t\t\"opposite_position\": null,\n" +
            "\t\t\"container_id\": null,\n" +
            "\t\t\"row\": \"01\",\n" +
            "\t\t\"col\": \"03\",\n" +
            "\t\t\"layer\": \"11\"\n" +
            "\t}, {\n" +
            "\t\t\"id\": \"8a0af66a6ac47e4f016ad95aef840f98\",\n" +
            "\t\t\"code\": \"c01-010312\",\n" +
            "\t\t\"area_type_id\": \"8a0af66a6ac47e4f016ad94c37700e81\",\n" +
            "\t\t\"stock_area\": 9,\n" +
            "\t\t\"length\": 999,\n" +
            "\t\t\"width\": 999,\n" +
            "\t\t\"height\": 999,\n" +
            "\t\t\"total_capaicty\": 999,\n" +
            "\t\t\"cur_capacity\": 0,\n" +
            "\t\t\"capaicty_unit\": 1,\n" +
            "\t\t\"state\": \"1\",\n" +
            "\t\t\"is_froze\": 0,\n" +
            "\t\t\"warehouse_id\": \"8a0a854b63d2b8bf0163d2c89d2b0009\",\n" +
            "\t\t\"deep_pos_type\": null,\n" +
            "\t\t\"opposite_position\": null,\n" +
            "\t\t\"container_id\": null,\n" +
            "\t\t\"row\": \"01\",\n" +
            "\t\t\"col\": \"03\",\n" +
            "\t\t\"layer\": \"12\"\n" +
            "\t}, {\n" +
            "\t\t\"id\": \"8a0af66a6ac47e4f016ad95aefb20f99\",\n" +
            "\t\t\"code\": \"c01-010313\",\n" +
            "\t\t\"area_type_id\": \"8a0af66a6ac47e4f016ad94c37700e81\",\n" +
            "\t\t\"stock_area\": 9,\n" +
            "\t\t\"length\": 999,\n" +
            "\t\t\"width\": 999,\n" +
            "\t\t\"height\": 999,\n" +
            "\t\t\"total_capaicty\": 999,\n" +
            "\t\t\"cur_capacity\": 0,\n" +
            "\t\t\"capaicty_unit\": 1,\n" +
            "\t\t\"state\": \"1\",\n" +
            "\t\t\"is_froze\": 0,\n" +
            "\t\t\"warehouse_id\": \"8a0a854b63d2b8bf0163d2c89d2b0009\",\n" +
            "\t\t\"deep_pos_type\": null,\n" +
            "\t\t\"opposite_position\": null,\n" +
            "\t\t\"container_id\": null,\n" +
            "\t\t\"row\": \"01\",\n" +
            "\t\t\"col\": \"03\",\n" +
            "\t\t\"layer\": \"13\"\n" +
            "\t}, {\n" +
            "\t\t\"id\": \"8a0af66a6ac47e4f016ad95aefd20f9a\",\n" +
            "\t\t\"code\": \"c01-010314\",\n" +
            "\t\t\"area_type_id\": \"8a0af66a6ac47e4f016ad94c37700e81\",\n" +
            "\t\t\"stock_area\": 9,\n" +
            "\t\t\"length\": 999,\n" +
            "\t\t\"width\": 999,\n" +
            "\t\t\"height\": 999,\n" +
            "\t\t\"total_capaicty\": 999,\n" +
            "\t\t\"cur_capacity\": 0,\n" +
            "\t\t\"capaicty_unit\": 1,\n" +
            "\t\t\"state\": \"1\",\n" +
            "\t\t\"is_froze\": 0,\n" +
            "\t\t\"warehouse_id\": \"8a0a854b63d2b8bf0163d2c89d2b0009\",\n" +
            "\t\t\"deep_pos_type\": null,\n" +
            "\t\t\"opposite_position\": null,\n" +
            "\t\t\"container_id\": null,\n" +
            "\t\t\"row\": \"01\",\n" +
            "\t\t\"col\": \"03\",\n" +
            "\t\t\"layer\": \"14\"\n" +
            "\t}, {\n" +
            "\t\t\"id\": \"8a0af66a6ac47e4f016ad95aefe10f9b\",\n" +
            "\t\t\"code\": \"c01-010315\",\n" +
            "\t\t\"area_type_id\": \"8a0af66a6ac47e4f016ad94c37700e81\",\n" +
            "\t\t\"stock_area\": 9,\n" +
            "\t\t\"length\": 999,\n" +
            "\t\t\"width\": 999,\n" +
            "\t\t\"height\": 999,\n" +
            "\t\t\"total_capaicty\": 999,\n" +
            "\t\t\"cur_capacity\": 0,\n" +
            "\t\t\"capaicty_unit\": 1,\n" +
            "\t\t\"state\": \"1\",\n" +
            "\t\t\"is_froze\": 0,\n" +
            "\t\t\"warehouse_id\": \"8a0a854b63d2b8bf0163d2c89d2b0009\",\n" +
            "\t\t\"deep_pos_type\": null,\n" +
            "\t\t\"opposite_position\": null,\n" +
            "\t\t\"container_id\": null,\n" +
            "\t\t\"row\": \"01\",\n" +
            "\t\t\"col\": \"03\",\n" +
            "\t\t\"layer\": \"15\"\n" +
            "\t}, {\n" +
            "\t\t\"id\": \"8a0af66a6ac47e4f016ad95af0100f9c\",\n" +
            "\t\t\"code\": \"c01-010316\",\n" +
            "\t\t\"area_type_id\": \"8a0af66a6ac47e4f016ad94c37700e81\",\n" +
            "\t\t\"stock_area\": 9,\n" +
            "\t\t\"length\": 999,\n" +
            "\t\t\"width\": 999,\n" +
            "\t\t\"height\": 999,\n" +
            "\t\t\"total_capaicty\": 999,\n" +
            "\t\t\"cur_capacity\": 0,\n" +
            "\t\t\"capaicty_unit\": 1,\n" +
            "\t\t\"state\": \"1\",\n" +
            "\t\t\"is_froze\": 0,\n" +
            "\t\t\"warehouse_id\": \"8a0a854b63d2b8bf0163d2c89d2b0009\",\n" +
            "\t\t\"deep_pos_type\": null,\n" +
            "\t\t\"opposite_position\": null,\n" +
            "\t\t\"container_id\": null,\n" +
            "\t\t\"row\": \"01\",\n" +
            "\t\t\"col\": \"03\",\n" +
            "\t\t\"layer\": \"16\"\n" +
            "\t}, {\n" +
            "\t\t\"id\": \"8a0af66a6ac47e4f016ad95af02f0f9d\",\n" +
            "\t\t\"code\": \"c01-010317\",\n" +
            "\t\t\"area_type_id\": \"8a0af66a6ac47e4f016ad94c37700e81\",\n" +
            "\t\t\"stock_area\": 9,\n" +
            "\t\t\"length\": 999,\n" +
            "\t\t\"width\": 999,\n" +
            "\t\t\"height\": 999,\n" +
            "\t\t\"total_capaicty\": 999,\n" +
            "\t\t\"cur_capacity\": 0,\n" +
            "\t\t\"capaicty_unit\": 1,\n" +
            "\t\t\"state\": \"1\",\n" +
            "\t\t\"is_froze\": 0,\n" +
            "\t\t\"warehouse_id\": \"8a0a854b63d2b8bf0163d2c89d2b0009\",\n" +
            "\t\t\"deep_pos_type\": null,\n" +
            "\t\t\"opposite_position\": null,\n" +
            "\t\t\"container_id\": null,\n" +
            "\t\t\"row\": \"01\",\n" +
            "\t\t\"col\": \"03\",\n" +
            "\t\t\"layer\": \"17\"\n" +
            "\t}, {\n" +
            "\t\t\"id\": \"8a0af66a6ac47e4f016ad95af04f0f9e\",\n" +
            "\t\t\"code\": \"c01-010318\",\n" +
            "\t\t\"area_type_id\": \"8a0af66a6ac47e4f016ad94c37700e81\",\n" +
            "\t\t\"stock_area\": 9,\n" +
            "\t\t\"length\": 999,\n" +
            "\t\t\"width\": 999,\n" +
            "\t\t\"height\": 999,\n" +
            "\t\t\"total_capaicty\": 999,\n" +
            "\t\t\"cur_capacity\": 0,\n" +
            "\t\t\"capaicty_unit\": 1,\n" +
            "\t\t\"state\": \"1\",\n" +
            "\t\t\"is_froze\": 0,\n" +
            "\t\t\"warehouse_id\": \"8a0a854b63d2b8bf0163d2c89d2b0009\",\n" +
            "\t\t\"deep_pos_type\": null,\n" +
            "\t\t\"opposite_position\": null,\n" +
            "\t\t\"container_id\": null,\n" +
            "\t\t\"row\": \"01\",\n" +
            "\t\t\"col\": \"03\",\n" +
            "\t\t\"layer\": \"18\"\n" +
            "\t}, {\n" +
            "\t\t\"id\": \"8a0af66a6ac47e4f016ad95af06e0f9f\",\n" +
            "\t\t\"code\": \"c01-010319\",\n" +
            "\t\t\"area_type_id\": \"8a0af66a6ac47e4f016ad94c37700e81\",\n" +
            "\t\t\"stock_area\": 9,\n" +
            "\t\t\"length\": 999,\n" +
            "\t\t\"width\": 999,\n" +
            "\t\t\"height\": 999,\n" +
            "\t\t\"total_capaicty\": 999,\n" +
            "\t\t\"cur_capacity\": 0,\n" +
            "\t\t\"capaicty_unit\": 1,\n" +
            "\t\t\"state\": \"1\",\n" +
            "\t\t\"is_froze\": 0,\n" +
            "\t\t\"warehouse_id\": \"8a0a854b63d2b8bf0163d2c89d2b0009\",\n" +
            "\t\t\"deep_pos_type\": null,\n" +
            "\t\t\"opposite_position\": null,\n" +
            "\t\t\"container_id\": null,\n" +
            "\t\t\"row\": \"01\",\n" +
            "\t\t\"col\": \"03\",\n" +
            "\t\t\"layer\": \"19\"\n" +
            "\t}, {\n" +
            "\t\t\"id\": \"8a0af66a6ac47e4f016ad95af08d0fa0\",\n" +
            "\t\t\"code\": \"c01-010320\",\n" +
            "\t\t\"area_type_id\": \"8a0af66a6ac47e4f016ad94c37700e81\",\n" +
            "\t\t\"stock_area\": 9,\n" +
            "\t\t\"length\": 999,\n" +
            "\t\t\"width\": 999,\n" +
            "\t\t\"height\": 999,\n" +
            "\t\t\"total_capaicty\": 999,\n" +
            "\t\t\"cur_capacity\": 0,\n" +
            "\t\t\"capaicty_unit\": 1,\n" +
            "\t\t\"state\": \"1\",\n" +
            "\t\t\"is_froze\": 0,\n" +
            "\t\t\"warehouse_id\": \"8a0a854b63d2b8bf0163d2c89d2b0009\",\n" +
            "\t\t\"deep_pos_type\": null,\n" +
            "\t\t\"opposite_position\": null,\n" +
            "\t\t\"container_id\": null,\n" +
            "\t\t\"row\": \"01\",\n" +
            "\t\t\"col\": \"03\",\n" +
            "\t\t\"layer\": \"20\"\n" +
            "\t}, {\n" +
            "\t\t\"id\": \"8a0af66a6ac47e4f016ad95af0ac0fa1\",\n" +
            "\t\t\"code\": \"c01-010401\",\n" +
            "\t\t\"area_type_id\": \"8a0af66a6ac47e4f016ad94c37700e81\",\n" +
            "\t\t\"stock_area\": 9,\n" +
            "\t\t\"length\": 999,\n" +
            "\t\t\"width\": 999,\n" +
            "\t\t\"height\": 999,\n" +
            "\t\t\"total_capaicty\": 999,\n" +
            "\t\t\"cur_capacity\": 0,\n" +
            "\t\t\"capaicty_unit\": 1,\n" +
            "\t\t\"state\": \"1\",\n" +
            "\t\t\"is_froze\": 0,\n" +
            "\t\t\"warehouse_id\": \"8a0a854b63d2b8bf0163d2c89d2b0009\",\n" +
            "\t\t\"deep_pos_type\": null,\n" +
            "\t\t\"opposite_position\": null,\n" +
            "\t\t\"container_id\": null,\n" +
            "\t\t\"row\": \"01\",\n" +
            "\t\t\"col\": \"04\",\n" +
            "\t\t\"layer\": \"01\"\n" +
            "\t}, {\n" +
            "\t\t\"id\": \"8a0af66a6ac47e4f016ad95af0cc0fa2\",\n" +
            "\t\t\"code\": \"c01-010402\",\n" +
            "\t\t\"area_type_id\": \"8a0af66a6ac47e4f016ad94c37700e81\",\n" +
            "\t\t\"stock_area\": 9,\n" +
            "\t\t\"length\": 999,\n" +
            "\t\t\"width\": 999,\n" +
            "\t\t\"height\": 999,\n" +
            "\t\t\"total_capaicty\": 999,\n" +
            "\t\t\"cur_capacity\": 0,\n" +
            "\t\t\"capaicty_unit\": 1,\n" +
            "\t\t\"state\": \"1\",\n" +
            "\t\t\"is_froze\": 0,\n" +
            "\t\t\"warehouse_id\": \"8a0a854b63d2b8bf0163d2c89d2b0009\",\n" +
            "\t\t\"deep_pos_type\": null,\n" +
            "\t\t\"opposite_position\": null,\n" +
            "\t\t\"container_id\": null,\n" +
            "\t\t\"row\": \"01\",\n" +
            "\t\t\"col\": \"04\",\n" +
            "\t\t\"layer\": \"02\"\n" +
            "\t}, {\n" +
            "\t\t\"id\": \"8a0af66a6ac47e4f016ad95af0eb0fa3\",\n" +
            "\t\t\"code\": \"c01-010403\",\n" +
            "\t\t\"area_type_id\": \"8a0af66a6ac47e4f016ad94c37700e81\",\n" +
            "\t\t\"stock_area\": 9,\n" +
            "\t\t\"length\": 999,\n" +
            "\t\t\"width\": 999,\n" +
            "\t\t\"height\": 999,\n" +
            "\t\t\"total_capaicty\": 999,\n" +
            "\t\t\"cur_capacity\": 0,\n" +
            "\t\t\"capaicty_unit\": 1,\n" +
            "\t\t\"state\": \"1\",\n" +
            "\t\t\"is_froze\": 0,\n" +
            "\t\t\"warehouse_id\": \"8a0a854b63d2b8bf0163d2c89d2b0009\",\n" +
            "\t\t\"deep_pos_type\": null,\n" +
            "\t\t\"opposite_position\": null,\n" +
            "\t\t\"container_id\": null,\n" +
            "\t\t\"row\": \"01\",\n" +
            "\t\t\"col\": \"04\",\n" +
            "\t\t\"layer\": \"03\"\n" +
            "\t}, {\n" +
            "\t\t\"id\": \"8a0af66a6ac47e4f016ad95af10a0fa4\",\n" +
            "\t\t\"code\": \"c01-010404\",\n" +
            "\t\t\"area_type_id\": \"8a0af66a6ac47e4f016ad94c37700e81\",\n" +
            "\t\t\"stock_area\": 9,\n" +
            "\t\t\"length\": 999,\n" +
            "\t\t\"width\": 999,\n" +
            "\t\t\"height\": 999,\n" +
            "\t\t\"total_capaicty\": 999,\n" +
            "\t\t\"cur_capacity\": 0,\n" +
            "\t\t\"capaicty_unit\": 1,\n" +
            "\t\t\"state\": \"1\",\n" +
            "\t\t\"is_froze\": 0,\n" +
            "\t\t\"warehouse_id\": \"8a0a854b63d2b8bf0163d2c89d2b0009\",\n" +
            "\t\t\"deep_pos_type\": null,\n" +
            "\t\t\"opposite_position\": null,\n" +
            "\t\t\"container_id\": null,\n" +
            "\t\t\"row\": \"01\",\n" +
            "\t\t\"col\": \"04\",\n" +
            "\t\t\"layer\": \"04\"\n" +
            "\t}, {\n" +
            "\t\t\"id\": \"8a0af66a6ac47e4f016ad95af1290fa5\",\n" +
            "\t\t\"code\": \"c01-010405\",\n" +
            "\t\t\"area_type_id\": \"8a0af66a6ac47e4f016ad94c37700e81\",\n" +
            "\t\t\"stock_area\": 9,\n" +
            "\t\t\"length\": 999,\n" +
            "\t\t\"width\": 999,\n" +
            "\t\t\"height\": 999,\n" +
            "\t\t\"total_capaicty\": 999,\n" +
            "\t\t\"cur_capacity\": 0,\n" +
            "\t\t\"capaicty_unit\": 1,\n" +
            "\t\t\"state\": \"1\",\n" +
            "\t\t\"is_froze\": 0,\n" +
            "\t\t\"warehouse_id\": \"8a0a854b63d2b8bf0163d2c89d2b0009\",\n" +
            "\t\t\"deep_pos_type\": null,\n" +
            "\t\t\"opposite_position\": null,\n" +
            "\t\t\"container_id\": null,\n" +
            "\t\t\"row\": \"01\",\n" +
            "\t\t\"col\": \"04\",\n" +
            "\t\t\"layer\": \"05\"\n" +
            "\t}, {\n" +
            "\t\t\"id\": \"8a0af66a6ac47e4f016ad95af1490fa6\",\n" +
            "\t\t\"code\": \"c01-010406\",\n" +
            "\t\t\"area_type_id\": \"8a0af66a6ac47e4f016ad94c37700e81\",\n" +
            "\t\t\"stock_area\": 9,\n" +
            "\t\t\"length\": 999,\n" +
            "\t\t\"width\": 999,\n" +
            "\t\t\"height\": 999,\n" +
            "\t\t\"total_capaicty\": 999,\n" +
            "\t\t\"cur_capacity\": 0,\n" +
            "\t\t\"capaicty_unit\": 1,\n" +
            "\t\t\"state\": \"1\",\n" +
            "\t\t\"is_froze\": 0,\n" +
            "\t\t\"warehouse_id\": \"8a0a854b63d2b8bf0163d2c89d2b0009\",\n" +
            "\t\t\"deep_pos_type\": null,\n" +
            "\t\t\"opposite_position\": null,\n" +
            "\t\t\"container_id\": null,\n" +
            "\t\t\"row\": \"01\",\n" +
            "\t\t\"col\": \"04\",\n" +
            "\t\t\"layer\": \"06\"\n" +
            "\t}, {\n" +
            "\t\t\"id\": \"8a0af66a6ac47e4f016ad95af1680fa7\",\n" +
            "\t\t\"code\": \"c01-010407\",\n" +
            "\t\t\"area_type_id\": \"8a0af66a6ac47e4f016ad94c37700e81\",\n" +
            "\t\t\"stock_area\": 9,\n" +
            "\t\t\"length\": 999,\n" +
            "\t\t\"width\": 999,\n" +
            "\t\t\"height\": 999,\n" +
            "\t\t\"total_capaicty\": 999,\n" +
            "\t\t\"cur_capacity\": 0,\n" +
            "\t\t\"capaicty_unit\": 1,\n" +
            "\t\t\"state\": \"1\",\n" +
            "\t\t\"is_froze\": 0,\n" +
            "\t\t\"warehouse_id\": \"8a0a854b63d2b8bf0163d2c89d2b0009\",\n" +
            "\t\t\"deep_pos_type\": null,\n" +
            "\t\t\"opposite_position\": null,\n" +
            "\t\t\"container_id\": null,\n" +
            "\t\t\"row\": \"01\",\n" +
            "\t\t\"col\": \"04\",\n" +
            "\t\t\"layer\": \"07\"\n" +
            "\t}, {\n" +
            "\t\t\"id\": \"8a0af66a6ac47e4f016ad95af1780fa8\",\n" +
            "\t\t\"code\": \"c01-010408\",\n" +
            "\t\t\"area_type_id\": \"8a0af66a6ac47e4f016ad94c37700e81\",\n" +
            "\t\t\"stock_area\": 9,\n" +
            "\t\t\"length\": 999,\n" +
            "\t\t\"width\": 999,\n" +
            "\t\t\"height\": 999,\n" +
            "\t\t\"total_capaicty\": 999,\n" +
            "\t\t\"cur_capacity\": 0,\n" +
            "\t\t\"capaicty_unit\": 1,\n" +
            "\t\t\"state\": \"1\",\n" +
            "\t\t\"is_froze\": 0,\n" +
            "\t\t\"warehouse_id\": \"8a0a854b63d2b8bf0163d2c89d2b0009\",\n" +
            "\t\t\"deep_pos_type\": null,\n" +
            "\t\t\"opposite_position\": null,\n" +
            "\t\t\"container_id\": null,\n" +
            "\t\t\"row\": \"01\",\n" +
            "\t\t\"col\": \"04\",\n" +
            "\t\t\"layer\": \"08\"\n" +
            "\t}, {\n" +
            "\t\t\"id\": \"8a0af66a6ac47e4f016ad95af1970fa9\",\n" +
            "\t\t\"code\": \"c01-010409\",\n" +
            "\t\t\"area_type_id\": \"8a0af66a6ac47e4f016ad94c37700e81\",\n" +
            "\t\t\"stock_area\": 9,\n" +
            "\t\t\"length\": 999,\n" +
            "\t\t\"width\": 999,\n" +
            "\t\t\"height\": 999,\n" +
            "\t\t\"total_capaicty\": 999,\n" +
            "\t\t\"cur_capacity\": 0,\n" +
            "\t\t\"capaicty_unit\": 1,\n" +
            "\t\t\"state\": \"1\",\n" +
            "\t\t\"is_froze\": 0,\n" +
            "\t\t\"warehouse_id\": \"8a0a854b63d2b8bf0163d2c89d2b0009\",\n" +
            "\t\t\"deep_pos_type\": null,\n" +
            "\t\t\"opposite_position\": null,\n" +
            "\t\t\"container_id\": null,\n" +
            "\t\t\"row\": \"01\",\n" +
            "\t\t\"col\": \"04\",\n" +
            "\t\t\"layer\": \"09\"\n" +
            "\t}, {\n" +
            "\t\t\"id\": \"8a0af66a6ac47e4f016ad95af1b60faa\",\n" +
            "\t\t\"code\": \"c01-010410\",\n" +
            "\t\t\"area_type_id\": \"8a0af66a6ac47e4f016ad94c37700e81\",\n" +
            "\t\t\"stock_area\": 9,\n" +
            "\t\t\"length\": 999,\n" +
            "\t\t\"width\": 999,\n" +
            "\t\t\"height\": 999,\n" +
            "\t\t\"total_capaicty\": 999,\n" +
            "\t\t\"cur_capacity\": 0,\n" +
            "\t\t\"capaicty_unit\": 1,\n" +
            "\t\t\"state\": \"1\",\n" +
            "\t\t\"is_froze\": 0,\n" +
            "\t\t\"warehouse_id\": \"8a0a854b63d2b8bf0163d2c89d2b0009\",\n" +
            "\t\t\"deep_pos_type\": null,\n" +
            "\t\t\"opposite_position\": null,\n" +
            "\t\t\"container_id\": null,\n" +
            "\t\t\"row\": \"01\",\n" +
            "\t\t\"col\": \"04\",\n" +
            "\t\t\"layer\": \"10\"\n" +
            "\t}, {\n" +
            "\t\t\"id\": \"8a0af66a6ac47e4f016ad95af1d50fab\",\n" +
            "\t\t\"code\": \"c01-010411\",\n" +
            "\t\t\"area_type_id\": \"8a0af66a6ac47e4f016ad94c37700e81\",\n" +
            "\t\t\"stock_area\": 9,\n" +
            "\t\t\"length\": 999,\n" +
            "\t\t\"width\": 999,\n" +
            "\t\t\"height\": 999,\n" +
            "\t\t\"total_capaicty\": 999,\n" +
            "\t\t\"cur_capacity\": 0,\n" +
            "\t\t\"capaicty_unit\": 1,\n" +
            "\t\t\"state\": \"1\",\n" +
            "\t\t\"is_froze\": 0,\n" +
            "\t\t\"warehouse_id\": \"8a0a854b63d2b8bf0163d2c89d2b0009\",\n" +
            "\t\t\"deep_pos_type\": null,\n" +
            "\t\t\"opposite_position\": null,\n" +
            "\t\t\"container_id\": null,\n" +
            "\t\t\"row\": \"01\",\n" +
            "\t\t\"col\": \"04\",\n" +
            "\t\t\"layer\": \"11\"\n" +
            "\t}, {\n" +
            "\t\t\"id\": \"8a0af66a6ac47e4f016ad95af1e50fac\",\n" +
            "\t\t\"code\": \"c01-010412\",\n" +
            "\t\t\"area_type_id\": \"8a0af66a6ac47e4f016ad94c37700e81\",\n" +
            "\t\t\"stock_area\": 9,\n" +
            "\t\t\"length\": 999,\n" +
            "\t\t\"width\": 999,\n" +
            "\t\t\"height\": 999,\n" +
            "\t\t\"total_capaicty\": 999,\n" +
            "\t\t\"cur_capacity\": 0,\n" +
            "\t\t\"capaicty_unit\": 1,\n" +
            "\t\t\"state\": \"1\",\n" +
            "\t\t\"is_froze\": 0,\n" +
            "\t\t\"warehouse_id\": \"8a0a854b63d2b8bf0163d2c89d2b0009\",\n" +
            "\t\t\"deep_pos_type\": null,\n" +
            "\t\t\"opposite_position\": null,\n" +
            "\t\t\"container_id\": null,\n" +
            "\t\t\"row\": \"01\",\n" +
            "\t\t\"col\": \"04\",\n" +
            "\t\t\"layer\": \"12\"\n" +
            "\t}, {\n" +
            "\t\t\"id\": \"8a0af66a6ac47e4f016ad95af2040fad\",\n" +
            "\t\t\"code\": \"c01-010413\",\n" +
            "\t\t\"area_type_id\": \"8a0af66a6ac47e4f016ad94c37700e81\",\n" +
            "\t\t\"stock_area\": 9,\n" +
            "\t\t\"length\": 999,\n" +
            "\t\t\"width\": 999,\n" +
            "\t\t\"height\": 999,\n" +
            "\t\t\"total_capaicty\": 999,\n" +
            "\t\t\"cur_capacity\": 0,\n" +
            "\t\t\"capaicty_unit\": 1,\n" +
            "\t\t\"state\": \"1\",\n" +
            "\t\t\"is_froze\": 0,\n" +
            "\t\t\"warehouse_id\": \"8a0a854b63d2b8bf0163d2c89d2b0009\",\n" +
            "\t\t\"deep_pos_type\": null,\n" +
            "\t\t\"opposite_position\": null,\n" +
            "\t\t\"container_id\": null,\n" +
            "\t\t\"row\": \"01\",\n" +
            "\t\t\"col\": \"04\",\n" +
            "\t\t\"layer\": \"13\"\n" +
            "\t}, {\n" +
            "\t\t\"id\": \"8a0af66a6ac47e4f016ad95af2230fae\",\n" +
            "\t\t\"code\": \"c01-010414\",\n" +
            "\t\t\"area_type_id\": \"8a0af66a6ac47e4f016ad94c37700e81\",\n" +
            "\t\t\"stock_area\": 9,\n" +
            "\t\t\"length\": 999,\n" +
            "\t\t\"width\": 999,\n" +
            "\t\t\"height\": 999,\n" +
            "\t\t\"total_capaicty\": 999,\n" +
            "\t\t\"cur_capacity\": 0,\n" +
            "\t\t\"capaicty_unit\": 1,\n" +
            "\t\t\"state\": \"1\",\n" +
            "\t\t\"is_froze\": 0,\n" +
            "\t\t\"warehouse_id\": \"8a0a854b63d2b8bf0163d2c89d2b0009\",\n" +
            "\t\t\"deep_pos_type\": null,\n" +
            "\t\t\"opposite_position\": null,\n" +
            "\t\t\"container_id\": null,\n" +
            "\t\t\"row\": \"01\",\n" +
            "\t\t\"col\": \"04\",\n" +
            "\t\t\"layer\": \"14\"\n" +
            "\t}, {\n" +
            "\t\t\"id\": \"8a0af66a6ac47e4f016ad95af2330faf\",\n" +
            "\t\t\"code\": \"c01-010415\",\n" +
            "\t\t\"area_type_id\": \"8a0af66a6ac47e4f016ad94c37700e81\",\n" +
            "\t\t\"stock_area\": 9,\n" +
            "\t\t\"length\": 999,\n" +
            "\t\t\"width\": 999,\n" +
            "\t\t\"height\": 999,\n" +
            "\t\t\"total_capaicty\": 999,\n" +
            "\t\t\"cur_capacity\": 0,\n" +
            "\t\t\"capaicty_unit\": 1,\n" +
            "\t\t\"state\": \"1\",\n" +
            "\t\t\"is_froze\": 0,\n" +
            "\t\t\"warehouse_id\": \"8a0a854b63d2b8bf0163d2c89d2b0009\",\n" +
            "\t\t\"deep_pos_type\": null,\n" +
            "\t\t\"opposite_position\": null,\n" +
            "\t\t\"container_id\": null,\n" +
            "\t\t\"row\": \"01\",\n" +
            "\t\t\"col\": \"04\",\n" +
            "\t\t\"layer\": \"15\"\n" +
            "\t}, {\n" +
            "\t\t\"id\": \"8a0af66a6ac47e4f016ad95af2520fb0\",\n" +
            "\t\t\"code\": \"c01-010416\",\n" +
            "\t\t\"area_type_id\": \"8a0af66a6ac47e4f016ad94c37700e81\",\n" +
            "\t\t\"stock_area\": 9,\n" +
            "\t\t\"length\": 999,\n" +
            "\t\t\"width\": 999,\n" +
            "\t\t\"height\": 999,\n" +
            "\t\t\"total_capaicty\": 999,\n" +
            "\t\t\"cur_capacity\": 0,\n" +
            "\t\t\"capaicty_unit\": 1,\n" +
            "\t\t\"state\": \"1\",\n" +
            "\t\t\"is_froze\": 0,\n" +
            "\t\t\"warehouse_id\": \"8a0a854b63d2b8bf0163d2c89d2b0009\",\n" +
            "\t\t\"deep_pos_type\": null,\n" +
            "\t\t\"opposite_position\": null,\n" +
            "\t\t\"container_id\": null,\n" +
            "\t\t\"row\": \"01\",\n" +
            "\t\t\"col\": \"04\",\n" +
            "\t\t\"layer\": \"16\"\n" +
            "\t}, {\n" +
            "\t\t\"id\": \"8a0af66a6ac47e4f016ad95af2720fb1\",\n" +
            "\t\t\"code\": \"c01-010417\",\n" +
            "\t\t\"area_type_id\": \"8a0af66a6ac47e4f016ad94c37700e81\",\n" +
            "\t\t\"stock_area\": 9,\n" +
            "\t\t\"length\": 999,\n" +
            "\t\t\"width\": 999,\n" +
            "\t\t\"height\": 999,\n" +
            "\t\t\"total_capaicty\": 999,\n" +
            "\t\t\"cur_capacity\": 0,\n" +
            "\t\t\"capaicty_unit\": 1,\n" +
            "\t\t\"state\": \"1\",\n" +
            "\t\t\"is_froze\": 0,\n" +
            "\t\t\"warehouse_id\": \"8a0a854b63d2b8bf0163d2c89d2b0009\",\n" +
            "\t\t\"deep_pos_type\": null,\n" +
            "\t\t\"opposite_position\": null,\n" +
            "\t\t\"container_id\": null,\n" +
            "\t\t\"row\": \"01\",\n" +
            "\t\t\"col\": \"04\",\n" +
            "\t\t\"layer\": \"17\"\n" +
            "\t}, {\n" +
            "\t\t\"id\": \"8a0af66a6ac47e4f016ad95af2910fb2\",\n" +
            "\t\t\"code\": \"c01-010418\",\n" +
            "\t\t\"area_type_id\": \"8a0af66a6ac47e4f016ad94c37700e81\",\n" +
            "\t\t\"stock_area\": 9,\n" +
            "\t\t\"length\": 999,\n" +
            "\t\t\"width\": 999,\n" +
            "\t\t\"height\": 999,\n" +
            "\t\t\"total_capaicty\": 999,\n" +
            "\t\t\"cur_capacity\": 0,\n" +
            "\t\t\"capaicty_unit\": 1,\n" +
            "\t\t\"state\": \"1\",\n" +
            "\t\t\"is_froze\": 0,\n" +
            "\t\t\"warehouse_id\": \"8a0a854b63d2b8bf0163d2c89d2b0009\",\n" +
            "\t\t\"deep_pos_type\": null,\n" +
            "\t\t\"opposite_position\": null,\n" +
            "\t\t\"container_id\": null,\n" +
            "\t\t\"row\": \"01\",\n" +
            "\t\t\"col\": \"04\",\n" +
            "\t\t\"layer\": \"18\"\n" +
            "\t}, {\n" +
            "\t\t\"id\": \"8a0af66a6ac47e4f016ad95af2b00fb3\",\n" +
            "\t\t\"code\": \"c01-010419\",\n" +
            "\t\t\"area_type_id\": \"8a0af66a6ac47e4f016ad94c37700e81\",\n" +
            "\t\t\"stock_area\": 9,\n" +
            "\t\t\"length\": 999,\n" +
            "\t\t\"width\": 999,\n" +
            "\t\t\"height\": 999,\n" +
            "\t\t\"total_capaicty\": 999,\n" +
            "\t\t\"cur_capacity\": 0,\n" +
            "\t\t\"capaicty_unit\": 1,\n" +
            "\t\t\"state\": \"1\",\n" +
            "\t\t\"is_froze\": 0,\n" +
            "\t\t\"warehouse_id\": \"8a0a854b63d2b8bf0163d2c89d2b0009\",\n" +
            "\t\t\"deep_pos_type\": null,\n" +
            "\t\t\"opposite_position\": null,\n" +
            "\t\t\"container_id\": null,\n" +
            "\t\t\"row\": \"01\",\n" +
            "\t\t\"col\": \"04\",\n" +
            "\t\t\"layer\": \"19\"\n" +
            "\t}, {\n" +
            "\t\t\"id\": \"8a0af66a6ac47e4f016ad95af2c00fb4\",\n" +
            "\t\t\"code\": \"c01-010420\",\n" +
            "\t\t\"area_type_id\": \"8a0af66a6ac47e4f016ad94c37700e81\",\n" +
            "\t\t\"stock_area\": 9,\n" +
            "\t\t\"length\": 999,\n" +
            "\t\t\"width\": 999,\n" +
            "\t\t\"height\": 999,\n" +
            "\t\t\"total_capaicty\": 999,\n" +
            "\t\t\"cur_capacity\": 0,\n" +
            "\t\t\"capaicty_unit\": 1,\n" +
            "\t\t\"state\": \"1\",\n" +
            "\t\t\"is_froze\": 0,\n" +
            "\t\t\"warehouse_id\": \"8a0a854b63d2b8bf0163d2c89d2b0009\",\n" +
            "\t\t\"deep_pos_type\": null,\n" +
            "\t\t\"opposite_position\": null,\n" +
            "\t\t\"container_id\": null,\n" +
            "\t\t\"row\": \"01\",\n" +
            "\t\t\"col\": \"04\",\n" +
            "\t\t\"layer\": \"20\"\n" +
            "\t}]\n" +
            "\n" +
            "}]";


    //制造年度数据
    public static List<CalendarMain> createCalendarData(){
        List<CalendarMain> aList =new ArrayList<CalendarMain>();

        CalendarMain main =new CalendarMain();

        List<CalendarData> datas =new ArrayList<CalendarData>() ;

        CalendarData data1 =new CalendarData("2017-01-26", 5279);
//        CalendarData data2 =new CalendarData("2017-01-27", 6107);
//        CalendarData data3 =new CalendarData("2017-01-28", 7462);
//        CalendarData data4 =new CalendarData("2017-01-29", 7869);
//        CalendarData data5 =new CalendarData("2017-01-30", 6528);
//        CalendarData data6 =new CalendarData("2017-01-31", 3112);
//        CalendarData data7 =new CalendarData("2017-02-10", 5401);
//        CalendarData data8 =new CalendarData("2017-02-11", 6311);
        CalendarData data9 =new CalendarData("2017-02-15", 8917);
        CalendarData data10 =new CalendarData("2017-02-16", 9897);

        datas.add(data1);
//        datas.add(data2);
//        datas.add(data3);
//        datas.add(data4);
//        datas.add(data5);
//        datas.add(data6);
//        datas.add(data7);
//        datas.add(data8);
        datas.add(data9);
        datas.add(data10);

        main.setDataList(datas);


        CalendarMain main2 =new CalendarMain();

        List<CalendarData> dataVs =new ArrayList<CalendarData>() ;


//        CalendarData dataV1 =new CalendarData("2018-01-26", 5179);
        CalendarData dataV2 =new CalendarData("2018-01-27", 6007);
//        CalendarData dataV3 =new CalendarData("2018-01-28", 7062);
//        CalendarData dataV4 =new CalendarData("2018-01-29", 7069);
//        CalendarData dataV5 =new CalendarData("2018-01-30", 6528);
//        CalendarData dataV6 =new CalendarData("2018-01-31", 3112);
//        CalendarData dataV7 =new CalendarData("2018-02-10", 5401);
//        CalendarData dataV8 =new CalendarData("2018-02-11", 6311);
//        CalendarData dataV9 =new CalendarData("2018-02-15", 8917);
        CalendarData dataV10 =new CalendarData("2018-02-16", 9897);


//        dataVs.add(dataV1);
        dataVs.add(dataV2);
//        dataVs.add(dataV3);
//        dataVs.add(dataV4);
//        dataVs.add(dataV5);
//        dataVs.add(dataV6);
//        dataVs.add(dataV7);
//        dataVs.add(dataV8);
//        dataVs.add(dataV9);
        dataVs.add(dataV10);

        main2.setDataList(dataVs);

        aList.add(main);
        aList.add(main2);

        return aList;
    }

    //获取k线图数据
    public static List<KData>  getKdata(){
        List<Object[]> objects = Arrays.asList(new Object[]{"2013/1/24", 2320.26, 2320.26, 2287.3, 2362.94},
                new Object[]{"2013/1/25", 2300, 2291.3, 2288.26, 2308.38},
                new Object[]{"2013/1/28", 2295.35, 2346.5, 2295.35, 2346.92},
                new Object[]{"2013/1/29", 2347.22, 2358.98, 2337.35, 2363.8},
                new Object[]{"2013/1/30", 2360.75, 2382.48, 2347.89, 2383.76},
                new Object[]{"2013/1/31", 2383.43, 2385.42, 2371.23, 2391.82},
                new Object[]{"2013/2/1", 2377.41, 2419.02, 2369.57, 2421.15},
                new Object[]{"2013/2/4", 2425.92, 2428.15, 2417.58, 2440.38},
                new Object[]{"2013/2/5", 2411, 2433.13, 2403.3, 2437.42},
                new Object[]{"2013/2/6", 2432.68, 2434.48, 2427.7, 2441.73},
                new Object[]{"2013/2/7", 2430.69, 2418.53, 2394.22, 2433.89},
                new Object[]{"2013/2/8", 2416.62, 2432.4, 2414.4, 2443.03},
                new Object[]{"2013/2/18", 2441.91, 2421.56, 2415.43, 2444.8},
                new Object[]{"2013/2/19", 2420.26, 2382.91, 2373.53, 2427.07},
                new Object[]{"2013/2/20", 2383.49, 2397.18, 2370.61, 2397.94},
                new Object[]{"2013/2/21", 2378.82, 2325.95, 2309.17, 2378.82},
                new Object[]{"2013/2/22", 2322.94, 2314.16, 2308.76, 2330.88},
                new Object[]{"2013/2/25", 2320.62, 2325.82, 2315.01, 2338.78},
                new Object[]{"2013/2/26", 2313.74, 2293.34, 2289.89, 2340.71},
                new Object[]{"2013/2/27", 2297.77, 2313.22, 2292.03, 2324.63},
                new Object[]{"2013/2/28", 2322.32, 2365.59, 2308.92, 2366.16},
                new Object[]{"2013/3/1", 2364.54, 2359.51, 2330.86, 2369.65},
                new Object[]{"2013/3/4", 2332.08, 2273.4, 2259.25, 2333.54},
                new Object[]{"2013/3/5", 2274.81, 2326.31, 2270.1, 2328.14},
                new Object[]{"2013/3/6", 2333.61, 2347.18, 2321.6, 2351.44},
                new Object[]{"2013/3/7", 2340.44, 2324.29, 2304.27, 2352.02},
                new Object[]{"2013/3/8", 2326.42, 2318.61, 2314.59, 2333.67},
                new Object[]{"2013/3/11", 2314.68, 2310.59, 2296.58, 2320.96},
                new Object[]{"2013/3/12", 2309.16, 2286.6, 2264.83, 2333.29},
                new Object[]{"2013/3/13", 2282.17, 2263.97, 2253.25, 2286.33},
                new Object[]{"2013/3/14", 2255.77, 2270.28, 2253.31, 2276.22},
                new Object[]{"2013/3/15", 2269.31, 2278.4, 2250, 2312.08},
                new Object[]{"2013/3/18", 2267.29, 2240.02, 2239.21, 2276.05},
                new Object[]{"2013/3/19", 2244.26, 2257.43, 2232.02, 2261.31},
                new Object[]{"2013/3/20", 2257.74, 2317.37, 2257.42, 2317.86},
                new Object[]{"2013/3/21", 2318.21, 2324.24, 2311.6, 2330.81},
                new Object[]{"2013/3/22", 2321.4, 2328.28, 2314.97, 2332},
                new Object[]{"2013/3/25", 2334.74, 2326.72, 2319.91, 2344.89},
                new Object[]{"2013/3/26", 2318.58, 2297.67, 2281.12, 2319.99},
                new Object[]{"2013/3/27", 2299.38, 2301.26, 2289, 2323.48},
                new Object[]{"2013/3/28", 2273.55, 2236.3, 2232.91, 2273.55},
                new Object[]{"2013/3/29", 2238.49, 2236.62, 2228.81, 2246.87},
                new Object[]{"2013/4/1", 2229.46, 2234.4, 2227.31, 2243.95},
                new Object[]{"2013/4/2", 2234.9, 2227.74, 2220.44, 2253.42},
                new Object[]{"2013/4/3", 2232.69, 2225.29, 2217.25, 2241.34},
                new Object[]{"2013/4/8", 2196.24, 2211.59, 2180.67, 2212.59},
                new Object[]{"2013/4/9", 2215.47, 2225.77, 2215.47, 2234.73},
                new Object[]{"2013/4/10", 2224.93, 2226.13, 2212.56, 2233.04},
                new Object[]{"2013/4/11", 2236.98, 2219.55, 2217.26, 2242.48},
                new Object[]{"2013/4/12", 2218.09, 2206.78, 2204.44, 2226.26},
                new Object[]{"2013/4/15", 2199.91, 2181.94, 2177.39, 2204.99},
                new Object[]{"2013/4/16", 2169.63, 2194.85, 2165.78, 2196.43},
                new Object[]{"2013/4/17", 2195.03, 2193.8, 2178.47, 2197.51},
                new Object[]{"2013/4/18", 2181.82, 2197.6, 2175.44, 2206.03},
                new Object[]{"2013/4/19", 2201.12, 2244.64, 2200.58, 2250.11},
                new Object[]{"2013/4/22", 2236.4, 2242.17, 2232.26, 2245.12},
                new Object[]{"2013/4/23", 2242.62, 2184.54, 2182.81, 2242.62},
                new Object[]{"2013/4/24", 2187.35, 2218.32, 2184.11, 2226.12},
                new Object[]{"2013/4/25", 2213.19, 2199.31, 2191.85, 2224.63},
                new Object[]{"2013/4/26", 2203.89, 2177.91, 2173.86, 2210.58},
                new Object[]{"2013/5/2", 2170.78, 2174.12, 2161.14, 2179.65},
                new Object[]{"2013/5/3", 2179.05, 2205.5, 2179.05, 2222.81},
                new Object[]{"2013/5/6", 2212.5, 2231.17, 2212.5, 2236.07},
                new Object[]{"2013/5/7", 2227.86, 2235.57, 2219.44, 2240.26},
                new Object[]{"2013/5/8", 2242.39, 2246.3, 2235.42, 2255.21},
                new Object[]{"2013/5/9", 2246.96, 2232.97, 2221.38, 2247.86},
                new Object[]{"2013/5/10", 2228.82, 2246.83, 2225.81, 2247.67},
                new Object[]{"2013/5/13", 2247.68, 2241.92, 2231.36, 2250.85},
                new Object[]{"2013/5/14", 2238.9, 2217.01, 2205.87, 2239.93},
                new Object[]{"2013/5/15", 2217.09, 2224.8, 2213.58, 2225.19},
                new Object[]{"2013/5/16", 2221.34, 2251.81, 2210.77, 2252.87},
                new Object[]{"2013/5/17", 2249.81, 2282.87, 2248.41, 2288.09},
                new Object[]{"2013/5/20", 2286.33, 2299.99, 2281.9, 2309.39},
                new Object[]{"2013/5/21", 2297.11, 2305.11, 2290.12, 2305.3},
                new Object[]{"2013/5/22", 2303.75, 2302.4, 2292.43, 2314.18},
                new Object[]{"2013/5/23", 2293.81, 2275.67, 2274.1, 2304.95},
                new Object[]{"2013/5/24", 2281.45, 2288.53, 2270.25, 2292.59},
                new Object[]{"2013/5/27", 2286.66, 2293.08, 2283.94, 2301.7},
                new Object[]{"2013/5/28", 2293.4, 2321.32, 2281.47, 2322.1},
                new Object[]{"2013/5/29", 2323.54, 2324.02, 2321.17, 2334.33},
                new Object[]{"2013/5/30", 2316.25, 2317.75, 2310.49, 2325.72},
                new Object[]{"2013/5/31", 2320.74, 2300.59, 2299.37, 2325.53},
                new Object[]{"2013/6/3", 2300.21, 2299.25, 2294.11, 2313.43},
                new Object[]{"2013/6/4", 2297.1, 2272.42, 2264.76, 2297.1},
                new Object[]{"2013/6/5", 2270.71, 2270.93, 2260.87, 2276.86},
                new Object[]{"2013/6/6", 2264.43, 2242.11, 2240.07, 2266.69},
                new Object[]{"2013/6/7", 2242.26, 2210.9, 2205.07, 2250.63},
                new Object[]{"2013/6/13", 2190.1, 2148.35, 2126.22, 2190.1});

        List<KData> list =new ArrayList<KData>();
        KData kData =null;
        Object[] objectArr=null;
        for(int i=0;i<objects.size();i++){
            objectArr  = objects.get(i);
            kData =new KData((String)objectArr[0],new BigDecimal(objectArr[1].toString()),new BigDecimal(objectArr[2].toString()),new BigDecimal(objectArr[3].toString()),new BigDecimal(objectArr[4].toString()));
            list.add(kData);
        }

        return list;

    }

}