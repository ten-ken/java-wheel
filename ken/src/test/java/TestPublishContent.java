import com.ken.sys.common.dto.Article;
import com.ken.sys.common.util.BigDecimalCalcUtil;
import com.ken.sys.common.util.HtmlToMarkdownUtil;
import com.ken.sys.common.util.RegExUtil;
import com.overzealous.remark.Remark;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.*;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;

/**
 * <ul>
 * <li>Title: TestPublishContent</li>
 * <li>Description: TODO </li>
 * </ul>
 *
 * @author swc
 * @date 2020/7/18 0018 下午 15:51
 */
public class TestPublishContent {

    //作者（用户）主页
    public final static String host ="https://blog.csdn.net/${authorCode}/";
    //作者（用户）文章列表
    public final static String restArticleList ="article/list/${page}";
    //作者（用户）文章详情
    public final static String restArticleDetail ="article/details/${id}";
    //作者（用户）唯一标识
    public final static String authorCode ="jikuicui7402";
//    public final static String authorCode ="dyc87112";

    public  static Remark remark;

    static {
        remark = new Remark();
    }


    public static void main(String[] args) throws Exception{
        String outFolder ="D:\\article\\csdn\\";
        // 抓取cdsn 文章内容将其改为markdown 格式 抓取的文章写入本地
//        spiderContent(authorCode,outFolder,1,null);

        //抓取当前系统某个文件夹下的html 【仅支持一级目录】 将其转为markdown语法 并输出
        String folder="C:\\Users\\Administrator\\Desktop\\yinxiang";
        createFolderMardownFile(folder,outFolder);

    }

    /**
     * 功能描述: 抓取当前系统某个文件夹下的html 【仅支持一级目录】 将其转为markdown语法 并输出
     * @param folder
     * @param outFolder
     * @return: void
     * @author: swc
     * @date: 2020/7/22 0022 下午 15:11
     */
    private static void createFolderMardownFile(String folder,String outFolder) {
        File file =new File(folder);

        File[] fs = file.listFiles();
        for (File fn :fs) {
            if(fn.getName().endsWith(".html")){
                String content = remark.convert(readFile(fn));
                writeToTXT(outFolder,fn.getName(),fn.getName(),content);
            }
        }
    }

    private static void spiderContent(String authorCode,String writeInFolder, Integer currentPage, Integer totalPage) {
        currentPage = currentPage==null || currentPage.compareTo(0)<0?1:currentPage;
        totalPage = totalPage==null || totalPage.compareTo(0)<=0?null:totalPage;
        try {
            //先获得的是整个页面的html标签页面
            String url =host.replace("${authorCode}", authorCode)+restArticleList.replace("${page}",String.valueOf(currentPage));
            Document doc = Jsoup.connect(url).get();

            Elements btEl = doc.getElementsByTag("main");

            String prefix =host.replace("${authorCode}", authorCode)+restArticleDetail.replace("${id}","");
            Elements hrefElements = btEl.get(0).getElementsByAttributeValueStarting("href",prefix);

            Map<String,String> htmlMappingMarkDown = HtmlToMarkdownUtil.htmlMappingMarkDown();

            if(totalPage==null){
                Elements scriptEles = doc.getElementsByTag("script");
                String varReg1 = "(pageSize|pagesize)[\\s|\\S]{1,3}(\\d){1,4}";
                String varReg2 = "(listTotal|listtotal)[\\s|\\S]{1,7}(\\d)";
                Matcher matcher1 =null;
                Matcher matcher2 =null;
                String scriptContent ="";
                String var1 ="0";
                String var2 ="0";
                for (int i = 0; i < scriptEles.size(); i++) {
                    scriptContent = scriptEles.get(i).html();
                    if(scriptContent.contains("pageSize") && scriptContent.contains("listTotal")){
                        matcher1= RegExUtil.setPublicConfig(varReg1, scriptContent);
                        matcher2 = RegExUtil.setPublicConfig(varReg2, scriptContent);
                        if(matcher1.find()){
                            var1 = matcher1.group().replaceAll("(pageSize|pagesize|=|\\s)","");
                        }
                        if(matcher2.find()){
                            var2 =matcher2.group().replaceAll("(listTotal|listtotal|=|\\s)","");
                        }
                    }
                }

                BigDecimal page = BigDecimalCalcUtil.div(new BigDecimal(var2),new BigDecimal(var1),1);
                page = page.setScale(0,BigDecimal.ROUND_UP);
                //总页数 -->totalPage
                totalPage = page.intValue();
            }

            Map<String,String> urlMap =new HashMap<String,String>();
            String href ="";
            List<Article> list =new ArrayList<Article>();
            Article article =null;

            System.out.println("第"+currentPage+"页数据正在抓取......");
            for (int i = 0; i < hrefElements.size(); i++) {
                href = hrefElements.get(i).attributes().get("href");
                if(!urlMap.containsKey(href)){
                    getArticleDetail(href,htmlMappingMarkDown,writeInFolder);
                }
                urlMap.put(href,"");
            }
            urlMap.clear();
            System.out.println("***第"+currentPage+"页数据已抓取完成***");

            //从第二页开始
            if(currentPage<totalPage){
                currentPage++;
                spiderContent(authorCode,writeInFolder, currentPage,totalPage);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    //https://www.jianshu.com/author/notebooks 文章分类接口
    //https://www.jianshu.com/author/notebooks/26849990/notes 文章列表
    //https://www.jianshu.com/author/notes  新增文章
    //结果 {"id":74071007,"title":"2020-07-18","slug":"be8b521fbb6a","shared":false,"notebook_id":26849990,"seq_in_nb":-4,"note_type":2,"autosave_control":0,"content_updated_at":1595062140,"last_compiled_at":0}
    //#notebook_id  # id
    //https://www.jianshu.com/author/notes/#{id}  实际给文章添加标题和内容
    //传参   //{id: "74071007", autosave_control: 1, title: "2020-07-18", content: "222"}
    private static void getArticleDetail(String url,Map<String,String> htmlMappingMarkDown,String folder) {
        try {
            Document doc = Jsoup.connect(url).get();
            Elements btEl = doc.getElementsByClass("title-article");
            String title = btEl.text();

            Element element = doc.getElementById("content_views");

//          String content = HtmlToMarkdownUtil.toMarkdown(element,htmlMappingMarkDown);

            String content = remark.convert(element.outerHtml());

            //"D:\\article\\csdn\\"
            writeToTXT(folder,title,title,content);
//            article  =new Article();
//            article.setTitle(title);
//            article.setContent(content);
//            list.add(article);
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }



    /**
     * <pre>
     * 把内容写入TXT文件
     * </pre>
     *
     * @param content 要写入的内容
     *
     */
    public static void writeToTXT(String filePath,String fileName,String title,String content) {
        FileWriter fw = null;
        BufferedWriter bw = null;

        StringBuffer sbf =new StringBuffer();
        sbf.append(title+"\r\n");
        sbf.append(content);

        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");

        String childFolder = sdf.format(date); // 以当前时间的的格式化文件名

        fileName = fileName.replaceAll("[<>/\\\\|:\"\\*\\?|\\s*]{1,5}","");

        String path = filePath+childFolder+File.separator+fileName+".txt"; // 保存的路径和文件名

        try {
            File file = new File(path);
            if (!file.getParentFile().exists()) { // 父目录不存在
                file.mkdirs();
            }
            if (!file.exists()) { // 若文件不存在，则新建一个
                file.createNewFile();
            }
            fw = new FileWriter(file);
            bw = new BufferedWriter(fw);
            bw.write(sbf.toString()); // 写入内容
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                bw.close();
                fw.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    public static String readFile(File file) {
        StringBuffer sbf =new StringBuffer();
        Reader reader = null;
        try {
//            System.out.println("以字符为单位读取文件内容，一次读一个字节：");
            // 一次读一个字符
            reader = new InputStreamReader(new FileInputStream(file));
            int tempchar;
            while ((tempchar = reader.read()) != -1) {
                // 对于windows下，\r\n这两个字符在一起时，表示一个换行。
                sbf.append((char) tempchar);
//                if (((char) tempchar) != '\r') {
//                    System.out.print((char) tempchar);
//                }
            }
            reader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sbf.toString();

    }
}
