import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ken.sys.common.entity.TInStockInfo;
import com.ken.sys.common.util.EntityToSqlUtil;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author swc  测试自动生成sql
 * @date 2020/5/28 0028 下午 16:38
 */
public class TestGenerateMySql {

    @Test
    public void testCreateTable(){
        Class[] classes =new Class[]{TInStockInfo.class};
        EntityToSqlUtil.generateMySql(classes);
    }

    @Test
    public void testCreateSql(){
        JSONArray jsonArray =new JSONArray();

        JSONObject j1=new JSONObject();
        j1.put("nums",new BigDecimal("30"));
        j1.put("curdate",new Date());
        j1.put("remark","就是做测试呢");

        JSONObject j2=new JSONObject();
        j2.put("nums",new BigDecimal("335"));
        j2.put("remark","哈哈哈");
        j2.put("curdate",new Date());

        JSONObject j3=new JSONObject();
        j3.put("nums",new BigDecimal("150"));
        j3.put("remark","呵呵哒");
        j3.put("curdate",new Date());

        jsonArray.add(j1);
        jsonArray.add(j2);
        jsonArray.add(j3);

        String s = EntityToSqlUtil.creatInsertSql(jsonArray, TInStockInfo.class);
        System.out.println(s);
    }

//    List<TInStockInfo> list =new ArrayList<>();
//
//        TInStockInfo t1=new TInStockInfo();
//        t1.setInStockNums(new BigDecimal("200"));
//        t1.setUseDate(new Date());
//
//        TInStockInfo t2=new TInStockInfo();
//        t2.setInStockNums(new BigDecimal("196"));
//        t2.setRemark("196的备注");
//
//        TInStockInfo t3=new TInStockInfo();
//        t3.setInStockNums(new BigDecimal("329"));
//        t3.setRemark("329的备注，你看看");
//
//        list.add(t1);
//        list.add(t2);
//        list.add(t3);
}
