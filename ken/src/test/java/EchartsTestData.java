//
//import com.ken.sys.common.util.EmptyUtils;
//
//import java.util.HashMap;
//import java.util.Map;
//
///**
// * <ul>
// * <li>Title: EchartsTestData</li>
// * <li>Description: echarts测试数据 </li>
// * <li>Copyright: Copyright (c) 2018</li>
// * <li>Company: http://www.jiangqiaotech.com/</li>
// * </ul>
// *
// * @author swc
// * @date 2020/7/6 0006 下午 14:38
// */
//public class EchartsTestData {
//
//    public static void main(String[] args) {
//        String a1 ="\n" +
//                "type: 'bar' ,\n" +
//                "id ... ,\n" +
//                "name ... ,\n" +
//                "legendHoverLink: true ,\n" +
//                "coordinateSystem: 'cartesian2d' ,\n" +
//                "xAxisIndex: 0 ,\n" +
//                "yAxisIndex: 0 ,\n" +
//                "roundCap: false ,\n" +
//                "label: {...} ,\n" +
//                "itemStyle: {...} ,\n" +
//                "showBackground: false ,\n" +
//                "backgroundStyle: {...} ,\n" +
//                "emphasis: {...} ,\n" +
//                "stack ... ,\n" +
//                "cursor: 'pointer' ,\n" +
//                "barWidth: 自适应 ,\n" +
//                "barMaxWidth ... ,\n" +
//                "barMinWidth ... ,\n" +
//                "barMinHeight: 0 ,\n" +
//                "barGap: 30% ,\n" +
//                "barCategoryGap: '20%' ,\n" +
//                "large: false ,\n" +
//                "largeThreshold: 400 ,\n" +
//                "progressive: 5000 ,\n" +
//                "progressiveThreshold: 3000 ,\n" +
//                "progressiveChunkMode: mod ,\n" +
//                "dimensions ... ,\n" +
//                "encode ... ,\n" +
//                "seriesLayoutBy: 'column' ,\n" +
//                "datasetIndex: 0 ,\n" +
//                "data: [{...}] ,\n" +
//                "markPoint: {...} ,\n" +
//                "markLine: {...} ,\n" +
//                "markArea: {...} ,\n" +
//                "clip: true ,\n" +
//                "zlevel: 0 ,\n" +
//                "z: 2 ,\n" +
//                "silent: false ,\n" +
//                "animation: true ,\n" +
//                "animationThreshold: 2000 ,\n" +
//                "animationDuration: 1000 ,\n" +
//                "animationEasing: cubicOut ,\n" +
//                "animationDelay: 0 ,\n" +
//                "animationDurationUpdate: 300 ,\n" +
//                "animationEasingUpdate: cubicOut ,\n" +
//                "animationDelayUpdate: 0 ,\n" +
//                "tooltip: {...} ,";
//
//        String a2 ="\n" +
//                "type: 'bar' ,\n" +
//                "id ... ,\n" +
//                "name ... ,\n" +
//                "legendHoverLink: true ,\n" +
//                "coordinateSystem: 'cartesian2d' ,\n" +
//                "xAxisIndex: 0 ,\n" +
//                "yAxisIndex: 0 ,\n" +
//                "roundCap: false ,\n" +
//                "label: {...} ,\n" +
//                "itemStyle: {...} ,\n" +
//                "showBackground: false ,\n" +
//                "backgroundStyle: {...} ,\n" +
//                "emphasis: {...} ,\n" +
//                "stack ... ,\n" +
//                "cursor: 'pointer' ,\n" +
//                "barWidth: 自适应 ,\n" +
//                "barMaxWidth ... ,\n" +
//                "barMinWidth ... ,\n" +
//                "barMinHeight: 0 ,\n" +
//                "barGap: 30% ,\n" +
//                "barCategoryGap: '20%' ,\n" +
//                "large: false ,\n" +
//                "largeThreshold: 400 ,\n" +
//                "progressive: 5000 ,\n" +
//                "progressiveThreshold: 3000 ,\n" +
//                "progressiveChunkMode: mod ,\n" +
//                "dimensions ... ,\n" +
//                "encode ... ,\n" +
//                "seriesLayoutBy: 'column' ,\n" +
//                "datasetIndex: 0 ,\n" +
//                "data: [{...}] ,\n" +
//                "markPoint: {...} ,\n" +
//                "markLine: {...} ,\n" +
//                "markArea: {...} ,\n" +
//                "clip: true ,\n" +
//                "zlevel: 0 ,\n" +
//                "z: 2 ,\n" +
//                "silent: false ,\n" +
//                "animation: true ,\n" +
//                "animationThreshold: 2000 ,\n" +
//                "animationDuration: 1000 ,\n" +
//                "animationEasing: cubicOut ,\n" +
//                "animationDelay: 0 ,\n" +
//                "animationDurationUpdate: 300 ,\n" +
//                "animationEasingUpdate: cubicOut ,\n" +
//                "animationDelayUpdate: 0 ,\n" +
//                "tooltip: {...} ,";
//
//        String a3 ="\n" +
//                "type: 'pie' ,\n" +
//                "id ... ,\n" +
//                "name ... ,\n" +
//                "legendHoverLink: true ,\n" +
//                "hoverAnimation: true ,\n" +
//                "hoverOffset: 10 ,\n" +
//                "selectedMode: false ,\n" +
//                "selectedOffset: 10 ,\n" +
//                "clockwise: true ,\n" +
//                "startAngle: 90 ,\n" +
//                "minAngle: 0 ,\n" +
//                "minShowLabelAngle: 0 ,\n" +
//                "roseType: false ,\n" +
//                "avoidLabelOverlap: true ,\n" +
//                "stillShowZeroSum: true ,\n" +
//                "cursor: 'pointer' ,\n" +
//                "zlevel: 0 ,\n" +
//                "z: 2 ,\n" +
//                "left: 0 ,\n" +
//                "top: 0 ,\n" +
//                "right: 0 ,\n" +
//                "bottom: 0 ,\n" +
//                "width: 'auto' ,\n" +
//                "height: 'auto' ,\n" +
//                "label: {...} ,\n" +
//                "labelLine: {...} ,\n" +
//                "itemStyle: {...} ,\n" +
//                "emphasis: {...} ,\n" +
//                "center: ['50%', '50%'] ,\n" +
//                "radius: [0, '75%'] ,\n" +
//                "seriesLayoutBy: 'column' ,\n" +
//                "datasetIndex: 0 ,\n" +
//                "dimensions ... ,\n" +
//                "encode ... ,\n" +
//                "data: [{...}] ,\n" +
//                "markPoint: {...} ,\n" +
//                "markLine: {...} ,\n" +
//                "markArea: {...} ,\n" +
//                "silent: false ,\n" +
//                "animationType: 'expansion' ,\n" +
//                "animationTypeUpdate: 'transition' ,\n" +
//                "animation: true ,\n" +
//                "animationThreshold: 2000 ,\n" +
//                "animationDuration: 1000 ,\n" +
//                "animationEasing: cubicOut ,\n" +
//                "animationDelay: 0 ,\n" +
//                "animationDurationUpdate: 300 ,\n" +
//                "animationEasingUpdate: cubicOut ,\n" +
//                "animationDelayUpdate: 0 ,\n" +
//                "tooltip: {...} ,";
//
//        String  a4 ="\n" +
//                "type: 'funnel' ,\n" +
//                "id ... ,\n" +
//                "name ... ,\n" +
//                "min: 0 ,\n" +
//                "max: 100 ,\n" +
//                "minSize: '0%' ,\n" +
//                "maxSize: '100%' ,\n" +
//                "sort: 'descending' ,\n" +
//                "gap: 0 ,\n" +
//                "legendHoverLink: true ,\n" +
//                "funnelAlign: 'center' ,\n" +
//                "label: {...} ,\n" +
//                "labelLine: {...} ,\n" +
//                "itemStyle: {...} ,\n" +
//                "emphasis: {...} ,\n" +
//                "zlevel: 0 ,\n" +
//                "z: 2 ,\n" +
//                "left: 80 ,\n" +
//                "top: 60 ,\n" +
//                "right: 80 ,\n" +
//                "bottom: 60 ,\n" +
//                "width: 'auto' ,\n" +
//                "height: 'auto' ,\n" +
//                "seriesLayoutBy: 'column' ,\n" +
//                "datasetIndex: 0 ,\n" +
//                "dimensions ... ,\n" +
//                "encode ... ,\n" +
//                "data: [{...}] ,\n" +
//                "markPoint: {...} ,\n" +
//                "markLine: {...} ,\n" +
//                "markArea: {...} ,\n" +
//                "silent: false ,\n" +
//                "animation: true ,\n" +
//                "animationThreshold: 2000 ,\n" +
//                "animationDuration: 1000 ,\n" +
//                "animationEasing: cubicOut ,\n" +
//                "animationDelay: 0 ,\n" +
//                "animationDurationUpdate: 300 ,\n" +
//                "animationEasingUpdate: cubicOut ,\n" +
//                "animationDelayUpdate: 0 ,\n" +
//                "tooltip: {...} ,";
//
//
//        String a5 ="\n" +
//                "type: 'gauge' ,\n" +
//                "id ... ,\n" +
//                "name ... ,\n" +
//                "radius: '75%' ,\n" +
//                "legendHoverLink: true ,\n" +
//                "startAngle: 225 ,\n" +
//                "endAngle: -45 ,\n" +
//                "clockwise: true ,\n" +
//                "data: [{...}] ,\n" +
//                "min: 0 ,\n" +
//                "max: 100 ,\n" +
//                "splitNumber: 10 ,\n" +
//                "axisLine: {...} ,\n" +
//                "splitLine: {...} ,\n" +
//                "axisTick: {...} ,\n" +
//                "axisLabel: {...} ,\n" +
//                "pointer: {...} ,\n" +
//                "itemStyle: {...} ,\n" +
//                "emphasis: {...} ,\n" +
//                "title: {...} ,\n" +
//                "detail: {...} ,\n" +
//                "markPoint: {...} ,\n" +
//                "markLine: {...} ,\n" +
//                "markArea: {...} ,\n" +
//                "silent: false ,\n" +
//                "animation: true ,\n" +
//                "animationThreshold: 2000 ,\n" +
//                "animationDuration: 1000 ,\n" +
//                "animationEasing: cubicOut ,\n" +
//                "animationDelay: 0 ,\n" +
//                "animationDurationUpdate: 300 ,\n" +
//                "animationEasingUpdate: cubicOut ,\n" +
//                "animationDelayUpdate: 0 ,\n" +
//                "tooltip: {...} ,";
//
//
//        String a6 ="\n" +
//                "type: 'heatmap' ,\n" +
//                "id ... ,\n" +
//                "name ... ,\n" +
//                "coordinateSystem: 'cartesian2d' ,\n" +
//                "xAxisIndex: 0 ,\n" +
//                "yAxisIndex: 0 ,\n" +
//                "geoIndex: 0 ,\n" +
//                "calendarIndex: 0 ,\n" +
//                "pointSize: 20 ,\n" +
//                "blurSize: 20 ,\n" +
//                "minOpacity: 0 ,\n" +
//                "maxOpacity: 1 ,\n" +
//                "progressive: 400 ,\n" +
//                "progressiveThreshold: 3000 ,\n" +
//                "label: {...} ,\n" +
//                "itemStyle: {...} ,\n" +
//                "emphasis: {...} ,\n" +
//                "data: [{...}] ,\n" +
//                "markPoint: {...} ,\n" +
//                "markLine: {...} ,\n" +
//                "markArea: {...} ,\n" +
//                "zlevel: 0 ,\n" +
//                "z: 2 ,\n" +
//                "silent: false ,\n" +
//                "tooltip: {...} ,";
//
//        String [] arr1 =a1.split(",");
//        String [] arr2 =a2.split(",");
//        String [] arr3 =a3.split(",");
//        String [] arr4 =a4.split(",");
//        String [] arr5 =a5.split(",");
//        String [] arr6 =a6.split(",");
//
//        Map<String,Integer> map =new HashMap<>();
//
////        Map<String,Integer> countMap =new HashMap<String,Integer>();
//
////        List<String[]> strings = Arrays.asList(arr1, arr2, arr3, arr4, arr5, arr6);
//        // 合并六个数组
//        String[] ee = new String[arr1.length + arr2.length + arr3.length+ arr4.length+ arr5.length+ arr6.length];
//        System.arraycopy(arr1, 0, ee, 0, arr1.length);
//        System.arraycopy(arr2, 0, ee, arr1.length, arr2.length);
//        System.arraycopy(arr3, 0, ee, arr1.length + arr2.length, arr3.length);
//        System.arraycopy(arr4, 0, ee, arr1.length + arr2.length+arr3.length,arr4.length);
//        System.arraycopy(arr5, 0, ee, arr1.length + arr2.length+arr3.length+arr4.length,arr5.length);
//        System.arraycopy(arr6, 0, ee, arr1.length + arr2.length+arr3.length+arr4.length+arr5.length,arr6.length);
//
//
//        for(String str1:ee){
//            if(map.containsKey(str1.trim())){
//                map.put(str1.trim(),map.get(str1.trim())+1);
//                continue;
//            }
//            map.put(str1.trim(),0);
//        }
//
//
//        for(String key:map.keySet()){
//            if(map.get(key)>=5 && !EmptyUtils.isNullOrEmpty(key)){
//                System.out.println(key+"---"+map.get(key));
//            }
//        }
//
//    }
//}
