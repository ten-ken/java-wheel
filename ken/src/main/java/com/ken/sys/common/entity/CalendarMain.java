/******************************************************************************
 *
 *
 *
 ******************************************************************************
 *
 *****************************************************************************/
package com.ken.sys.common.entity;

import java.util.Calendar;
import java.util.List;

/**
 * <ul>
 * <li>Title: CalendarMain</li>
 * <li>Description: TODO </li>
 * <li>Copyright: Copyright (c) 2018</li>
 * <li>微信:tianji_vip</li>
 * </ul>
 *
 * @author swc
 *
 * @date 2020/7/2 0002 下午 14:37
 */
public class CalendarMain {

    private List<CalendarData> dataList;

    public List<CalendarData> getDataList() {
        return dataList;
    }

    public void setDataList(List<CalendarData> dataList) {
        this.dataList = dataList;
    }
}
