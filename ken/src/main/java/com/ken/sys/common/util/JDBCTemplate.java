package com.ken.sys.common.util;

import com.ken.sys.common.anno.KTableField;
import com.ken.sys.common.anno.KTableName;
import com.ken.sys.common.entity.Area;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * <ul>
 * <li>Title: JDBCTemplate</li>
 * <li>Description: TODO </li>
 * </ul>
 *
 * @author swc
 * @date 2020/7/17 0017 上午 9:44
 */
public class JDBCTemplate {

    public final static String ADD ="add";

    public final static String UPDATE ="update";

    public final static String DELETE ="delete";

    public final static String SELECT ="select";

    public final static String SELECT_COUNT ="select_Count";

    public static Execute execute ;

    public   <T> Integer  selectCountBySql(String sql,T t) throws Exception {
        return execute.selectCountBySql(sql,t);
    }

    public <T> List<T> selectBySql(String sql, Class<T> tClass) throws Exception {
        execute = new JDBCTemplate.Execute();
        return execute.selectListBySql(sql,tClass);
    }

    public <T,B> List<B> selectBySql(String sql,T t ,Class<B> tClass) throws Exception {
        execute = new JDBCTemplate.Execute();
        return execute.selectListBySql(sql,t,tClass);
    }

    public <T> boolean save(String sql, T t) throws Exception {
        execute = new JDBCTemplate.Execute();
        return execute.executeSql(sql,t);
    }

    public  <T> boolean update(String sql, T t) throws Exception {
        execute = new JDBCTemplate.Execute();
        return execute.executeSql(sql,t);
    }
    public <T> boolean delete(String sql, T t) throws Exception {
        execute = new JDBCTemplate.Execute();
        return execute.executeSql(sql,t);
    }

    public  <T> int  selectCount(T t) throws Exception {
        execute = new JDBCTemplate.Execute();
        Object o = execute.executeSql(t, JDBCTemplate.SELECT_COUNT);
        return (Integer)o;
    }

    public  <T> T  selectOneByObj(T t) throws Exception {
        execute = new JDBCTemplate.Execute();
        List<T> list = (List<T>)execute.executeSql(t, JDBCTemplate.SELECT);
        return list.get(0);
    }

    public  <T> List<T>  selectListByObj(T t) throws Exception {
        execute = new JDBCTemplate.Execute();
        List<T> list = (List<T>)execute.executeSql(t, JDBCTemplate.SELECT);
        return list;
    }

    public   <T> boolean  insertByObj(T t) throws Exception {
        execute = new JDBCTemplate.Execute();
        return execute.executeByObj(t,JDBCTemplate.ADD);
    }
    public   <T> boolean  insertBatch(List<T> list) throws Exception {
        execute = new JDBCTemplate.Execute();
        return execute.executeByList(list,JDBCTemplate.ADD);
    }

    public   <T> boolean  updateByObj(T t) throws Exception {
        execute = new JDBCTemplate.Execute();
        return execute.executeByObj(t,JDBCTemplate.UPDATE);
    }

    public   <T> boolean  updateBatch(List<T> list) throws Exception {
        execute = new JDBCTemplate.Execute();
        return execute.executeByList(list,JDBCTemplate.UPDATE);
    }

    public   <T> boolean  deleteById(T t) throws Exception {
        execute = new JDBCTemplate.Execute();
        return execute.executeByObj(t,JDBCTemplate.DELETE);
    }

    public   <T> boolean  deleteBatch(List<T> list) throws Exception {
        return execute.executeByList(list,JDBCTemplate.DELETE);
    }


    //内部类
    class Execute{
        /**
         * 功能描述: 执行查询sql 只返回一条结果
         * @param t
         * @param executeType
         * @return: boolean
         * @author: swc
         * @date: 2020/7/17 0017 下午 16:49
         */
        private   <T> boolean  executeByObj(T t,String executeType) throws Exception {
            boolean flag = false;
            Connection connection =null;
            PreparedStatement preparedStatement =null;
            try {
                connection =JdbcUtil.getConnection();
                String currentSql= createSql(t,0,executeType);
                preparedStatement = connection.prepareStatement(currentSql);

                int result = preparedStatement.executeUpdate();
                flag = result > 0 ? true : false;
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                JdbcUtil.closeConnection(connection,preparedStatement,null);
            }
            return flag;
        }


        /**
         * 功能描述: 执行新增 修改 删除的具体实现方法
         * @param list
         * @param executeType
         * @return: boolean
         * @author: swc
         * @date: 2020/7/17 0017 下午 16:44
         */
        private   <T> boolean  executeByList(List<T> list,String executeType) throws Exception {
            boolean flag = false;
            Connection connection =null;
            PreparedStatement preparedStatement =null;
            try {
                connection =JdbcUtil.getConnection();
//                String currentSql= "";
                StringBuffer sbf =new StringBuffer();
                for (int i = 0; i < list.size(); i++) {
                    sbf.append(createSql(list.get(i),i,executeType));
//                    currentSql= createSql(list.get(i),i,executeType);
                }
                preparedStatement = connection.prepareStatement(sbf.toString());
                preparedStatement.addBatch();
                int[] result = preparedStatement.executeBatch();
                flag = result.length > 0 ? true : false;
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                JdbcUtil.closeConnection(connection,preparedStatement,null);
            }
            return flag;
        }



        /**
         * 功能描述: 更加不同的类型 创建不同的sql语句
         * @param t
         * @param ind
         * @param execType
         * @return: java.lang.String
         * @author: swc
         * @date: 2020/7/17 0017 下午 16:43
         */
        private  <T> String createSql(T t,int ind,String execType) {
            String sql ="";
            if(JDBCTemplate.ADD.equalsIgnoreCase(execType)){
                sql =createInsertSql(t,ind);
            }else if(JDBCTemplate.UPDATE.equalsIgnoreCase(execType)){
                sql = createUpdateSqlByPrimary(t,ind);
            }else if(JDBCTemplate.DELETE.equalsIgnoreCase(execType)){
                sql = createDeleteSql(t,ind);
            }
            else if(JDBCTemplate.SELECT.equalsIgnoreCase(execType)){
                sql = createSelectSql(t);
            }
            else if(JDBCTemplate.SELECT_COUNT.equalsIgnoreCase(execType)){
                sql = createCountSql(t);
            }
            return sql;
        }

        /**
         * 功能描述: 根据实体 生成select 语句
         * @param t
         * @return: java.lang.String
         * @author: swc
         * @date: 2020/7/17 0017 下午 16:43
         */
        private <T> String createSelectSql(T t) {
            String template ="select ${tableColums} from ${tableName} ";
            String  condition ="where ${condition}";

            StringBuffer conditionBf =new StringBuffer();
            StringBuffer tableColums =new StringBuffer();

            Field[] accessibleField = ReflectUtils.getAccessibleField(t);

            KTableName KTableName = t.getClass().getAnnotation(KTableName.class);

            if(KTableName!=null){
                template = template.replace("${tableName}",KTableName.value());

            }
            KTableField KTableField =null;
            Object value = null;
            String columName="";
            for (int i = 0; i <accessibleField.length ; i++) {
                KTableField = accessibleField[i].getAnnotation(KTableField.class);
                if(KTableField==null || KTableField.exist()){
                    value = ReflectUtils.getFieldValue(t,accessibleField[i].getName());
                    columName = KTableField!=null && !EmptyUtils.isNullOrEmpty(KTableField.alias())
                            ?KTableField.alias():accessibleField[i].getName();
                    if(!EmptyUtils.isNullOrEmpty(value)  ){
                        conditionBf.append(columName+"='"+value+"' and ");
                    }
                    tableColums.append(columName+",");
                }
            }

            tableColums = tableColums.replace(tableColums.length()-1,tableColums.length(),"");
            template = template.replace("${tableColums}",tableColums);

            if(!EmptyUtils.isNullOrEmpty(conditionBf)){
                condition = condition.replace("${condition}",conditionBf);
                condition = condition.substring(0,condition.lastIndexOf("and"));
                return template+condition;
            }
            return template;


        }

        /**
         * 功能描述: 根据实体 生成select 语句 查询 总条数/总记录数
         * @param t
         * @return: java.lang.String
         * @author: swc
         * @date: 2020/7/17 0017 下午 16:42
         */
        private <T> String createCountSql(T t) {
            String template ="select count(0) from ${tableName} ";
            String  condition ="where ${condition}";

            StringBuffer conditionBf =new StringBuffer();

            Field[] accessibleField = ReflectUtils.getAccessibleField(t);

            KTableName KTableName = t.getClass().getAnnotation(KTableName.class);

            if(KTableName!=null){
                template = template.replace("${tableName}",KTableName.value());

            }

            KTableField KTableField =null;
            Object value = null;
//            for (int i = 0; i <accessibleField.length ; i++) {
//                KTableField = accessibleField[i].getAnnotation(KTableField.class);
//                if(KTableField!=null ){
//                    value = ReflectUtils.getFieldValue(t,accessibleField[i].getName());
//                    conditionBf.append(EmptyUtils.isNullOrEmpty(value)?"":KTableField.alias()+"='"+value+"' and ");
//                }
//            }
            String columName ="";
            for (int i = 0; i <accessibleField.length ; i++) {
                KTableField = accessibleField[i].getAnnotation(KTableField.class);
                if(KTableField==null || KTableField.exist()){
                    value = ReflectUtils.getFieldValue(t,accessibleField[i].getName());
                    if(!EmptyUtils.isNullOrEmpty(value)  ){
                        columName = KTableField!=null && !EmptyUtils.isNullOrEmpty(KTableField.alias())
                                ?KTableField.alias():accessibleField[i].getName();
                        conditionBf.append(columName+"='"+value+"' and ");
                    }
                }
            }

            if(!EmptyUtils.isNullOrEmpty(conditionBf)){
                condition = condition.replace("${condition}",conditionBf);
                condition = condition.substring(0,condition.lastIndexOf("and"));
                return template+condition;
            }
            return template;
        }

        /**
         * 功能描述: 根据实体 生成delete语句
         * @param t
         * @param ind
         * @return: java.lang.String
         * @author: swc
         * @date: 2020/7/17 0017 下午 16:42
         */
        private  <T> String createDeleteSql(T t, int ind) {
            String head =ind==0?"":";";

            String template ="delete from ${tableName} where ${primaryKey} = #{primaryKey}";
            StringBuffer primaryKey =new StringBuffer();

            Field[] accessibleField = ReflectUtils.getAccessibleField(t);

            KTableName KTableName = t.getClass().getAnnotation(KTableName.class);

            if(KTableName!=null){
                template = template.replace("${tableName}",KTableName.value());
            }
            KTableField KTableField =null;

            for (int i = 0; i <accessibleField.length ; i++) {
                KTableField = accessibleField[i].getAnnotation(KTableField.class);
                if(KTableField!=null && KTableField.isPrimary()){
                    template = template.replace("${primaryKey}",KTableField.alias());
                    primaryKey.append("'"+ReflectUtils.getFieldValue(t,accessibleField[i].getName())+"'");
                }
            }

            return head+template.replace("#{primaryKey}",primaryKey);
        }

        /**
         * 功能描述: 根据实体 生成update语句
         * @param t
         * @return: java.lang.String
         * @author: swc
         * @date: 2020/7/17 0017 下午 16:41
         */
        private  <T> String createUpdateSqlByPrimary(T t,int ind) {
            String template ="update ${tableName} set ${setSql}  where ${primaryKey} = #{primaryKey}" ;
            StringBuffer setSql =new StringBuffer();
            StringBuffer primaryKey =new StringBuffer();

            Field[] accessibleField = ReflectUtils.getAccessibleField(t);

            KTableName KTableName = t.getClass().getAnnotation(KTableName.class);

            if(KTableName!=null){
                template = template.replace("${tableName}",KTableName.value());
            }
            KTableField KTableField =null;
            Object value =null;
            String columName ="";

            for (int i = 0; i <accessibleField.length ; i++) {
                KTableField = accessibleField[i].getAnnotation(KTableField.class);
                if(KTableField==null || KTableField.exist()){
                    value = ReflectUtils.getFieldValue(t,accessibleField[i].getName());
                    //&&  (KTableField==null || !KTableField.exist())
                    if(!EmptyUtils.isNullOrEmpty(value) ){
                        columName = KTableField!=null && !EmptyUtils.isNullOrEmpty(KTableField.alias())
                                ?KTableField.alias():accessibleField[i].getName();
                        setSql.append(columName+" = '"+value+"',");
                    }
                    if(!EmptyUtils.isNullOrEmpty(value) &&  KTableField!=null && KTableField.isPrimary()){
                        columName = !EmptyUtils.isNullOrEmpty(KTableField.alias())
                                ?KTableField.alias():accessibleField[i].getName();
                        template = template.replace("${primaryKey}",columName);
                        primaryKey.append("'"+value+"'");
                    }
                }
            }
            setSql = setSql.replace(setSql.length()-1,setSql.length(),"");

            String head =ind==0?"":";";

            return head+template.replace("#{primaryKey}",primaryKey).replace("${setSql}",setSql);
        }

        /**
         * 功能描述: 根据实体创建insert 语句
         * @param t
         * @param ind
         * @return: java.lang.String
         * @author: swc
         * @date: 2020/7/17 0017 下午 16:41
         */
        private  <T> String createInsertSql(T t,int ind) {
            String template ="insert  into ${tableName} (${tableCoums}) " ;

            String body = ind==0?"values(${valueList})":",(${valueList})\n" ;

            StringBuffer tableCoums =new StringBuffer();
            StringBuffer valueList =new StringBuffer();

            Field[] accessibleField = ReflectUtils.getAccessibleField(t);

            KTableName KTableName = t.getClass().getAnnotation(KTableName.class);

            if(KTableName!=null){
                template = template.replace("${tableName}",KTableName.value());

            }
            KTableField KTableField =null;

            Object value =null;
            String columName ="";

            for (int i = 0; i <accessibleField.length ; i++) {
                KTableField = accessibleField[i].getAnnotation(KTableField.class);
                if(KTableField==null || KTableField.exist()){
                    value = ReflectUtils.getFieldValue(t,accessibleField[i].getName());
                    if(!EmptyUtils.isNullOrEmpty(value)){
                        columName = KTableField!=null && !EmptyUtils.isNullOrEmpty(KTableField.alias())
                                ? KTableField.alias():accessibleField[i].getName();
                        tableCoums.append(columName+",");
                        valueList.append("'"+value+"',");
                    }

                }
            }
            tableCoums = tableCoums.replace(tableCoums.length() - 1, tableCoums.length(), "");

            valueList = valueList.replace(valueList.length()-1,valueList.length(),"");

            template = template.replace("${tableCoums}",tableCoums);

            if(ind!=0){
                return body.replace("${valueList}",valueList);
            }

            return template+body.replace("${valueList}",valueList);
        }


        /**
         * 功能描述: 自定义的新增 修改 删除的sql 的公共 执行方法
         * @param sql
         * @param t
         * @return: boolean
         * @author: swc
         * @date: 2020/7/17 0017 下午 16:40
         */
        private   <T> boolean  executeSql(String sql, T t) throws Exception {
            boolean flag = false;
            Connection connection =null;
            PreparedStatement preparedStatement =null;
            try {
                connection =JdbcUtil.getConnection();
                String currentSql= initSql(sql,  t);
                preparedStatement = connection.prepareStatement(currentSql);
                int result = preparedStatement.executeUpdate();
                flag = result > 0 ? true : false;
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                JdbcUtil.closeConnection(connection,preparedStatement,null);
            }
            return flag;
        }




        /**
         * 功能描述: 通过自定义sql 查询结果集 【查询的参数已设置 或者 没有查询参数】
         * @param sql
         * @param clazz
         * @return: java.util.List<B>
         * @author: swc
         * @date: 2020/7/17 0017 下午 16:39
         */
        public  <T,B>List<B> selectListBySql(String sql,Class<B> clazz) throws Exception {
            return selectListBySql(sql,null,clazz);
        }


        /**
         * 功能描述: 通过自定义sql 查询结果集
         * @param sql
         * @param params
         * @param clazz
         * @return: java.util.List<B>
         * @author: swc
         * @date: 2020/7/17 0017 下午 16:39
         */
        public  <T,B>List<B> selectListBySql(String sql, T params,Class<B> clazz) throws Exception {
            Connection connection =null;
            PreparedStatement preparedStatement =null;
            ResultSet resultSet =null;

            //需要反射成的实体
            HashMap<String,Field> hashMap =new HashMap<String,Field>();
            Field[] declaredFields = clazz.getDeclaredFields();
            KTableField KTableField =null;
            String columName ="";
            for(Field field:declaredFields){
                KTableField = field.getAnnotation(KTableField.class);
                if(KTableField==null || KTableField.exist()){
                    columName = KTableField!=null && !EmptyUtils.isNullOrEmpty(KTableField.alias())?KTableField.alias():field.getName();
                }
                if(!EmptyUtils.isNullOrEmpty(columName)){
                    hashMap.put(columName,field);
                }
                columName ="";
            }

            List<B> list = new ArrayList<B>();
            B b =null;
            try {
                connection =JdbcUtil.getConnection();
                String currentSql= initSql(sql,  params);
                preparedStatement = connection.prepareStatement(currentSql);

                resultSet = preparedStatement.executeQuery();
                ResultSetMetaData setMetaData = resultSet.getMetaData();
                // 获取列的数量
                int col_len = setMetaData.getColumnCount();
                while (resultSet.next()) {
                    b = clazz.newInstance();
                    for (int i = 0; i < col_len; i++) {
                        //getColumnLabel 可以取到别名
                        String col_name = setMetaData.getColumnLabel(i + 1);
                        Object col_value = resultSet.getObject(col_name);
                        if (col_value == null) {
                            col_value = "";
                        }
                        if(hashMap.containsKey(col_name)){
                            System.out.println(col_name);
                            setEntityValue(b,hashMap.get(col_name),col_value);
                        }
                    }
                    list.add(b);
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                JdbcUtil.closeConnection(connection,preparedStatement,resultSet);
            }
            return list;

        }


        /**
         * 功能描述: 查询总数的sql语句  执行结果为Integer 【具体的实现方法】
         * @param sql
         * @param params
         * @return: java.lang.Integer
         * @author: swc
         * @date: 2020/7/17 0017 下午 16:38
         */
        public  <T>Integer  selectCountBySql(String sql, T params) throws Exception {
            int count = 0;
            Connection connection =null;
            PreparedStatement preparedStatement =null;
            ResultSet resultSet =null;
            try {
                connection =JdbcUtil.getConnection();
                String currentSql= initSql(sql,  params);
                preparedStatement = connection.prepareStatement(currentSql);
                resultSet = preparedStatement.executeQuery();
                if (resultSet.next()) {
                    count = resultSet.getInt(1);
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                JdbcUtil.closeConnection(connection,preparedStatement,resultSet);
            }
            return count;

        }

        /**
         * 功能描述: 查询总数的sql语句  执行结果为Integer
         * @param sql
         * @return: java.lang.Integer
         * @author: swc
         * @date: 2020/7/17 0017 下午 16:37
         */
        public  Integer  selectCountBySql(String sql) throws Exception {
            return  selectCountBySql(sql,null);
        }

        /**
         * 功能描述: 根据类型进行匹配 给表字段设置 值
         * @param b
         * @param field
         * @param col_value
         * @return: void
         * @author: swc
         * @date: 2020/7/17 0017 上午 11:16
         */
        private  <B> void setEntityValue(B b, Field field, Object col_value) {
            if(EmptyUtils.isNullOrEmpty(col_value)){
                return;
            }
            if(field.getType() == Integer.class ){
                col_value =Integer.valueOf(col_value.toString());
            }
            else if(field.getType() == Long.class ){
                col_value =Long.valueOf(col_value.toString());
            }
            else if(field.getType() == Double.class ){
                col_value =Double.valueOf(col_value.toString());
            }
            else if(field.getType() == String.class ){
                col_value =col_value.toString();
            }
            else if(field.getType() == BigDecimal.class ){
                col_value =new BigDecimal(col_value.toString());
            }
            else if(field.getType() == Float.class ){
                col_value =Float.valueOf(col_value.toString());
            }
            else if(field.getType() == Short.class ){
                col_value =Integer.valueOf(col_value.toString());
            }
            else if(field.getType() == Date.class ){
                col_value = DateUtil.strToDate(col_value.toString(),DateUtil.FORMAT_YYYY_MM_DD);
            }

            ReflectUtils.setFieldValue(b,field.getName(),col_value);
        }


        /**
         * 功能描述: 初始化sql 针对自定义的sql 【参数部分没有设置值】
         * @param sql
         * @param t
         * @return: java.lang.String
         * @author: swc
         * @date: 2020/7/17 0017 下午 16:34
         */
        private  <T> String initSql(String sql, T t) {
            Object fieldValue =null;
            if (t != null) {
                Field[] fields = ReflectUtils.getAccessibleField(t);
                for(Field field:fields){
                    if(sql.contains(field.getName())){
                        fieldValue =ReflectUtils.getFieldValue(t,field.getName());
                        if(fieldValue instanceof Date){
                            fieldValue = DateUtil.dateToStr((Date)fieldValue,DateUtil.FORMAT_YYYY_MM_DD_HH_MM_SS);
                        }
                        sql = sql.replace("#{"+field.getName()+"}","'"+fieldValue+"'");
                    }
                }
            }
            return sql;
        }

        /**
         * 功能描述: 针对查询sql的执行 返回可能是Int 单个对象 和集合
         * @param t
         * @param executeType
         * @return: java.lang.Object
         * @author: swc
         * @date: 2020/7/17 0017 下午 16:35
         */
        public <T> Object executeSql(T t,String executeType) throws Exception{
            Object object =null;
            String currentSql= createSql(t,0,executeType);
            if(JDBCTemplate.SELECT_COUNT.equalsIgnoreCase(executeType)){
                object = selectCountBySql(currentSql);
            }else if(JDBCTemplate.SELECT.equalsIgnoreCase(executeType)){
                object = selectListBySql(currentSql,t.getClass());
            }

            return object;
        }


    }


    public static void main(String[] args) throws Exception{
        JDBCTemplate template = new JDBCTemplate();

        List<Area> list =new ArrayList<>();

        Area area1 =new Area();
        area1.setId(10132L);
        area1.setPid("315");
        area1.setCode("MN98K");
        area1.setNums(new BigDecimal(3360));

        Area area2 =new Area();
        area2.setId(10133L);
        area2.setPid("18.8");
        area2.setCode("TDK");
        area2.setNums(new BigDecimal(889));

        list.add(area1);
        list.add(area2);


//        List<Area> maps =  template.selectBySql("select pid,area_name,area_code,create_date,nums  from sys_area ",Area.class);
//        System.out.println(maps);
////
//        Integer count =  template.selectCountBySql("select count(id)  from sys_area ",null);
////
////
//        boolean flag=  template.save("update sys_area set area_name =123 where pid=#{pid}",area1);
////
//        System.out.println(count);
//        System.out.println(flag);

        //新增
//        template.insertByObj(area1);
//          template.insertBatch(list);

        //修改
//         template.updateByObj(area1);
//         template.updateBatch(list);

        //删除
//        template.deleteById(area1);
//        template.deleteBatch(list);


//        int i = template.selectCount(area1);
        Area area = template.selectOneByObj(area1);
        List<Area> areas = template.selectListByObj(area1);
        System.out.println(111);
    }

}
