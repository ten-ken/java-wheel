
package com.ken.sys.common.ifs;

import java.util.List;
import java.util.Map;

/**
 * <ul>
 * <li>Title:-DataCompare</li>
 * <li>Description: TODO </li>
 * <li>Copyright: Copyright (c) 2018</li>
 * <li>微信:tianji_vip</li>
 * </ul>
 *
 * @author swc
 * @version
 * @date 2019/11/14 0014 上午 11:22
 */
public interface DataCompare {
    /**
     * 功能描述: 用于 数据库值和数据源对比 返回错误信息
     * @param existMap
     * @param dataList
     * @return: void
     * @author: swc
     * @date: 2019/11/14 0014 上午 11:37
    */
    <T> void handData(Map<Object,T> existMap, List<T> dataList);
}
