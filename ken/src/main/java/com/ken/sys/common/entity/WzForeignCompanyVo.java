package com.ken.sys.common.entity;


import com.ken.sys.common.anno.ExcelColumnAnnotation;

/**
 * @author: guidayang
 * @date: 2020/7/9
 */
public class WzForeignCompanyVo {
    /**
     * 企业名称
     */
    @ExcelColumnAnnotation(index = 0,length = 1000,nullable = true,columnName="企业名称",fieldName="companyName")
    private String companyName;

    /**
     * 社会信用代码
     */
    @ExcelColumnAnnotation(index = 1,length = 1000,nullable = true,columnName="社会信用代码",fieldName="sociCreditCode")
    private String sociCreditCode;

    /**
     * 企业地址
     */
    @ExcelColumnAnnotation(index = 2,length = 1000,nullable = true,columnName="地址",fieldName="companyAddr")
    private String companyAddr;

    /**
     * 企业类型
     */
    @ExcelColumnAnnotation(index = 3,length = 1000,nullable = true,columnName="公司类型",fieldName="companyType")
    private String companyType;

    /**
     * 经营状态
     */
    @ExcelColumnAnnotation(index = 4,length = 1000,nullable = true,columnName="经营状态",fieldName="businessStatus")
    private String businessStatus;

    /**
     * 省份
     */
    @ExcelColumnAnnotation(index = 5,length = 1000,nullable = true,columnName="省份",fieldName="provinceName")
    private String provinceName;

    /**
     * 行政区划编码
     */
    @ExcelColumnAnnotation(index = 6,length = 1000,nullable = true,columnName="行政区划编码",fieldName="adminDivisionCode")
    private String adminDivisionCode;

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getSociCreditCode() {
        return sociCreditCode;
    }

    public void setSociCreditCode(String sociCreditCode) {
        this.sociCreditCode = sociCreditCode;
    }

    public String getCompanyAddr() {
        return companyAddr;
    }

    public void setCompanyAddr(String companyAddr) {
        this.companyAddr = companyAddr;
    }

    public String getCompanyType() {
        return companyType;
    }

    public void setCompanyType(String companyType) {
        this.companyType = companyType;
    }

    public String getBusinessStatus() {
        return businessStatus;
    }

    public void setBusinessStatus(String businessStatus) {
        this.businessStatus = businessStatus;
    }

    public String getProvinceName() {
        return provinceName;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }

    public String getAdminDivisionCode() {
        return adminDivisionCode;
    }

    public void setAdminDivisionCode(String adminDivisionCode) {
        this.adminDivisionCode = adminDivisionCode;
    }
}
