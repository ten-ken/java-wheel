package com.ken.sys.common.entity.echarts.option;

import com.ken.sys.common.entity.echarts.axis.EchartsXYAxis;
import com.ken.sys.common.entity.echarts.dataset.EchartsDataset;
import com.ken.sys.common.entity.echarts.legend.EchartsLegend;
import com.ken.sys.common.entity.echarts.series.EchartBaseSeries;
import com.ken.sys.common.entity.echarts.textStyle.EchartsTextStyle;
import com.ken.sys.common.entity.echarts.title.EchartsTitle;

import java.util.List;

/**
 *
 * @author swc
 *
 * @date 2020/7/3 0003 下午 13:43
 */
public class EchartsOption {
    //标题
    private EchartsTitle title;

    //图例组件。
    //图例组件展现了不同系列的标记(symbol)，颜色和名字。可以通过点击图例控制哪些系列不显示。
    private EchartsLegend legend;

    //直角坐标系 grid 中的 x 轴，
    private EchartsXYAxis xAxis;

    //直角坐标系 grid 中的 y 轴，[x轴和y轴 属性是一样的]
    private EchartsXYAxis yAxis;

    //默认颜色值  全局
    private Object color =new String[]{"#c23531","#2f4554", "#61a0a8", "#d48265", "#91c7ae","#749f83",  "#ca8622", "#bda29a","#6e7074", "#546570", "#c4ccd3"};

    //全局的字体样式。
    private EchartsTextStyle textStyle;

    //系列列表。每个系列通过 type 决定自己的图表类型
    private List<EchartBaseSeries> series;

    //注意
    //Charts 4 开始支持了 数据集（dataset）组件用于单独的数据集声明，
    // 从而数据可以单独管理，被多个组件复用，并且可以自由指定数据到视觉的映射。这在不少场景下能带来使用上的方便。
    private EchartsDataset dataset;

    public EchartsTitle getTitle() {
        return title;
    }

    public void setTitle(EchartsTitle title) {
        this.title = title;
    }

    public EchartsLegend getLegend() {
        return legend;
    }

    public void setLegend(EchartsLegend legend) {
        this.legend = legend;
    }

    public EchartsXYAxis getxAxis() {
        return xAxis;
    }

    public void setxAxis(EchartsXYAxis xAxis) {
        this.xAxis = xAxis;
    }

    public EchartsXYAxis getyAxis() {
        return yAxis;
    }

    public void setyAxis(EchartsXYAxis yAxis) {
        this.yAxis = yAxis;
    }

    public Object getColor() {
        return color;
    }

    public void setColor(Object color) {
        this.color = color;
    }

    public EchartsTextStyle getTextStyle() {
        return textStyle;
    }

    public void setTextStyle(EchartsTextStyle textStyle) {
        this.textStyle = textStyle;
    }


    public List<EchartBaseSeries> getSeries() {
        return series;
    }

    public void setSeries(List<EchartBaseSeries> series) {
        this.series = series;
    }

    public EchartsDataset getDataset() {
        return dataset;
    }

    public void setDataset(EchartsDataset dataset) {
        this.dataset = dataset;
    }


}
