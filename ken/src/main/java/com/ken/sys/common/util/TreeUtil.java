/******************************************************************************
 *
 * 作者（author）：ken
 * 微信（weChat）：mlchao1992
 * 个人博客（website）：
 *
 ******************************************************************************
 * 注意：尊重原创
 *****************************************************************************/
package com.ken.sys.common.util;

import com.ken.sys.common.dto.TreeInfoVo;

import java.util.ArrayList;
import java.util.List;

/**
 * <ul>
 * <li>Title: ken-TreeUtil</li>
 * <li>Description: TODO </li>
 * <li>Copyright: Copyright (c) 2018</li>
 * </ul>
 *
 * @author ken
 * @version V1.0
 * @date 2019/9/20 20:04
 */
public class TreeUtil {
   

 /**
     * 使用递归方法建树
     * 该方法为传入一个list，以及root节点，动态以root参数为跟建立树形结构
     * @param treeNodes,root
     * @return
     */
    public static List<TreeInfoVo> buildTree(List<TreeInfoVo> treeNodes,String root) {
        List<TreeInfoVo> trees = new ArrayList<TreeInfoVo>();
        for (TreeInfoVo treeNode : treeNodes) {
            if (treeNode.getCode().equals(root)) {
                trees.add(findChildren(treeNode,treeNodes));
            }
        }
        return trees;
    }

    private static TreeInfoVo findChildren(TreeInfoVo treeNode,List<TreeInfoVo> treeNodes) {
        for (TreeInfoVo it : treeNodes) {
            if(treeNode.getCode().equals(it.getPid())) {
                if (treeNode.getSubTree() == null) {
                    treeNode.setSubTree(new ArrayList<TreeInfoVo>());
                }
                treeNode.getSubTree().add(findChildren(it,treeNodes));
            }
        }
        return treeNode;
    }


    /**
     * 获取某个父节点下面的所有子节点
     * @param TreeInfoVos
     * @param pid
     * @return
     */
    public static List<TreeInfoVo> treeList(List<TreeInfoVo> TreeInfoVos, String pid,List<TreeInfoVo> result){
        for(TreeInfoVo tb: TreeInfoVos){
            /***
             * 添加本级节点到list中--本级不需要
             */
            //遍历出父id等于参数的id，add进子节点集合
            if(tb.getPid().equals(pid)){
                //递归遍历下一级
                treeList(TreeInfoVos,tb.getCode(),result);
                result.add(tb);
            }else{
                if(tb.getSubTree()!=null && tb.getSubTree().size()!=0){
                    treeList(tb.getSubTree(),pid,result);
                }
            }
        }
        return result;
    }

    /**
     * 功能描述: 根据ID获取树中的对象
     * @param TreeInfoVos
     * @param selfId 自身的id 或者自身的code
     * @param result
     * @return: com.example.demo.TreeInfoVo
     * @author: swc
     * @date: 2021/3/22 0022 上午 10:50
    */
    public static TreeInfoVo treeObject(List<TreeInfoVo> TreeInfoVos, String selfId,TreeInfoVo result){
        for (TreeInfoVo tb : TreeInfoVos) {
            //遍历出父id等于参数的id，add进子节点集合
            if(tb.getCode().equals(selfId)){
                result = tb;
                break;
            }else{
                if(tb.getSubTree()!=null && tb.getSubTree().size()!=0){
                    result = treeObject(tb.getSubTree(), selfId,result);
                }
            }
        }
        return result;
    }

}
