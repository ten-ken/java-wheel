package com.ken.sys.common.util;

import com.ken.sys.common.entity.Table;
import com.ken.sys.common.entity.TableInfo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ReadDBTable {

	private static String dbname ="";//表名

	private static String tableType="mysql";//表类型

	/**
	 * 获取数据库所有表名称
	 * @return
	 */
	public static List<String> getDBColumData(String url, String user, String password, String ctablename, String ctype) {
		if(ctablename==null || ctype==null){
			ctablename=dbname;
			ctype=tableType;
		}
		List<String> tableList = new ArrayList<String>();//
		Connection conn=JdbcUtil.getConnection(url,user,password);
		String sql1="select table_name from information_schema.tables where table_schema='"+ctablename+"' and table_type='"+ctype+"'";//查询数据库里面所有表 名
		PreparedStatement stmt=null;
		ResultSet rs=null;

		try {
			stmt =conn.prepareStatement(sql1);
			rs= stmt.executeQuery();
			while(rs.next()){
				String tableName = rs.getString("table_name");
				tableList.add(tableName);
			}
		} catch (SQLException e) {
			System.out.println("数据库连接异常");
			e.printStackTrace();
		}finally {
			JdbcUtil.closeConnection(conn, stmt, rs);
		}
		return tableList;
	}

	/**
	 * 获取表字段详细信息
	 * @param tableName
	 * @return
	 */
	public static Table getTableColumData(String url, String user, String password, String tableName){
		Table table =new Table();
		Connection conn=JdbcUtil.getConnection(url,user,password);
		String sql ="select column_name,data_type,is_nullable,numeric_precision,column_type,column_key,character_maximum_length,column_comment from information_schema.columns where table_schema='"+dbname+"' and table_name='"+tableName+"'";
		PreparedStatement stmt=null;
		ResultSet rs=null;

		try {
			stmt =conn.prepareStatement(sql);
			rs= stmt.executeQuery();
			List<TableInfo> list =new ArrayList<TableInfo>();
			while(rs.next()){
				TableInfo tableInfo =new TableInfo(); 
				tableInfo.setColumn_name(rs.getString("column_name"));//列名
				tableInfo.setData_type(rs.getString("data_type"));//类型
				tableInfo.setColumn_type(rs.getString("column_type"));//字段详细类型 包含长度和精度
				tableInfo.setColumn_comment( rs.getString("column_comment"));//中文注释
				if(rs.getString("numeric_precision")!=null) {
					tableInfo.setLength(rs.getLong("numeric_precision"));//最大长度
				}
				if(rs.getString("character_maximum_length")!=null) {
					tableInfo.setLength(rs.getLong("character_maximum_length"));//最大长度
				}
				tableInfo.setIsNotEmpty("NO".equalsIgnoreCase(rs.getString("is_nullable"))?true:false);
				tableInfo.setIsPrimary("PRI".equalsIgnoreCase(rs.getString("column_key"))?true:false);
				//System.out.println(tableName+"表的列名:"+rs.getString("column_name"));
				list.add(tableInfo);
			}
			table.setName(tableName);
			table.setList(list);
		} catch (SQLException e) {
			System.out.println("数据库连接异常");
			e.printStackTrace();
		}finally {
			JdbcUtil.closeConnection(conn, stmt, rs);
		}
		return table;
	}


	/**
	 * 获取数据库中 每个表的所有字段详细信息
	 * @return
	 */
	public static List<Table> getAllTableInfo() {
		List<String> list = ReadDBTable.getDBColumData(null,null,null,null,null);
		List<Table> tablecolum =new ArrayList<Table>();//
		for(String tableName:list) {
			Table table = ReadDBTable.getTableColumData(null,null,null,null);
			tablecolum.add(table);
		}
		return tablecolum;
	}

	public static void main(String[] args) {
		ReadDBTable.getAllTableInfo();
	}



}
