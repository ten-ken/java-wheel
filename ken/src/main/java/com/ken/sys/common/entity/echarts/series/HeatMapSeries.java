
package com.ken.sys.common.entity.echarts.series;

/**
 * <ul>
 * <li>Title = -HeatMapSeries</li>
 * <li>Description =  TODO </li>
 * </ul>
 *  热力图
 * @author swc
 * @version
 * @date 2020/3/21 0 = 11
 */
public class HeatMapSeries extends EchartBaseSeries{

    private String type ="heatmap";

//    private String id ;

    //系列名称，用于tooltip的显示，legend 的图例筛选，在 setOption 更新数据和配置项时用于指定对应的系列。
    private String name  ;

//    该系列使用的坐标系，可选：
//            'cartesian2d'
//    使用二维的直角坐标系（也称笛卡尔坐标系），通过 xAxisIndex, yAxisIndex指定相应的坐标轴组件。
//            'geo'
//    使用地理坐标系，通过 geoIndex 指定相应的地理坐标系组件。
//            'calendar'
//    使用日历坐标系
    private String  coordinateSystem = "cartesian2d"  ;

    //使用的 x 轴的 index，在单个图表实例中存在多个 x 轴的时候有用。
    private Integer xAxisIndex = 0 ;

    //使用的 y 轴的 index，在单个图表实例中存在多个 y轴的时候有用。
    private Integer  yAxisIndex = 0 ;

    //使用的地理坐标系的 index，在单个图表实例中存在多个地理坐标系的时候有用。
    private Integer   geoIndex = 0 ;

//    使用的日历坐标系的 index，在单个图表实例中存在多个日历坐标系的时候有用。
    private Integer calendarIndex =  0  ;

    //每个点的大小，在地理坐标系(coordinateSystem: 'geo')上有效。
    private Integer  pointSize =  20  ;

    //每个点模糊的大小，在地理坐标系(coordinateSystem: 'geo')上有效。
    private Integer  blurSize = 20  ;

    //最小的透明度，在地理坐标系(coordinateSystem: 'geo')上有效。
    private Integer  minOpacity =  0  ;

    //最大的透明度，在地理坐标系(coordinateSystem: 'geo')上有效。
    private Integer  maxOpacity =  1  ;

//    渐进式渲染时每一帧绘制图形数量，设为 0 时不启用渐进式渲染，支持每个系列单独配置。
//    在图中有数千到几千万图形元素的时候，一下子把图形绘制出来，或者交互重绘的时候可能会造成界面的卡顿甚至假死。ECharts 4 开始全流程支持渐进渲染（progressive rendering），渲染的时候会把创建好的图形分到数帧中渲染，每一帧渲染只渲染指定数量的图形。
//    该配置项就是用于配置该系列每一帧渲染的图形数，可以根据图表图形复杂度的需要适当调整这个数字使得在不影响交互流畅性的前提下达到绘制速度的最大化。比如在 lines 图或者平行坐标中线宽大于 1 的 polyline 绘制会很慢，这个数字就可以设置小一点，而线宽小于等于 1 的 polyline 绘制非常快，该配置项就可以相对调得比较大
    private Integer  progressive =  400  ;

    //启用渐进式渲染的图形数量阈值，在单个系列的图形数量超过该阈值时启用渐进式渲染。
    private Integer progressiveThreshold =  3000  ;

    //在直角坐标系(coordinateSystem: 'cartesian2d')上有效。
    private Object emphasis;

    //标记的图形。
    private Object  markPoint;

    //图表标线。
    private Object markLine ;

      //图表标域，常用于标记图表中某个范围的数据，例如标出某段时间投放了广告。
    private Object markArea;

    //热力图所有图形的 zlevel 值。
    private Integer zlevel =  0  ;

    //热力图组件的所有图形的z值。控制图形的前后顺序。z值小的图形会被z值大的图形覆盖。
    //z相比zlevel优先级更低，而且不会创建新的 Canvas。
    private Integer z =  2  ;

    //图形是否不响应和触发鼠标事件，默认为 false，即响应和触发鼠标事件。
    private boolean silent;


    //本系列特定的 tooltip 设定。
    private Object tooltip;

    @Override
    public String getType() {
        return type;
    }

    @Override
    public void setType(String type) {
        this.type = type;
    }

//    public String getId() {
//        return id;
//    }
//
//    public void setId(String id) {
//        this.id = id;
//    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    public String getCoordinateSystem() {
        return coordinateSystem;
    }

    public void setCoordinateSystem(String coordinateSystem) {
        this.coordinateSystem = coordinateSystem;
    }

    public Integer getxAxisIndex() {
        return xAxisIndex;
    }

    public void setxAxisIndex(Integer xAxisIndex) {
        this.xAxisIndex = xAxisIndex;
    }

    public Integer getyAxisIndex() {
        return yAxisIndex;
    }

    public void setyAxisIndex(Integer yAxisIndex) {
        this.yAxisIndex = yAxisIndex;
    }

    public Integer getGeoIndex() {
        return geoIndex;
    }

    public void setGeoIndex(Integer geoIndex) {
        this.geoIndex = geoIndex;
    }

    public Integer getCalendarIndex() {
        return calendarIndex;
    }

    public void setCalendarIndex(Integer calendarIndex) {
        this.calendarIndex = calendarIndex;
    }

    public Integer getPointSize() {
        return pointSize;
    }

    public void setPointSize(Integer pointSize) {
        this.pointSize = pointSize;
    }

    public Integer getBlurSize() {
        return blurSize;
    }

    public void setBlurSize(Integer blurSize) {
        this.blurSize = blurSize;
    }

    public Integer getMinOpacity() {
        return minOpacity;
    }

    public void setMinOpacity(Integer minOpacity) {
        this.minOpacity = minOpacity;
    }

    public Integer getMaxOpacity() {
        return maxOpacity;
    }

    public void setMaxOpacity(Integer maxOpacity) {
        this.maxOpacity = maxOpacity;
    }

    public Integer getProgressive() {
        return progressive;
    }

    public void setProgressive(Integer progressive) {
        this.progressive = progressive;
    }

    public Integer getProgressiveThreshold() {
        return progressiveThreshold;
    }

    public void setProgressiveThreshold(Integer progressiveThreshold) {
        this.progressiveThreshold = progressiveThreshold;
    }


    public Object getEmphasis() {
        return emphasis;
    }

    public void setEmphasis(Object emphasis) {
        this.emphasis = emphasis;
    }

    public Object getMarkPoint() {
        return markPoint;
    }

    public void setMarkPoint(Object markPoint) {
        this.markPoint = markPoint;
    }

    public Object getMarkLine() {
        return markLine;
    }

    public void setMarkLine(Object markLine) {
        this.markLine = markLine;
    }

    public Object getMarkArea() {
        return markArea;
    }

    public void setMarkArea(Object markArea) {
        this.markArea = markArea;
    }

    public Integer getZlevel() {
        return zlevel;
    }

    public void setZlevel(Integer zlevel) {
        this.zlevel = zlevel;
    }

    public Integer getZ() {
        return z;
    }

    public void setZ(Integer z) {
        this.z = z;
    }

    public boolean isSilent() {
        return silent;
    }

    public void setSilent(boolean silent) {
        this.silent = silent;
    }

    public Object getTooltip() {
        return tooltip;
    }

    public void setTooltip(Object tooltip) {
        this.tooltip = tooltip;
    }

}
