package com.ken.sys.common.entity;

/**
 * <ul>
 * <li>Title: DbSettingInfo</li>
 * <li>Description: TODO </li>
 * </ul>
 *
 * @author swc
 * @date 2021-12-09 15:57
 */
public class DbSettingInfo {

    /**
     * 数据库驱动
    */
    private String driver;

    /**
     * 数据库连接
     */
    private String dbUrl;

    /**
     * 用户名
     */
    private String userName;

    /**
     * 用户名称
     */
    private String password;

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public String getDbUrl() {
        return dbUrl;
    }

    public void setDbUrl(String dbUrl) {
        this.dbUrl = dbUrl;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
