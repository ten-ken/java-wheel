/******************************************************************************
 *
 * 作者（author）：ken
 * 微信（weChat）：mlchao1992
 * 个人博客（website）：
 *
 ******************************************************************************
 * 注意：尊重原创
 *****************************************************************************/
package com.ken.sys.common.dto;

import java.util.List;

/**
 * <ul>
 * <li>Title: ken-TreeInfoVo</li>
 * <li>Description: 封装的tree信息 </li>
 * <li>Copyright: Copyright (c) 2018</li>
 * </ul>
 *
 * @author ken
 * @version V1.0
 * @date 2019/9/20 20:35
 */
public class TreeInfoVo<T> {
      /**
     * name 树id
     */
    private String treeid ;

    /**
     * name 树名称
     */
    private String name;

    /**
     * 是否可选
     */
    private String open;

    /**
     * 对应code
     */
    private String code;

    /**
     * 父类code 或者 父类的id
     */
    private String pid;


    /**
     * 子级
     */
    private List<TreeInfoVo> subTree;


    public String getTreeid() {
        return treeid;
    }

    public void setTreeid(String treeid) {
        this.treeid = treeid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOpen() {
        return open;
    }

    public void setOpen(String open) {
        this.open = open;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public List<TreeInfoVo> getSubTree() {
        return subTree;
    }

    public void setSubTree(List<TreeInfoVo> subTree) {
        this.subTree = subTree;
    }

    public TreeInfoVo(String treeid, String pid, String code, String name) {
        this.treeid = treeid;
        this.name = name;
        this.code = code;
        this.pid = pid;
    }

    public TreeInfoVo() {
    }
}
