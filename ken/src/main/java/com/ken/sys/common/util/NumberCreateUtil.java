package com.ken.sys.common.util;

import java.security.SecureRandom;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author swc
 * @description 生成编号---支持小型系统 用户量低于5万 大型系统请用 SnowflakeIdWorker【雪花算法】
 * @date 2020/4/8 0008 上午 10:47
 */
public class NumberCreateUtil {

    //最大长度
    private static Integer maxLength =22;

    /**
     * 功能描述: 同时同秒  配上用户的id 加0-3位随机数(生成订单号）
     * @param userId
     * @return: java.lang.String
     * @author: swc
     * @date: 2020/3/11 0011 下午 16:17
     */
    private  static String createNumber(long userId){
        StringBuffer sbf =new StringBuffer();
        //10位
        sbf.append(DateUtil.dateToStr(new Date(),"yyyyMMddHH"));

        //生成7位的随机数
        sbf.append(generateRandomDigitString(7));

        //取用户id的后四位 不足3位 补0
        sbf.append(String.format("%03d", userId%1000));//将用户id拼接上  补足4位补位

        return sbf.toString();
    }

    /**
     * 产生指定长度的无规律数字字符串
     *
     * @param aLength  生成的随机数的长度
     * @return		           生成的随机字符串
     */
    private static String generateRandomDigitString(int aLength) {
        SecureRandom tRandom = new SecureRandom();
        long tLong;
        String aString = "";
        tRandom.nextLong();
        tLong = Math.abs(tRandom.nextLong());
        aString = (String.valueOf(tLong)).trim();
        while (aString.length() < aLength){
            tLong = Math.abs(tRandom.nextLong());
            aString += (String.valueOf(tLong)).trim();
        }
        aString = aString.substring(0, aLength);
        return aString;
    }
}
