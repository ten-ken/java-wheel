package com.ken.sys.common.entity;

import java.util.List;

public class Table {
    private String name;
    private List<TableInfo> list;

    //起始行
    private int firstRow;

    //末行
    private int lastRow;

    //中文名称
    private String cnName;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<TableInfo> getList() {
        return list;
    }

    public void setList(List<TableInfo> list) {
        this.list = list;
    }

    public int getLastRow() {
        return lastRow;
    }

    public void setLastRow(int lastRow) {
        this.lastRow = lastRow;
    }

    public int getFirstRow() {
        return firstRow;
    }

    public void setFirstRow(int firstRow) {
        this.firstRow = firstRow;
    }

    public String getCnName() {
        return cnName;
    }

    public void setCnName(String cnName) {
        this.cnName = cnName;
    }
}
