package com.ken.sys.common.util;

import java.util.Date;

/**
 * <ul>
 * <li>Title: GenerateSQLUtil-->mysql  自动生成sql</li>
 * </ul>
 *
 * @author swc
 * @date 2020/3/18 0018 下午 17:14
 */
public class GenerateSQLUtil {

    /**
     * 功能描述: 生成某期间的所有时间（每天）时间的sql 如 2020-03-18 2020-03-19
     * @param startDate
     * @param endDate
     * @param dateFormat
     * @return: java.lang.String
     * @author: swc
     * @date: 2020/3/18 0018 下午 13:46
     */
    public String getCompleteDateSql(String startDate,String endDate,String dateFormat) {
        long days = calculatedDays(DateUtil.strToDate(startDate,dateFormat),DateUtil.strToDate(endDate,dateFormat));
        StringBuffer apendSql =new StringBuffer();
        apendSql.append(" select date('"+startDate+"') as order_date \n");//默认
        for(int i=1;i<=days;i++){
            apendSql.append("union all \n");
            apendSql.append("SELECT DATE_ADD(date('"+startDate+"'),interval "+i+" day) as order_date \n");
        }
        return apendSql.toString();
    }

    /**
     * 功能描述:生成某期间的所有时间（每天）时间的sql  如 2020-03-18 2020-03-19
     * @param startDate
     * @param endDate
     * @param dateFormat
     * @return: java.lang.String
     * @author: swc
     * @date: 2020/3/18 0018 下午 17:21
    */
    public String getCompleteDateSql(Date startDate, Date endDate, String dateFormat) {
        return getCompleteDateSql(DateUtil.dateToStr(startDate,dateFormat),DateUtil.dateToStr(endDate,dateFormat),dateFormat);
    }



    /**
     * 功能描述: 计算两个时间内的相差天数
     * @param start
     * @param end
     * @return: long
     * @author: swc
     * @date: 2020/3/18 0018 下午 13:46
     */
    protected long calculatedDays(Date start,Date end) {
        long times = end.getTime()
                -start.getTime();
        long days = times /1000/60/60/24;//两个时间相差多少天
        return days;
    }
}
