/******************************************************************************
 *
 *
 *
 ******************************************************************************
 *
 *****************************************************************************/
package com.ken.sys.common.entity;

/**
 * <ul>
 * <li>Title: CalendarData</li>
 * <li>Description: TODO </li>
 * <li>Copyright: Copyright (c) 2018</li>
 * <li>微信:tianji_vip</li>
 * </ul>
 *
 * @author swc
 *
 * @date 2020/7/2 0002 下午 14:37
 */
public class CalendarData {

    private String curDateStr;

    private Integer value;

    public CalendarData() {
    }

    public CalendarData(String curDateStr, Integer value) {
        this.curDateStr = curDateStr;
        this.value = value;
    }

    public String getCurDateStr() {
        return curDateStr;
    }

    public void setCurDateStr(String curDateStr) {
        this.curDateStr = curDateStr;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }
}
