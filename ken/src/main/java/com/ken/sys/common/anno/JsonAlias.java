package com.ken.sys.common.anno;

import java.lang.annotation.*;

/**
 * <ul>
 * <li>Title: JsonAlias</li>
 * <li>Description: json反射的别名 </li>
 * </ul>
 *
 * @author swc
 * @date 2020/7/10 23:51
 */
@Documented
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface JsonAlias {
    /**
     * 设置别名
     * @return
     */
    String value() default "";
}
