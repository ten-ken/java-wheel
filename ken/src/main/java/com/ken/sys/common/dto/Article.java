package com.ken.sys.common.dto;

/**
 * <ul>
 * <li>Title: Article</li>
 * <li>Description: TODO </li>
 * </ul>
 *
 * @author swc
 * @date 2020/7/19 12:35
 */
public class Article {

    //标题
    private String title;

    //标签
    private String tags;

    //类别
    private String type;

    //内容
    private String content;


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
