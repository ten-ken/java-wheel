package com.ken.sys.common.entity;


//@POExcelImportAnnotation(startRowIndex = 1)
public class TableInfo implements java.io.Serializable {

	//字段名
//	@ColumnCheckAnnotation(index = 1, dataType = DataType.String)
	private String column_name;
	//字段长度
//	@ColumnCheckAnnotation(index = 2, dataType = DataType.Integer)
	private long length;
	//字段类型
//	@ColumnCheckAnnotation(index = 3, dataType = DataType.String)
	private String data_type;
	
	//字段详细类型 包含长度和精度
//	@ColumnCheckAnnotation(index = 4, dataType = DataType.String)
	private String column_type;
	
	//中文注释
	private String column_comment;

	//是否主键
	private boolean isPrimary;

	//是否非空
	private boolean isNotEmpty;

	public String getColumn_name() {
		return column_name;
	}

	public void setColumn_name(String column_name) {
		this.column_name = column_name;
	}

	public long getLength() {
		return length;
	}

	public void setLength(long length) {
		this.length = length;
	}

	public String getData_type() {
		return data_type;
	}

	public void setData_type(String data_type) {
		this.data_type = data_type;
	}

	public String getColumn_type() {
		return column_type;
	}

	public void setColumn_type(String column_type) {
		this.column_type = column_type;
	}

	public String getColumn_comment() {
		return column_comment;
	}

	public void setColumn_comment(String column_comment) {
		this.column_comment = column_comment;
	}

	public boolean isPrimary() {
		return isPrimary;
	}

	public void setIsPrimary(boolean primary) {
		isPrimary = primary;
	}

	public boolean isNotEmpty() {
		return isNotEmpty;
	}

	public void setIsNotEmpty(boolean notEmpty) {
		isNotEmpty = notEmpty;
	}
}
