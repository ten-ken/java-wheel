/******************************************************************************
 *
 *
 *
 ******************************************************************************
 *
 *****************************************************************************/
package com.ken.sys.common.constant;

/**
 * <ul>
 * <li>Title: EchartsConstant</li>
 * <li>Description: TODO </li>
 * <li>Copyright: Copyright (c) 2018</li>
 * <li>微信:tianji_vip</li>
 * </ul>
 *
 * @author swc
 *
 * @date 2020/7/3 0003 下午 15:09
 */
public class EchartsConstant {

    //折线图
    public static final String LINE ="line";

    //柱状图 条形图
    public static final String BAR ="bar";

    //饼状图 圆形图
    public static final String PIE ="pie";

    //漏斗图
    public static final String FUNNEL ="funnel";

    //仪表图
    public static final String GAUGE ="gauge";

    //热力图
    public static final String HEATMAP ="heatmap";

    //K线图
    public static final String CANDLESTICK ="candlestick";
}
