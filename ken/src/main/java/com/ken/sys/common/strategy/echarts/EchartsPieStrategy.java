package com.ken.sys.common.strategy.echarts;

import com.ken.sys.common.entity.echarts.dto.HandEchartsDto;
import com.ken.sys.common.entity.echarts.option.EchartsOption;
import com.ken.sys.common.entity.echarts.series.PieSeries;
import com.ken.sys.common.ifs.IEchartSeries;
import com.ken.sys.common.strategy.echarts.base.BaseStrategy;
import com.ken.sys.common.strategy.echarts.base.EchartsStrategy;

import java.util.List;

/**
 * <ul>
 * <li>Title:EchartsLineStrategy</li>
 * <li>Description: TODO </li>
 * </ul>
 *
 * @author swc
 * @date 2020/7/3 0003 下午 15:02
 */
public class EchartsPieStrategy extends BaseStrategy implements EchartsStrategy {

    @Override
    public <T>EchartsOption createOption(IEchartSeries iEchartSeries, List<T> list, HandEchartsDto handEchartsDto){
        return super.createOptionFGP(iEchartSeries,PieSeries.class,list,handEchartsDto);
    }

    @Override
    public <T> List<EchartsOption> createMulOption(IEchartSeries iEchartSeries, List<T> list, HandEchartsDto handEchartsDto) {
        return null;
    }
}
