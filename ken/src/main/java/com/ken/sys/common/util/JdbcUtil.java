package com.ken.sys.common.util;
import com.ken.sys.common.entity.DbSettingInfo;
import com.ken.sys.common.ifs.PrepareFunction;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.List;
import java.util.Properties;

public class JdbcUtil {
	 private static Connection conn = null;

	 private static  String finaUrl= "jdbc:mysql://192.168.0.118:3306/erp-sy?useUnicode=true&characterEncoding=utf-8&allowMultiQueries=true";

	 private static  String finaUser = "root";

	 private static  String finaPass = "123456";

	 //默认是 mysql
	 private static	String driver ="com.mysql.jdbc.Driver";

	 //默认最大提交数
	 private static final int COMMIT_MAX_COUNT = 2000;

	static {
		try {
			String rootPath = JdbcUtil.class.getClassLoader().getResource("").getPath();
			//读取SQL配置文件 配置文件在src下
			InputStream in = new FileInputStream( rootPath+"jdbc.properties");
			Properties properties = new Properties();
			properties.load(in);
			//获取参数
			driver = properties.getProperty("jdbc.driver");
			finaUrl = properties.getProperty("jdbc.url");
			finaUser = properties.getProperty("jdbc.username");
			finaPass = properties.getProperty("jdbc.password");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	//建立连接  根据属性文件的配置
	public static Connection getConnection(){
		return getConnection(driver,finaUrl,finaUser,finaPass);
	}

	//建立连接  根据属性文件的配置
	private static Connection getConnection(DbSettingInfo dbSettingInfo){
		if(dbSettingInfo==null){
			throw new Error("dbSettingInfo is not allowed to be null");
		}
		return getConnection(dbSettingInfo.getDriver(),dbSettingInfo.getDbUrl(),dbSettingInfo.getUserName(),dbSettingInfo.getPassword());
	}


	public static Connection getConnection(String driver,String url, String user, String pass){
		 try {
			 Class.forName(driver);
			 conn= DriverManager.getConnection(url,user,pass);
		 } catch (ClassNotFoundException e) {
			 e.printStackTrace();
		 }catch (SQLException e) {
			 e.printStackTrace();
		 }
		 return conn;
	 }
	 
	 public static void closeConnection(Connection conn, PreparedStatement stmt, ResultSet rs){
		 try {
			 if(rs!=null) {
				 rs.close();
			 }
			 if(stmt!=null) {
				 stmt.close();
			 }
			 if(conn!=null) {
				 conn.close();
			 }
		 } catch (SQLException e) {
			 e.printStackTrace();
		 }
	 }

	/**
	 * 功能描述: 批量执行sql
	 * @param dbSettingInfo
	 * @param executeSql
	 * @param list
	 * @return: void
	 * @author: swc
	 * @date: 2021-12-09 10:44
	 */
	public static <T> void executeBatch(DbSettingInfo dbSettingInfo
			,String executeSql, List<T> list){
		executeBatch(dbSettingInfo,executeSql,list,COMMIT_MAX_COUNT,null);
	}

	/**
	 * 功能描述: 批量执行sql
	 * @param dbSettingInfo
	 * @param executeSql
	 * @param list
	 * @param commitMaxCount
	 * @return: void
	 * @author: swc
	 * @date: 2021-12-09 10:44
	 */
	public static <T> void executeBatch(DbSettingInfo dbSettingInfo
			,String executeSql, List<T> list, int commitMaxCount){
		executeBatch(dbSettingInfo,executeSql,list,commitMaxCount,null);
	}


	/**
	 * 功能描述: 批量执行sql
	 * @param dbSettingInfo 数据库信息
	 * @param executeSql 执行sql
	 * @param list 数据源
	 * @param commitMaxCount 最大提交数
	 * @param prepareFunction  一般可以预处理参数 可以做外部的干预
	 * @return: void
	 * @author: swc
	 * @date: 2021-12-09 10:44
	 */
	public static <T> void executeBatch(DbSettingInfo dbSettingInfo
			,String executeSql, List<T> list, int commitMaxCount, PrepareFunction prepareFunction){

		PreparedStatement pstmt =null;
		Connection conn =null;
		try {
			commitMaxCount = commitMaxCount>0 && commitMaxCount<=COMMIT_MAX_COUNT ?commitMaxCount:COMMIT_MAX_COUNT;//默认最大提交数
			conn = getConnection(dbSettingInfo);
			conn.setAutoCommit(false);
			pstmt = conn.prepareStatement(executeSql);

			int ind =0;
			boolean hand =true;

			for (T t : list) {
				if(prepareFunction!=null){
					hand = prepareFunction.hand(t, pstmt);//参数在此设置
				}
				//不执行此条数据
				if(!hand){
					continue;
				}
				pstmt.addBatch();
				ind++;
				if(ind==commitMaxCount){
					pstmt.executeBatch();//批量提交
					pstmt.clearParameters();//清除参数
					ind =0;
				}
			}

			//不足提交最大数  仍有插入数 进行提交
			if(ind>0){
				pstmt.executeBatch();//批量提交
				pstmt.clearParameters();//清除参数
			}

			//提交，设置事务初始值
			conn.commit();
			conn.setAutoCommit(true);

		} catch (Exception e) {
			//提交失败，执行回滚操作
			try {
				if(conn!=null){
					conn.rollback();
				}
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();

		}finally {
			try {
				if(pstmt!=null){
					pstmt.close();
				}
				if(conn != null){
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}

}
