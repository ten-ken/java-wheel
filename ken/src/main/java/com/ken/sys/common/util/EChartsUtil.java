package com.ken.sys.common.util;
import com.ken.sys.common.entity.echarts.dto.HandEchartsDto;
import com.ken.sys.common.entity.echarts.series.*;
import com.ken.sys.common.entity.echarts.series.data.SeriesData;
import com.ken.sys.common.entity.echarts.series.property.SeriesAreaStyle;
import com.ken.sys.common.entity.echarts.series.property.SeriesItemStyle;
import com.ken.sys.common.entity.echarts.series.property.SeriesLabel;
import com.ken.sys.common.entity.echarts.series.property.SeriesLineStyle;
import com.ken.sys.common.ifs.IEchartSeries;

import java.util.*;

public class EChartsUtil {

    //折线图
    public static final String LINE ="line";

    //柱状图 条形图
    public static final String BAR ="bar";

    //饼状图 圆形图
    public static final String PIE ="pie";

    //漏斗图
    public static final String FUNNEL ="funnel";

    //仪表图
    public static final String GAUGE ="gauge";

    //热力图
    public static final String HEATMAP ="heatmap";

    /**
     * 功能描述: 生成折线图、柱状图、饼状图的 legendData 、xAxisData和series
     * @param type "bar"==>柱状图  or "line" 折线图 pie 饼图
     * @param iEchartSeries 接口 操作series
     * @param list 数据源
     * @param xyAxisName x或y轴需要对应的值 （实体的属性名)
     * @param legends
     * @param fields y轴需要对应的值 （单个serie对应的data[value]数据  实体对应的属性名)
     * @param saveList 每个list和fields的值是一一对应的
     * @return: java.util.Map<java.lang.String,java.util.List<java.lang.Object>>
     * @author: ken
     * @date: 2019/12/4 0004 下午 13:47
     */
    public static <T>Map<String,List<Object>> getECharOptionsToFoldLine(String type,IEchartSeries iEchartSeries, List<T> list, String xyAxisName, Object[] legends, Object[] fields, List ...saveList) {
        List<Object> namesList =new ArrayList<Object>();
        Map<String,List<Object>> map =new HashMap<String,List<Object>>();
        Number value =null;
        SeriesData data=null;
        String nameValue =null;

        for(T t:list){
            nameValue = ReflectUtils.getFieldValue(t,xyAxisName).toString();
            //折线图和条形图 设置
            if(!EChartsUtil.PIE.equalsIgnoreCase(type) && !EChartsUtil.FUNNEL.equalsIgnoreCase(type) && !EChartsUtil.GAUGE.equalsIgnoreCase(type)){
                namesList.add(nameValue);
            }
            for(int ind=0;ind<fields.length;ind++){
                value = (Number) ReflectUtils.getFieldValue(t,(String)fields[ind]);
                value = EmptyUtils.isNullOrEmpty(value)?0:value;

                data =new SeriesData();
                data.setValue(value);
                if(EChartsUtil.PIE.equalsIgnoreCase(type)||EChartsUtil.FUNNEL.equalsIgnoreCase(type)||EChartsUtil.GAUGE.equalsIgnoreCase(type)){
                    data.setName(nameValue);
                    namesList.add(nameValue);
                }
                //设置单个数据 样式
                if(!EmptyUtils.isNullOrEmpty(iEchartSeries)){
                    iEchartSeries.setSeriesDataProp(data,new SeriesLabel(),new SeriesItemStyle(),new SeriesAreaStyle(),new SeriesLineStyle());
                }
                saveList[ind].add(data);
            }
        }

        List<Object> backList =new ArrayList<Object>();
        EchartBaseSeries echartBaseSeries =null;
        for(int ind=0;ind<fields.length;ind++){
            //获取指定的series
            echartBaseSeries = getFitSeries(type,echartBaseSeries);
            echartBaseSeries.setName((String)legends[ind]);
//            echartBaseSeries.setType(type);
            echartBaseSeries.setData(saveList[ind]);
            //设置整体样式[如一条折线或条状图的样式]
            if(!EmptyUtils.isNullOrEmpty(iEchartSeries)){
                iEchartSeries.setSeriesDetailProp(echartBaseSeries,new SeriesLabel(),new SeriesItemStyle(),new SeriesAreaStyle(),new SeriesLineStyle());
            }
            backList.add(echartBaseSeries);
        }

        map.put("xAxisData",namesList);
        map.put("series",backList);

        if("pie".equalsIgnoreCase(type) || "funnel".equalsIgnoreCase(type)){
            map.put("legendData",namesList);
        }else{
            map.put("legendData",Arrays.asList(legends));
        }
        return map;
    }

    /**
     * 功能描述: 获取适配的series
     * @param type
     * @param series
     * @return: com.ken.sys.common.entity.echarts.EchartBaseSeries
     * @author: swc
     * @date: 2020/3/19 0019 上午 10:00
     */
    private static EchartBaseSeries getFitSeries(String type, EchartBaseSeries series) {
        type =type.toLowerCase();
        if(EChartsUtil.LINE.equalsIgnoreCase(type)){
            series = new LineSeries();
        }
        else if(EChartsUtil.BAR.equalsIgnoreCase(type)){
            series = new BarSeries();
        }
        else if(EChartsUtil.PIE.equalsIgnoreCase(type)){
            series = new PieSeries();
        }
        else if(EChartsUtil.FUNNEL.equalsIgnoreCase(type)){
            series = new FunnelSeries();
        }
        else if(EChartsUtil.GAUGE.equalsIgnoreCase(type)){
            series = new GaugeSeries();
        }
        else if(EChartsUtil.HEATMAP.equalsIgnoreCase(type)){
            series = new HeatMapSeries();
        }
        else{
            series = new EchartBaseSeries();
        }
        return series;
    }


    //获取条形图、折线图、饼图的option
    public static <T>Map<String,List<Object>> getECharOptionsToFoldLine(String type, List<T> list, String nameField, Object[] legends, Object[] fields, List ...saveList) {
        return getECharOptionsToFoldLine(type,null,list,nameField,legends,fields,saveList);
    }

    /**
     * 功能描述:
     * @param type 类型
     * @param iEchartSeries 接口
     * @param t  对象
    //     * @param titleFieldName
    //     * @param subFieldName 对象里面需要处理的list数据
    //     * @param xDimension 维度x轴
    //     * @param yDimension 维度y
    //     * @param xAxisDataName x轴需要展示的信息
    //     * @param yAxisDataName y轴需要展示的信息
    //     * @param showValField 需要展示数据的列
     * @return: java.util.Map<java.lang.String,java.lang.Object>
     * @author: swc
     * @date: 2020/7/2 0002 下午 12:52
     */
    public static <T>Map<String,Object> createHeatMapOption(String type, IEchartSeries iEchartSeries, T t, HandEchartsDto handEchartsDto) {
        //String titleFieldName,String subFieldName,String xDimension,String yDimension,String xAxisDataName,String yAxisDataName,String showValField
        String titleFieldName = handEchartsDto.getTitleFieldName();

        String subFieldName = handEchartsDto.getSubFieldName();

        String xDimension = handEchartsDto.getxDimension();

        String yDimension =  handEchartsDto.getyDimension();

        String xAxisDataName = handEchartsDto.getxAxisDataName();

        String yAxisDataName =  handEchartsDto.getyAxisDataName();

        String showValField =  handEchartsDto.getShowValField();

        String showDateField =  handEchartsDto.getShowDateField();

        String coordinateSystem =  handEchartsDto.getCoordinateSystem();

        Map<String,Object> map =new HashMap<String,Object>();


        //标题
        String titleName =null;
        if(!EmptyUtils.isNullOrEmpty(titleFieldName)){
            titleName =  ReflectUtils.getFieldValue(t,titleFieldName).toString();
        }

        List subList  = (List)ReflectUtils.getFieldValue(t,subFieldName);

        Set<String> xAxisData =new TreeSet<String>();

        Set<String> yAxisData =new TreeSet<String>();

        //数据值---单个数据
        List seriesDataValue ;

        EchartBaseSeries echartBaseSeries = getFitSeries(type,null);


        if("calendar".equals(coordinateSystem)){
            ((HeatMapSeries)echartBaseSeries).setCoordinateSystem("calendar");
            ((HeatMapSeries)echartBaseSeries).setCalendarIndex(handEchartsDto.getCalendarIndex());
        }


        List<SeriesData> seriesDataList = new ArrayList<SeriesData>();
        SeriesData seriesData =null;

        String xAxisValue =null;
        String yAxisValue =null;
        // 维度X
        String xDim = null;
        // 维度y
        String yDim = null;
        for(int i=0;i<subList.size();i++){
            seriesData =new SeriesData();

            if(!EmptyUtils.isNullOrEmpty(xDimension)){
                // 维度X
                xDim =  ReflectUtils.getFieldValue(subList.get(i),xDimension).toString();
            }

            if(!EmptyUtils.isNullOrEmpty(xDimension)){
                // 维度y
                yDim =  ReflectUtils.getFieldValue(subList.get(i),yDimension).toString();
            }


            //x轴的值
            if(!EmptyUtils.isNullOrEmpty(xAxisDataName)){
                xAxisValue =  ReflectUtils.getFieldValue(subList.get(i),xAxisDataName).toString();
                xAxisData.add(xAxisValue);
            }

            //y轴的值
            if(!EmptyUtils.isNullOrEmpty(yAxisDataName)){
                yAxisValue =  ReflectUtils.getFieldValue(subList.get(i),yAxisDataName).toString();
                yAxisData.add(yAxisValue);
            }

            seriesDataValue =new ArrayList();
//            seriesDataValue.add(Integer.parseInt(xDim)-1);
//            seriesDataValue.add(Integer.parseInt(yDim)-1);

            //目前支持cartesian2d出现x-y维度
            if("cartesian2d".equals(coordinateSystem)){
                seriesDataValue.add(xDim);
                seriesDataValue.add(yDim);
            }

            //coordinateSystem为calendar series里面的data-[value]为时间+某项值的数组
            if("calendar".equals(coordinateSystem)){
                Object showDate =EmptyUtils.isNullOrEmpty(showDateField)?0:ReflectUtils.getFieldValue(subList.get(i),showDateField);
                seriesDataValue.add(showDate);
            }


            Object showVal =EmptyUtils.isNullOrEmpty(showValField)?0:ReflectUtils.getFieldValue(subList.get(i),showValField);
            seriesDataValue.add(showVal);

            seriesDataValue.add(subList.get(i));

            //设置值
            seriesData.setValue(seriesDataValue);
//            seriesData.setVisualMap(false);

            if(!EmptyUtils.isNullOrEmpty(iEchartSeries)){
                iEchartSeries.setSeriesDetailProp(echartBaseSeries,new SeriesLabel(),new SeriesItemStyle(),new SeriesAreaStyle(),new SeriesLineStyle());
                iEchartSeries.setSeriesDataProp(seriesData,new SeriesLabel(),new SeriesItemStyle(),new SeriesAreaStyle(),new SeriesLineStyle());
            }

            seriesDataList.add(seriesData);

            echartBaseSeries.setData(seriesDataList);
        }
        map.put("title",titleName);
        map.put("xAxisData",xAxisData);
        map.put("yAxisData",yAxisData);
        map.put("series",echartBaseSeries);
        return map;
    }

    /**
     * 功能描述:
     * @param type 类型
     * @param iEchartSeries 接口
     * @param list 对象
    //     * @param titleFieldName
    //     * @param subFieldName 对象里面需要处理的list数据
    //     * @param xDimension 维度x轴
    //     * @param yDimension 维度y
    //     * @param xAxisDataName x轴需要展示的信息
    //     * @param yAxisDataName y轴需要展示的信息
    //     * @param showValField 需要展示数据的列
     * @return: java.util.Map<java.lang.String,java.lang.Object>
     * @author: swc
     * @date: 2020/7/2 0002 下午 12:52
     */
    public static <T>Map<String,Object> createHeatMapOption(String type, IEchartSeries iEchartSeries, List<T> list, HandEchartsDto handEchartsDto) {
        Map<String,Object> map =new HashMap<String,Object>();



        String coordinateSystem =  handEchartsDto.getCoordinateSystem();

        Map<String, Object> heatMapOption =null;

        EchartBaseSeries heatMapSeries =null;

        //拼接不同年份日历的series的值
        List<EchartBaseSeries> seriesList =new ArrayList<EchartBaseSeries>();

        for(int i=0;i<list.size();i++){
            handEchartsDto.setCalendarIndex(i);
            //如果coordinateSystem 是 cartesian2d  默认为 多个[option]热力图
            if("cartesian2d".equalsIgnoreCase(coordinateSystem)){
                //map数据的key为 0,1,2 下标  里面才是时间热力图需要的series x-y轴需要的数据
                map.put(String.valueOf(i),createHeatMapOption(type,iEchartSeries,list.get(i),handEchartsDto));
            }
            //如果coordinateSystem 是 calendar  默认为 日历 则是一个option的热力图  series的值为多个 而不是一个
            if("calendar".equalsIgnoreCase(coordinateSystem)){
                //map数据的key为 0,1,2 下标  里面才是时间热力图需要的series x-y轴需要的数据
                heatMapOption = createHeatMapOption(type, iEchartSeries, list.get(i), handEchartsDto);
//                heatMapSeries = (HeatMapSeries)heatMapOption.get("series");
                heatMapSeries =  (HeatMapSeries)heatMapOption.get("series");
                seriesList.add(heatMapSeries);
            }
        }

        if("calendar".equalsIgnoreCase(coordinateSystem)){
//            heatMapSeries.setData(seriesDataList);
            heatMapOption.put("series",seriesList);
            return heatMapOption;
        }

        return map;
    }


}
