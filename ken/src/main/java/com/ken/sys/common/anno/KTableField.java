package com.ken.sys.common.anno;

import java.lang.annotation.*;

@Documented
@Target({ElementType.FIELD,ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface KTableField {

    /**
     * 是否主键
     * @return
     */
    boolean isPrimary() default false;


    /**
     * 是否数据库表数据
     * @return
     */
    boolean exist() default true;

    /**
     * 是否自增长
     * @return
     */
    boolean  autoIncrement() default false;

    /**
     * 定义类型
     * @return
     */
    String defineType() default "";

    /**
     * 设置别名
     * @return
     */
    String alias() default "";

    /**
     * 设置字段长度
     * @return
     */
    int length() default 0;


    /**
     * 设置字段精度
     * @return
     */
    int precision() default 0;

    /**
     * 可以为空？
     * @return
     */
    boolean isNullAble() default true;

    /**
     * 中文备注
     * @return
     */
    String comment() default "";


    /**
     * jdbc的类型
     * @return
     */
    String jdbcType() default "varchar";
}
