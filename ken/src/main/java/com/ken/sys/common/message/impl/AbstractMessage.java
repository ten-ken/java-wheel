
package com.ken.sys.common.message.impl;
import com.alibaba.fastjson.JSONObject;
import com.ken.sys.common.message.Message;

/**
 * <ul>
 * <li>Title:     -AbstractMessage</li>
 * <li>Description: TODO </li>
 * <li>Copyright: Copyright (c) 2018</li>
 * <li>微信:tianji_vip</li>
 * </ul>
 *
 * @author swc
 * @version
 * @date 2019/9/2 0002 下午 13:07
 */
public abstract class AbstractMessage implements Message {
    protected JSONObject json;

    public AbstractMessage() {
    }

    protected JSONObject currentJson() {
        return this.json == null ? new JSONObject() : this.json;
    }

    public String toString() {
        return this.buildJSON().toString();
    }
}
