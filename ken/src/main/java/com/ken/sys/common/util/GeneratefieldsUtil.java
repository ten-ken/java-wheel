package com.ken.sys.common.util;

import java.math.BigDecimal;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author ken
 * @date 2020-01-11
 * @description ： 在数据库新增字段后 利用这个工具类
 *
 */
public class GeneratefieldsUtil {

    public static void main(String[] args) throws Exception{

        //able_name 表名   table_schema 数据库名 查询结果为 字段名称和字段类型
        String sql ="SELECT GROUP_CONCAT(column_name) column_name,GROUP_CONCAT(data_type) data_type FROM information_schema.COLUMNS WHERE able_name = 't_order_info' AND table_schema = 'test'";

        String filedStr ="id,name,account,user_id,pay_amout_rate";//利用上述的sql在数据库查询出来即可
        String jdbcTypeStr ="int,varchar,decimal,bigint,decimal";
        String tableName ="t_order_info";
        String mainField ="";//查询条件的数据库字段
        boolean useBigDecimal =true;

        //这个是数据库字段 需要的复制字段出来即可
        String[] fields =filedStr.split(",");
        String[] jdbcTypes =jdbcTypeStr.split(",");

        GeneratefieldsUtil.generateLocalSql(fields,tableName,mainField,jdbcTypes,useBigDecimal);
    }

    /**
     *
     * @param fields  fields 需要处理的字段
     * @param tableName 表名
     * @param mainField 更新查询条件的主自动 默认为id
     * @param jdbcTypes 数据库字段类型
     * @param useBigDecimal 是否double 和 float 也使用精确计算 ==> BigDecimal
     */
    private static void generateLocalSql(String[] fields, String tableName, String mainField, String[] jdbcTypes,boolean useBigDecimal) {
        if(mainField==null || "".equals(mainField.trim())){
            mainField = "id";
        }
        String mainFieldV= mainField;

        StringBuffer totalMessage = new StringBuffer();
        String finalOv =null;
        Pattern r = Pattern.compile("(_|-)\\w");

        //单个新增
        StringBuffer sbf_add1 =new StringBuffer();
        StringBuffer sbf_add2 =new StringBuffer();

        //单个选择性新增
        StringBuffer sbf_add_sel1 =new StringBuffer();
        StringBuffer sbf_add_sel2 =new StringBuffer();

        //批量新增
//        StringBuffer sbf_batch_add_sel1 =new StringBuffer();
        StringBuffer sbf_batch_add_sel2 =new StringBuffer();

        //单个更新
        StringBuffer sbf_update =new StringBuffer();
        //单个选择性更新
        StringBuffer sbf_update_sel =new StringBuffer();
        //批量新增
        StringBuffer sbf_batch_update =new StringBuffer();
        //选择性批量新增
        StringBuffer sbf_batch_sel_update =new StringBuffer();

        //实体的内容
        StringBuffer sbf_entity =new StringBuffer();

        String jdbc = "";
        String jdbcType = "";

        //查询部分替换实体属性
//        Pattern pt = Pattern.compile("(_|-)\\w");
        Matcher first = r.matcher(mainFieldV);
        while (first.find()){
            mainFieldV = mainFieldV.replace(first.group(),first.group().toUpperCase().replaceAll("(_|-)",""));
        }

        //String field : fields
        for(int i=0;i<fields.length;i++){
            finalOv =fields[i];
            Matcher m = r.matcher(fields[i]);
            while (m.find()){
                fields[i] = fields[i].replace(m.group(),m.group().toUpperCase().replaceAll("(_|-)",""));
            }

            if(jdbcTypes!=null && jdbcTypes.length>0){
                if("int".equalsIgnoreCase(jdbcTypes[i])){
                    jdbcTypes[i] ="INTEGER";
                }
                jdbc = (jdbcTypes[i]!= null&&jdbcTypes[i].trim()!="")? ",jdbcType="+jdbcTypes[i].toUpperCase():"";
                jdbcType = jdbcTypes[i];
            }

            sbf_add1.append(templateInsertTop ().replace("&jdbcType&",jdbc).replace("${lower}",finalOv));
            sbf_add2.append(templateInsertBottom ().replace("&jdbcType&",jdbc).replace("${upper}",fields[i]));

            sbf_add_sel1.append(templateSelectInsertTop().replace("&jdbcType&",jdbc).replace("${lower}",finalOv).replace("${upper}",fields[i]));
            sbf_add_sel2.append(templateSelectInsertBottom().replace("&jdbcType&",jdbc).replace("${lower}",finalOv).replace("${upper}",fields[i]));

            sbf_batch_add_sel2.append(templateInsertBottom ().replace("&jdbcType&",jdbc).replace("${upper}","item."+fields[i]));

            sbf_update.append(templateUpdate().replace("&jdbcType&",jdbc).replace("${lower}",finalOv).replace("${upper}",fields[i]));
            sbf_update_sel.append(templateSelectUpdate().replace("&jdbcType&",jdbc).replace("${lower}",finalOv).replace("${upper}",fields[i]));
            sbf_batch_update.append(templateBatchUpdate(false).replace("&jdbcType&",jdbc).replace("${lower}",finalOv).replace("${upper}",fields[i]));
            sbf_batch_sel_update.append(templateBatchUpdate(true).replace("&jdbcType&",jdbc).replace("${lower}",finalOv).replace("${upper}",fields[i]));


            sbf_entity.append(templateEntity().replace("${lower}",finalOv).replace("${upper}",fields[i]).replace("&entityType&",getEntityType(jdbcType,useBigDecimal)));

        }

        totalMessage.append("单个新增;\n");
        totalMessage.append("insert into "+tableName+"( "+  sbf_add1.toString().substring(0,sbf_add1.toString().length()-1)+" )\n"+ "values " +
                "( "+sbf_add2.toString().substring(0,sbf_add2.toString().length()-1)+" )");

        totalMessage.append("\n\n");

        totalMessage.append("单个选择性新增;\n");
        totalMessage.append("insert into "+tableName+"\n <trim prefix=\"(\" suffix=\")\" suffixOverrides=\",\">\n"+sbf_add_sel1.toString()+" </trim>"+
        "\n<trim prefix=\"values (\" suffix=\")\" suffixOverrides=\",\">\n"+sbf_add_sel2.toString()+" </trim>");

        totalMessage.append("\n\n");

        totalMessage.append("批量新增;\n");
        totalMessage.append("INSERT INTO "+tableName+"("+sbf_add1.toString().substring(0,sbf_add1.toString().length()-1)+" )\n VALUES\n"
        +" <foreach collection=\"list\" item=\"item\" separator=\",\">\n\t("+sbf_batch_add_sel2.toString().substring(0,sbf_batch_add_sel2.toString().length()-1)+" )\n"+" </foreach>");

        totalMessage.append("\n\n");
        totalMessage.append("单个更新;\n");
        totalMessage.append("update "+tableName +"\n\t set "+sbf_update.toString().substring(0,sbf_update.toString().length()-1)+" where "+mainField+" = #{"+mainFieldV+"}");

        totalMessage.append("\n\n");
        totalMessage.append("单个选择性更新;\n");
        totalMessage.append("update "+tableName +"\n<set>\n" +sbf_update_sel.toString()+"</set>\n"+"where "+mainField+" = #{"+mainFieldV+"}");

        totalMessage.append("\n\n");
        totalMessage.append("批量更新;\n");
        totalMessage.append("<foreach collection=\"list\" item=\"item\" index=\"index\" open=\"\" close=\"\" separator=\";\">\n"+
                " UPDATE "+tableName +"\n<set>\n\t"
                 +sbf_batch_update.toString().substring(0,sbf_batch_update.length()-1)+"\n" +
                "</set>\n<where>\n" +
                " "+mainField+" = #{item."+mainFieldV+"}\n" +
                "</where>\n</foreach>");

        totalMessage.append("\n\n");
        totalMessage.append("选择性批量更新;\n");
        totalMessage.append("<foreach collection=\"list\" item=\"item\" index=\"index\" open=\"\" close=\"\" separator=\";\">\n"+
                " UPDATE "+tableName +"\n<set>"
                +sbf_batch_sel_update.toString().substring(0,sbf_batch_sel_update.length()-1)+"\n" +
                "</set>\n<where>\n" +
                " "+mainField+" = #{item."+mainFieldV+"}\n" +
                "</where>\n</foreach>");

        totalMessage.append("\n\n");
        totalMessage.append("实体的内容;\n");
        totalMessage.append(sbf_entity);

        System.out.print( totalMessage.toString());
    }

    //获取实体类型
    public static String getEntityType(String source,boolean useBigDecimal) {
      String type="";
      if("varchar".equalsIgnoreCase(source)){
          type ="String";
      }else if("decimal".equalsIgnoreCase(source)){
          type ="BigDecimal";
      }else if("double".equalsIgnoreCase(source)){
          type = useBigDecimal ?"BigDecimal":"Double";
      }else if("float".equalsIgnoreCase(source)){
          type = useBigDecimal ?"BigDecimal":"Float";
      }
      else if("float".equalsIgnoreCase(source)){
          type = useBigDecimal ?"BigDecimal":"Float";
      }
      else if("int".equalsIgnoreCase(source)){
          type ="Integer";
      }else if("Integer".equalsIgnoreCase(source)){
          type ="Integer";
      }
      else if("bigint".equalsIgnoreCase(source)){
          type ="Long";
      }else if("date".equalsIgnoreCase(source)){
          type ="Date";
      }else if("datetime".equalsIgnoreCase(source)){
          type ="Date";
      }else if("time".equalsIgnoreCase(source)){
          type ="Date";
      } else if("timestamp".equalsIgnoreCase(source)){
          type ="Date";
      }else if("text".equalsIgnoreCase(source)){
          type ="String";
      }else{
          type ="String";
      }
      return type;
    }


    public  static String templateInsertTop (){
        String content ="${lower},";
        return content;
    }

    public  static String templateInsertBottom (){
        String content ="#{${upper}&jdbcType&},";
        return content;
    }


    //选择性插入的头部模板
    public  static String templateSelectInsertTop (){
        String content =" <if test=\"${upper} != null\">\n" +
                "    ${lower},\n" +
                " </if>\n";
        return content;
    }
    //选择性插入的尾部模板
    public  static String templateSelectInsertBottom(){
        String content ="<if test=\"${upper} != null\">\n" +
                "    #{${upper}&jdbcType&},\n" +
                "</if>\n";
        return content;
    }

    //选择性更新模板
    public  static String templateSelectUpdate(){
        String content ="<if test=\"${upper} != null\">\n" +
                "     ${lower} = #{${upper}&jdbcType&},\n" +
                "</if>\n";

        return content;
    }

    //单个更新模板
    public  static String templateUpdate(){
        String content =" ${lower}= #{${upper}&jdbcType&},\n" ;
        return content;
    }

    //批量更新模板==>select为是否选择性更新
    public  static String templateBatchUpdate(boolean select){
        String content =" ${lower}= #{item.${upper}&jdbcType&},\n" ;
        if(select){
            content ="\n<if test=\"item.${upper} !=null\">\n" +
                    "      ${lower} = #{item.${upper}&jdbcType&},\n" +
                    " </if>";
        }
        return content;
    }


    //实体内容
    public  static String templateEntity(){
        String content ="private &entityType& ${upper}\n" ;
        return content;
    }
}
