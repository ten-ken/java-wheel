package com.ken.sys.common.util;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ken.sys.common.anno.JsonAlias;
import com.ken.sys.common.anno.KTableField;
import com.ken.sys.common.anno.TableEntity;
import com.ken.sys.common.anno.KTableName;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.*;

/**
 * @despriction 根据实体反向生成sql
 * @author swc
 * @date 2018-11-12
 */
public class EntityToSqlUtil {

    public static final String author = "swc";

    private static Map<String,String> map =new HashMap<>();

    static{
        map.put("int","int(#{intlen})");
        map.put("short","int(#{intlen})");
        map.put("long","bigint(#{intlen})");
        map.put("java.lang.Long","bigint(#{intlen})");
        map.put("byte","TINYINT(1)");
        map.put("boolean","tinyint(1)");
        map.put("double","double(#{intlen},#{prec})");
        map.put("float","float(#{intlen},#{prec})");
        map.put("char","varchar(1)");
        map.put("String","varchar(#{strlen})");
        map.put("java.util.Date","datetime");
        map.put("date","date");
        map.put("datetime","datetime");
        map.put("java.math.BigDecimal","decimal(#{intlen},#{prec})");//默认精确两位小数
        map.put("java.lang.Integer","int(#{intlen})");
        map.put("java.lang.String","varchar(#{strlen})");
        map.put("java.lang.Double","double(#{intlen},#{prec})");
        map.put("java.lang.Float","float(#{intlen},#{prec})");
        map.put("java.lang.Short","int(#{intlen},#{prec})");
        map.put("java.lang.Boolean","int(#tinyint(1)");
        map.put("timestamp","timestamp");
    }

    /**
     * 自动生成sql
     * @return
     */
    public static void generateMySql(Class[] classes)  {
        StringBuffer sqlSbf =new StringBuffer();
        String mysql = "SET NAMES utf8mb4;\n" +
                "SET FOREIGN_KEY_CHECKS = 0;\n";

        for(Class clazz:classes){
            sqlSbf.append(createMySql(clazz));
        }
        System.out.println(mysql+sqlSbf.toString());
    }


    /**
     * 创建数据库sql
     *
     * @param clazz
     * @return
     */
    private static String createMySql(Class clazz) {
        Annotation tnAnno = clazz.getAnnotation(KTableName.class);
        StringBuffer sbf = new StringBuffer();

        if (tnAnno == null) {
            return "";
        }

        KTableName KTableName = (KTableName) tnAnno;//表上注解
        boolean isCover = KTableName.isCover();//是否覆盖表
        String tableComment ="".equals(KTableName.comment())?"" : " COMMENT '"+ KTableName.comment()+"'";//表注释
        String tname = "".equals(KTableName.value()) ? clazz.getSimpleName().toLowerCase() : KTableName.value();//数据库表名

        String primary = "";
        String foot = "";

        if (isCover) {
            primary = "PRIMARY KEY (#{primary}) USING BTREE)";
            sbf.append("   -- ----------------------------\n" +
                    "-- Table structure for " + tname + "\n" +
                    "-- ----------------------------\n");
            sbf.append(" DROP TABLE IF EXISTS " + tname + ";\n");
            sbf.append(" CREATE TABLE " + tname + " (\n");
            foot = "ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci "+tableComment+" ROW_FORMAT = Compact;\n";
        }
        Field[] files = clazz.getDeclaredFields();
        String key = "";

        int index=0;
        for (Field cFile : files) {
            Annotation stAnno = cFile.getAnnotation(KTableField.class);
            TableEntity params =getSetTableInfo(stAnno,cFile,index);
            if (isCover) {//覆盖原表
                if (params.isPrimary()) {
                    key+= params.getFieldAlias() + ",";
                }
                sbf.append( "" + params.getFieldAlias() + " "+params.getSqlType()+" " + params.getIsNullAble() + " " + params.getAutoIncrement() +params.getComment()+",\n");
            }else{
                sbf.append( "ALTER TABLE "+tname+" DROP "+params.getFieldAlias()+";\n");
                sbf.append( "ALTER TABLE "+tname+" ADD "+params.getFieldAlias()+" "+params.getSqlType()+" "+params.getIsNullAble()+" "+params.getComment()+"; \n");
            }
            index++;
        }
        if(key.length()>0){
            key = key.substring(0,key.length()-1);
        }
        primary =primary.replace("#{primary}",key);
        sbf.append(primary+foot);
        return sbf.toString();
    }

    /**
     * 封装获取的属性
     * @param stAnno
     * @param cFile
     * @return
     */
    public static TableEntity getSetTableInfo(Annotation stAnno , Field cFile, int index) {
        TableEntity entity =new TableEntity();
        String fieldAlias =null;
        String autoIncrement ="";
        String isNullAble ="";
        String comment ="";
        String filedType ="";
        int length =0;
        int precision =0;
        String sqlType="";
        boolean isPrimary =false;
        if (stAnno != null) {
            KTableField KTableField = (KTableField) stAnno;//字段注解
            fieldAlias = "".equals(KTableField.alias()) ? cFile.getName().toLowerCase() : KTableField.alias();
            autoIncrement = KTableField.autoIncrement() ? "AUTO_INCREMENT" : "";//自增长
            isNullAble = KTableField.isNullAble() ? "" : "NOT NULL";//可为空?
            comment ="".equals(KTableField.comment())?"" : " COMMENT '"+ KTableField.comment()+"'";//中文注释
            filedType = "".equals(KTableField.defineType()) ? cFile.getType().toString() : KTableField.defineType();//自定义字段类型
            length = KTableField.length();
            precision = KTableField.precision();
            sqlType = getsqlType(filedType, length,precision);
            isPrimary= KTableField.isPrimary();
        }else{
            fieldAlias = cFile.getName().toLowerCase();
            filedType =cFile.getType().toString();//自定义字段类型
            sqlType = getsqlType(filedType, length,precision);
        }
        if(!isPrimary && index==0){//默认第一个属性是主键
            isPrimary =true;
        }
        entity.setSqlType(sqlType);
        entity.setFieldAlias(fieldAlias);
        entity.setAutoIncrement(autoIncrement);
        entity.setIsNullAble(isNullAble);
        entity.setComment(comment);
        entity.setSqlType(sqlType);
        entity.setLength(length);
        entity.setPrecision(precision);
        entity.setPrimary(isPrimary);
        return entity;
    }

    /**
     * 默认的字段匹配数据库类型和长度
     * @param length
     * @param precision
     * @return
     */
    public static String getsqlType(String filedType, int length, int precision) {
        String type=filedType.replace("class","").trim();
        int intlen=length==0?12:length;//默认数字类型长的
        int strlen=length==0?15:length;//默认字符串长度
        int prec= (length>precision && precision==0)?precision:2;
        if(map.containsKey(type)){
            return  map.get(type).replace("#{intlen}",String.valueOf(intlen))
                    .replace("#{prec}",String.valueOf(prec))
                    .replace("#{strlen}",String.valueOf(strlen));

        }
        return "";
    }


    public static <T> String creatInsertSql(JSONArray jsonArray, Class<T> tClass) {
        StringBuffer headSbf =new StringBuffer();
        StringBuffer contentSbf =new StringBuffer();
        StringBuffer valueTemplate =new StringBuffer();
        String value ="";
        try {
            List<T> list =new ArrayList<T>();
            T t = tClass.newInstance();
            KTableName KTableName = tClass.getAnnotation(KTableName.class);
            if(KTableName !=null){
                headSbf.append(" insert into "+ KTableName.value()+"(\n");
                contentSbf.append("\nvalues\n");
            }

            Field[] accessibleField = ReflectUtils.getAccessibleField(t);
            Map<String, KTableField> map = getStringTableFieldMap(accessibleField,valueTemplate);
            String template ="";

            JSONObject obj =null;
            for (int i = 0; i < jsonArray.size(); i++) {
                obj = jsonArray.getJSONObject(i);
                template =valueTemplate.toString();

                //以实体主 防止json取值的随机性导致数据错位
                Set<String> set = map.keySet();
                for (String skey:set ) {
                    if(i==0){
                        headSbf.append(map.get(skey).alias()+",");
                    }
                    if("date".equalsIgnoreCase(map.get(skey).jdbcType())){
                        value = DateUtil.dateToStr((Date) obj.get(t),"YYYY-MM-dd");
                    }else if("datetime".equalsIgnoreCase(map.get(skey).jdbcType())){
                        value = DateUtil.dateToStr((Date) obj.get(skey),"YYYY-MM-dd HH:mm:ss");
                    }else{
                        value =  obj.get(skey)!=null? obj.get(skey).toString():"";
                    }
                    template  = template.replace("#{"+map.get(skey).alias()+"}",value);
                }
                contentSbf.append(template+",");
            }

            headSbf = headSbf.replace(headSbf.length()-1,headSbf.length(),"");
            headSbf.append("\n)\n ");
            contentSbf = contentSbf.replace(contentSbf.length()-1,contentSbf.length(),"");
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        headSbf.append(contentSbf);
        return headSbf.toString();
    }

    public static <T> String creatInsertSql(List<T> list, Class<T> tClass) {
        StringBuffer headSbf =new StringBuffer();
        StringBuffer contentSbf =new StringBuffer();
        String value ="";
        String  template ="";
        try {
            T t = tClass.newInstance();
            KTableName KTableName = tClass.getAnnotation(KTableName.class);

            StringBuffer valueTemplate =new StringBuffer();

            if(KTableName !=null){
                headSbf.append(" insert into "+ KTableName.value()+"(\n");
                contentSbf.append("\nvalues\n");
            }

            Field[] accessibleField = ReflectUtils.getAccessibleField(t);
            Map<String, KTableField> map = getStringTableFieldMap(accessibleField,valueTemplate);

            for (int i = 0; i < list.size(); i++) {
                t = list.get(i);
                //以实体主 防止json取值的随机性导致数据错位
                Set<String> set = map.keySet();
                template = valueTemplate.toString();
                for (String skey:set ) {
                    if(i==0){
                        headSbf.append(map.get(skey).alias()+",");
                    }
                    if("date".equalsIgnoreCase(map.get(skey).jdbcType())){
                        value = DateUtil.dateToStr((Date) ReflectUtils.getFieldValue(t,skey),"YYYY-MM-dd");
                    }else if("datetime".equalsIgnoreCase(map.get(skey).jdbcType())){
                        value = DateUtil.dateToStr((Date) ReflectUtils.getFieldValue(t,skey),"YYYY-MM-dd HH:mm:ss");
                    }else{
                        value = ReflectUtils.getFieldValue(t,skey).toString();
                    }
                    template  = template.replace("#{"+map.get(skey).alias()+"}",value);
                }
                contentSbf = contentSbf.replace(contentSbf.length()-1,contentSbf.length(),"");
            }

            headSbf = headSbf.replace(headSbf.length()-1,headSbf.length(),"");
            headSbf.append("\n)\n ");
            contentSbf = contentSbf.replace(contentSbf.length()-1,contentSbf.length(),"");
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        headSbf.append(contentSbf);
        return headSbf.toString();
    }

    private static Map<String, KTableField> getStringTableFieldMap(Field[] accessibleField, StringBuffer valueTemplate) {
        Map<String,KTableField> map =new TreeMap<String,KTableField>();
        valueTemplate.append("(\n");
        for (Field field:accessibleField) {
            JsonAlias annotation = field.getAnnotation(JsonAlias.class);
            KTableField KTableField = field.getAnnotation(KTableField.class);
            if(annotation!=null){
                map.put(annotation.value(), KTableField);
                valueTemplate.append("'#{"+ KTableField.alias()+"}',");
            }
        }

        valueTemplate = valueTemplate.append(")");

        if(valueTemplate!=null){
            valueTemplate =valueTemplate.replace(valueTemplate.length()-2,valueTemplate.length()-1,"");

        }
        return map;
    }
}

