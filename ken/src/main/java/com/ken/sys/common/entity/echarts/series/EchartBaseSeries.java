package com.ken.sys.common.entity.echarts.series;

import com.ken.sys.common.entity.echarts.series.data.SeriesData;
import com.ken.sys.common.entity.echarts.series.property.SeriesItemStyle;
import com.ken.sys.common.entity.echarts.series.property.SeriesLabel;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author swc
 */
public class EchartBaseSeries {
    private String name;

    private String id;

    //图表标线。
    private Object markLine ;

    //本系列特定的 tooltip 设定。
    private Object tooltip;

    //在直角坐标系上有效。
    private Object emphasis;

    //标记的图形。
    private Object  markPoint;

    //图形是否不响应和触发鼠标事件，默认为 false，即响应和触发鼠标事件。
    private boolean silent;

    //图表标域，常用于标记图表中某个范围的数据，例如标出某段时间投放了广告。
    private Object markArea;

    //图表类型 默认为line
    private String type ="line";

    private BigDecimal value;
    //data属性
    private List<SeriesData> data;
    private String stack;
    //设置图形上的文本标签【条状图、折线图】---针对所有条形图 折线图
    private SeriesLabel label;

    //设置图形样式【条状图、折线图】--- 针对的条状图、折线图【柱条的颜色、柱条的描边颜色等等】
    private SeriesItemStyle itemStyle;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStack() {
        return stack;
    }

    public void setStack(String stack) {
        this.stack = stack;
    }

    public SeriesLabel getLabel() {
        return label;
    }

    public void setLabel(SeriesLabel label) {
        this.label = label;
    }

    public SeriesItemStyle getItemStyle() {
        return itemStyle;
    }

    public void setItemStyle(SeriesItemStyle itemStyle) {
        this.itemStyle = itemStyle;
    }

    public List<SeriesData> getData() {
        return data;
    }

    public void setData(List<SeriesData> data) {
        this.data = data;
    }

}
