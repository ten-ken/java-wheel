package com.ken.sys.common.test;

import com.ken.sys.common.entity.WzForeignCompanyVo;
import com.ken.sys.common.ifs.IExcelListHander;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <ul>
 * <li>Title: XXService</li>
 * <li>Description: TODO </li>
 * </ul>
 *
 * @author swc
 * @date 2021/4/12 0012 下午 15:44
 */
@Service
public class XXService implements IExcelListHander {




    //@Autowired
    // xxx  xxx

    // 实际操作 应该是这样的伪代码
    /*public void doSomeThing() throws Exception{
        RowExcelHandler<WzForeignCompanyVo> tAppUserInfoPORowExcelHandler = new RowExcelHandler<WzForeignCompanyVo>(new WzForeignCompanyVo(), 1,this);
        //tAppUserInfoPORowExcelHandler.
        Map map = ExcelReaderSaxUtil.readExcel("63w.xlsx", new FileInputStream("C:\\Users\\Administrator\\Desktop\\测试\\63w.xlsx"), tAppUserInfoPORowExcelHandler);

    }*/


    @Override
    public <T> long execute(List<T> list, int sheetIndex, long batchDoNums) {
        //System.out.println("当前sheetIndex:"+sheetIndex);
        if(list!=null && list.get(0) instanceof WzForeignCompanyVo){
            List<WzForeignCompanyVo> curList = (List<WzForeignCompanyVo>)list;
            //数据达到2000条 插入数据库
            if(list.size()==2000){
                list.clear();
                batchDoNums+=2000;
            }
            //业务代码
            // 1、建议不要用多线程 不然事务不好控制  还有就是 有些业务 需要统一处理 会造成脏读 幻读现象
            // 2、建议使用jdbc 拼接sql 进行底层操作sql  进而提升性能
            // 3、与数据库比较数据重复的业务代码   不要放到 工具里面   这部分在最外层比较即可  必要时刻 可以直接注释这部分代码 插入和更新直接用sql来写
            /*for (WzForeignCompanyVo item:curList ) {
               // System.out.println("当前内容:"+item.getCompanyName()+":"+item.getProvinceName());
            }*/
        }

        return batchDoNums;
    }

}
