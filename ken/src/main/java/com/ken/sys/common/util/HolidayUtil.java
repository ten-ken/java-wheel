package com.ken.sys.common.util;

import java.util.*;
import java.util.regex.Pattern;

/**
 * <ul>
 * <li>Title: HolidayUtil</li>
 * <li>Description: TODO </li>
 * <li>Copyright: Copyright (c) 2018</li>
 * </ul>
 *
 * @author swc
 * @version
 * @date 2020/3/14 15:00
 */
public class HolidayUtil {
    /**
     * 功能描述: type 1 最近一个月内的节假日   2 半年内的节假日 默认是一年的节假日数据
     * @param type
     * @return: java.util.List<com.kcppc.common.util.Holiday>
     * @author: swc
     * @date: 2020/3/14 15:03
    */
    public static List<Holiday> getHolidays(int type){
        List<Holiday> list =new ArrayList<Holiday>();
        Holiday holiday =null;
        Map<String,Holiday> map =new HashMap<String,Holiday>();

        int days = type==0?30:(type==2?183:365);

        try{
            String reg ="10(-)[0][1-8]";//国庆
            Pattern p = Pattern.compile(reg);
            Calendar calendar =Calendar.getInstance();
            calendar.set(calendar.get(Calendar.YEAR),0,1);

            //国内法定放假的节假日
            String[] strs =new String[]{"春节","清明节","端午节","中秋节"};
            List<String> stringList =Arrays.asList(strs);
            SimpleCalendar.Element element =null;

            for(int i=0;i<days;i++){
                calendar.add(Calendar.DATE,i==0?0:1);
                int ind = calendar.get(Calendar.DAY_OF_WEEK) - 1;//周几
                element = SimpleCalendar.getCalendarDetail(calendar.getTime());
                //匹配国内的节假日
                if(stringList.contains(element.lunarFestival)){
                    holiday=new Holiday();
                    holiday.setName(element.lunarFestival);
                    holiday.setStartDate(calendar.getTime());

                    if(holiday.getName().contains("春节")){
                        holiday.setDays(10);//春节默认按十天 放假
                    }
                    if(holiday.getName().contains("清明节")){
                        holiday.setDays(3);
                        setRegDate(calendar, holiday, ind);
                    }
                    if(holiday.getName().contains("端午节")){
                        holiday.setDays(3);
                        setRegDate(calendar, holiday, ind);
                    }
                    if(holiday.getName().contains("中秋节")){
                        //中秋和国庆在一起
                        if(p.matcher(DateUtil.dateToStr(calendar.getTime(),DateUtil.FORMAT_YYYY_MM_DD_HH_MM_SS)).find()){
                            //设置10-01
                            Calendar reset = Calendar.getInstance();
                            reset.set(calendar.get(Calendar.YEAR),9,1);
                            holiday.setDays(8);
                        }else{
                            setRegDate(calendar, holiday, ind);
                            holiday.setDays(3);
                        }
                    }
                    map.put(holiday.getName(),holiday);
                }

                if(element.solarFestival.contains("国庆")  ){
                    holiday=new Holiday();
                    holiday.setStartDate(calendar.getTime());
                    holiday.setDays(element.lunarFestival.contains("中秋")?8:7);
                    holiday.setName("国庆节");
                    map.put(holiday.getName(),holiday);
                }
                if(element.solarFestival.contains("劳动节")){
                    holiday=new Holiday();
                    holiday.setName("劳动节");
                    holiday.setDays(5);
                    setRegDate(calendar, holiday, ind);
                    map.put(holiday.getName(),holiday);
                }
                if(element.solarFestival.contains("元旦")){
                    holiday=new Holiday();
                    holiday.setDays(1);
                    holiday.setName("元旦");
                    setRegDate(calendar, holiday, ind);
                    map.put(holiday.getName(),holiday);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        Calendar cal=null;
        for(String key:map.keySet()){
            holiday =map.get(key);
            cal =Calendar.getInstance();
            cal.setTime(holiday.getStartDate());
            cal.add(Calendar.DATE,holiday.getDays()-1);
            holiday.setEndDate(cal.getTime());
            list.add(map.get(key));
        }
        //时间正向排序
        Collections.sort(list, new Comparator<Holiday>() {
            public int compare(Holiday o1, Holiday o2){
                return o1.getStartDate().compareTo(o2.getStartDate());
            }
        });

        for(Holiday h:list){
            System.out.println(h.getName()+" - "+DateUtil.dateToStr(h.getStartDate(),DateUtil.FORMAT_YYYY_MM_DD)+" - "+DateUtil.dateToStr(h.getEndDate(),DateUtil.FORMAT_YYYY_MM_DD)+" :"+h.getDays());
        }
        return list;
    }


    private static void setRegDate(Calendar calendar, Holiday holiday, int ind) {
        Calendar reset = Calendar.getInstance();
        if(ind==0){//刚好是周日  //家假日开始时间为周六计算
            reset.set(calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH),calendar.get(Calendar.DATE)-1);
        }
        else if(ind==1){//刚好是周一  //家假日开始时间为周六计算
            reset.set(calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH),calendar.get(Calendar.DATE)-2);
            holiday.setStartDate(reset.getTime());
        }else{
            holiday.setStartDate(calendar.getTime());
        }
    }
}
