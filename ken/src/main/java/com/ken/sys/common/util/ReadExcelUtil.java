package com.ken.sys.common.util;

import com.ken.sys.common.entity.Table;
import com.ken.sys.common.entity.TableInfo;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;


public class ReadExcelUtil {

    private static final String EXCEL_XLS = "xls";
    private static final String EXCEL_XLSX = "xlsx";

    /**
     * 判断Excel的版本,获取Workbook
     * @return
     * @throws IOException
     */
    public static Workbook getWorkbok(InputStream in, File file) throws IOException {
        Workbook wb = null;
        if(file.getName().endsWith(EXCEL_XLS)){  //Excel 2003
            wb = new HSSFWorkbook(in);
        }else if(file.getName().endsWith(EXCEL_XLSX)){  // Excel 2007/2010
            wb = new XSSFWorkbook(in);
        }
        return wb;
    }

    /**
     * 判断文件是否是excel
     * @throws Exception
     */
    public static void checkExcelVaild(File file) throws Exception {
        if(!file.exists()){
            throw new Exception("文件不存在");
        }
        if(!(file.isFile() && (file.getName().endsWith(EXCEL_XLS) || file.getName().endsWith(EXCEL_XLSX)))){
            throw new Exception("文件不是Excel");
        }
    }


    /**
     * 读取Excel测试，兼容 Excel 2003/2007/2010
     * @throws Exception
     */
    public static List<Table> getInfoByExclePdm(String filePath) throws Exception {
        List<Table> tableList =new ArrayList<Table>();

        File excelFile = new File(filePath); // 创建文件对象

        FileInputStream in = new FileInputStream(excelFile); // 文件流
        checkExcelVaild(excelFile);
        Workbook workbook = getWorkbok(in,excelFile);

        int sheetCount = workbook.getNumberOfSheets(); // Sheet的数量
        /**
         * 设置当前excel中sheet的下标：0开始
         */
        if(sheetCount!=2){
            return null;
        }
        List<String> tableNameList = new ArrayList<String>();
        Sheet sheet1 = workbook.getSheetAt(0);   // 遍历第一个Sheet
        int rowNum =sheet1.getLastRowNum();//获取最后一行

        for(int i=0;i<=rowNum;i++){
            Row row = sheet1.getRow(i);
            int cellNum = row.getLastCellNum();
            for(int k=row.getFirstCellNum();k<cellNum;k++){
                Cell cell = row.getCell(k);
                if(k==2 && i>1){
                    tableNameList.add(getCellValue(cell));
                }
            }
        }

        Sheet sheet2 = workbook.getSheetAt(1);   // 遍历第二个Sheet

        for(int i=0;i<=sheet2.getLastRowNum();i++){
            Row row = sheet2.getRow(i);
            int cellNum = row.getLastCellNum();
            Table table =null;

            for(int k=row.getFirstCellNum();k<cellNum;k++){
                Cell cell = row.getCell(k);
                if(tableNameList.contains(getCellValue(cell))){
                    table =new Table();
                    table.setName(getCellValue(cell));
                    table.setCnName(getCellValue(row.getCell(k+1)));
                    table.setFirstRow(i+2);
                    continue;
                }
            }
            if(table!=null){
                tableList.add(table);
            }
        }

        handerListLastRow(tableList,sheet2.getLastRowNum());

        for(int li=0;li<tableList.size();li++){
            Table cur =tableList.get(li);
            List<TableInfo> infolist =new ArrayList<TableInfo>();
            for(int tb=cur.getFirstRow();tb<=cur.getLastRow();tb++){
                Row row = sheet2.getRow(tb);
                int cel = row.getLastCellNum();
                TableInfo info=new TableInfo();

                for(int k=row.getFirstCellNum();k<cel;k++){
                    if(k==1){
                        info.setColumn_name(getCellValue(row.getCell(k)));
                    }
                    if(k==2){
                        info.setData_type(getCellValue(row.getCell(k)));
                        info.setColumn_type(getCellValue(row.getCell(k)));
                    }
                    if(k==3){
                        info.setColumn_comment(getCellValue(row.getCell(k)));
                    }
                    if(k==4){//是否主键
                        ;
                        info.setIsPrimary("Y".equalsIgnoreCase(getCellValue(row.getCell(k)))?true:false);
                    }
                    if(k==5){//是否非空
                        info.setIsNotEmpty("Y".equalsIgnoreCase(getCellValue(row.getCell(k)))?true:false);
                    }
                    if(k==7){
                        try {
                            info.setLength(Long.parseLong(getCellValue(row.getCell(k))));
                        }catch (Exception e){
                            info.setLength(0L);
                        }
                    }
                }
                infolist.add(info);
            }
            cur.setList(infolist);
        }


        return tableList;
    }

    public static void handerListLastRow(List<Table> tableList, int lastRowNum) {
        for(int i=0;i<tableList.size();i++){
            if(tableList.size()-1==i){//最后一个
                tableList.get(i).setLastRow(lastRowNum);
            }else{
                tableList.get(i).setLastRow(tableList.get(i+1).getFirstRow()-5);
            }
        }
    }


    public static String getCellValue(Cell cell){
        String cellValue = "";
        if(cell == null){
            return cellValue;
        }
        //把数字当成String来读，避免出现1读成1.0的情况
        if(cell.getCellType() == Cell.CELL_TYPE_NUMERIC){
            cell.setCellType(Cell.CELL_TYPE_STRING);
        }
        //判断数据的类型
        switch (cell.getCellType()){
            case Cell.CELL_TYPE_NUMERIC: //数字
                cellValue = String.valueOf(cell.getNumericCellValue());
                break;
            case Cell.CELL_TYPE_STRING: //字符串
                cellValue = String.valueOf(cell.getStringCellValue());
                break;
            case Cell.CELL_TYPE_BOOLEAN: //Boolean
                cellValue = String.valueOf(cell.getBooleanCellValue());
                break;
            case Cell.CELL_TYPE_FORMULA: //公式
                cellValue = String.valueOf(cell.getCellFormula());
                break;
            case Cell.CELL_TYPE_BLANK: //空值
                cellValue = "";
                break;
            case Cell.CELL_TYPE_ERROR: //故障
                cellValue = "非法字符";
                break;
            default:
                cellValue = "未知类型";
                break;
        }
        return cellValue;
    }


}
