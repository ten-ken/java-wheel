package com.ken.sys.common.ifs;

import java.util.*;

/**
 * <ul>
 * <li>Title: 一般用于service的回调</li>
 * <li>Description: TODO </li>
 * </ul>
 *
 * @author swc
 * @date 2021/4/12 0012 下午 15:15
 */
public interface IExcelListHander {




    /**
     * 功能描述: 一般用于分批次插入数据库
     * @param list
     * @param sheetIndex
     * @param batchDoNums  记录处理的总数
     * @return: void
     * @author: swc
     * @date: 2021/4/12 0012 下午 15:31
    */
    <T> long  execute(List<T> list, int sheetIndex, long batchDoNums);


}
