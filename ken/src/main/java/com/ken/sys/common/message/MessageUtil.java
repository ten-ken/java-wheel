
package com.ken.sys.common.message;

import com.ken.sys.common.message.impl.BoolMessage;

/**
 * <ul>
 * <li>Title:     -MessageUtil</li>
 * <li>Description: TODO </li>
 * <li>Copyright: Copyright (c) 2018</li>
 * <li>微信:tianji_vip</li>
 * </ul>
 *
 * @author swc
 * @version
 * @date 2019/9/2 0002 下午 13:06
 */
public class MessageUtil {
    public MessageUtil() {
    }

    public static Message createMessage(boolean isSuccess, String message) {
        return new BoolMessage(isSuccess, message);
    }

    public static BoolMessage createMessageByRecord(boolean isSuccess, String message) {
        return new BoolMessage(isSuccess, message);
    }
}
