
package com.ken.sys.common.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.*;
import java.net.HttpURLConnection;

/**
 * <ul>
 * <li>Title: http系统间协议下文件传输</li>
 * <li>Description: TODO </li>
 * <li>Copyright: Copyright (c) 2018</li>
 * <li>微信:tianji_vip</li>
 * </ul>
 * @author swc
 * @version
 * @date 2019/10/31 0031 下午 17:40
 */
public class FileTransferUtil {

    private static Logger logger = LoggerFactory.getLogger(FileTransferUtil.class);
    /**
     * 调用流程上传文件接口上传文件(有用)
     * @param url
     * @param path
     * @return
     */
    public static String sendPostUplodFile(String url, String path) {
        DataOutputStream out = null;
        BufferedReader in = null;
        String result = "";
        try {
            result =URLUtil.connectByStream(url,path,out,in);
            logger.debug(result);
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }finally {
            try {
                if (out != null) {
                    out.close();
                }
                if (in != null) {
                    in.close();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return result;
    }

    /**
     * 功能描述: get 请求 进行访问 文件 进行跨域下载
     * @param url
     * @param query
     * @return: java.lang.String
     * @author: swc
     * @date: 2019/11/1 0001 下午 14:24
     */
    public static void  getDownLoad(String url, String query, String sendType,String localPath) throws Exception {
        sendType = EmptyUtils.isNullOrEmpty(sendType)?"POST":sendType.toUpperCase();
        HttpURLConnection conn = URLUtil.commonConnect(url,sendType,query,null);
        BufferedReader bReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        BufferedInputStream bis = null;
        BufferedOutputStream bos = null;
        try {
//            response.setContentType("text/html;charset=UTF-8");
            bis = new BufferedInputStream(conn.getInputStream());
//            bos = new BufferedOutputStream(response.getOutputStream());
            File file =new File(localPath);
            if(!file.getParentFile().exists()){
                file.mkdirs();
            }
            bos = new BufferedOutputStream(new FileOutputStream(file));
            byte[] buff = new byte[2048];
            int bytesRead;
            while (-1 != (bytesRead = bis.read(buff, 0, buff.length))) {
                bos.write(buff, 0, bytesRead);
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        } finally {
            if (bis != null) {
                bis.close();
            }
            if (bos != null) {
                bos.close();
            }

        }
        bReader.close();
    }

}
