package com.ken.sys.common.entity.echarts.legend;

import java.util.List;

/**
 * <ul>
 * <li>Title EchartsLegend</li>
 * <li>Description = TODO </li>
 * </ul>
 *
 * @author swc
 * @date 2020/7/3 0003 下午 14 =06
 */
public class EchartsLegend {
    private String type ;

    private String id ;

    private boolean show = true;

    private Integer zlevel = 0;

    private Integer z = 2;

    //string | number
    private Object left = "auto";

    //string | number
    private Object top = "auto";


    //string | number
    private Object right = "auto";

    //string | number
    private Object  bottom = "auto";

    //string | number
    private Object  width = "auto";

    //string | number
    private Object  height = "auto";

    private String orient = "horizontal";

    private String align = "auto";

    //array | number
    private Object  padding = 5;

    private Integer itemGap = 10;

    private Integer itemWidth = 25;

    private Integer  itemHeight = 14;

    private boolean symbolKeepAspect = true;

    //string | Function
    private Object formatter;

    private boolean selectedMode = true;

    //Color
    private Object  inactiveColor = "#ccc";

    //Object
    private Object  selected ;

    //图例的公用文本样式。 Object
    private Object   textStyle;

    private Object  tooltip ;

    private String icon ;

    //Array
    private List data;

    //Color
    private Object  backgroundColor = "transparent";

    //Color
    private Object borderColor = "#ccc";

    private Integer borderWidth = 1;

    //numberArray
    private Object borderRadius = 0;

    private Integer shadowBlur ;

    private Object shadowColor ;

    private Integer shadowOffsetX = 0;

    private Integer shadowOffsetY = 0;

    private Integer scrollDataIndex = 0;

    private Integer pageButtonItemGap = 5;

    private Integer pageButtonGap ;

    //string
    private String pageButtonPosition = "end";

    //string | Function
    private Object  pageFormatter = "{current}/{total}";

    private Object   pageIcons;

    private String pageIconColor = "#2f4554";

    private String pageIconInactiveColor = "#aaa";

    //number | Array
    private Object   pageIconSize = 15;

    private Object   pageTextStyle;

    private boolean animation ;

    private Integer animationDurationUpdate = 800;

    private Object emphasis;

    //boolean | Array
    private Object selector = false;

    private Object selectorLabel;

    private String selectorPosition = "auto";

    private Integer  selectorItemGap = 7;

    private Integer  selectorButtonGap = 10;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isShow() {
        return show;
    }

    public void setShow(boolean show) {
        this.show = show;
    }

    public Integer getZlevel() {
        return zlevel;
    }

    public void setZlevel(Integer zlevel) {
        this.zlevel = zlevel;
    }

    public Integer getZ() {
        return z;
    }

    public void setZ(Integer z) {
        this.z = z;
    }

    public Object getLeft() {
        return left;
    }

    public void setLeft(Object left) {
        this.left = left;
    }

    public Object getTop() {
        return top;
    }

    public void setTop(Object top) {
        this.top = top;
    }

    public Object getRight() {
        return right;
    }

    public void setRight(Object right) {
        this.right = right;
    }

    public Object getBottom() {
        return bottom;
    }

    public void setBottom(Object bottom) {
        this.bottom = bottom;
    }

    public Object getWidth() {
        return width;
    }

    public void setWidth(Object width) {
        this.width = width;
    }

    public Object getHeight() {
        return height;
    }

    public void setHeight(Object height) {
        this.height = height;
    }

    public String getOrient() {
        return orient;
    }

    public void setOrient(String orient) {
        this.orient = orient;
    }

    public String getAlign() {
        return align;
    }

    public void setAlign(String align) {
        this.align = align;
    }

    public Object getPadding() {
        return padding;
    }

    public void setPadding(Object padding) {
        this.padding = padding;
    }

    public Integer getItemGap() {
        return itemGap;
    }

    public void setItemGap(Integer itemGap) {
        this.itemGap = itemGap;
    }

    public Integer getItemWidth() {
        return itemWidth;
    }

    public void setItemWidth(Integer itemWidth) {
        this.itemWidth = itemWidth;
    }

    public Integer getItemHeight() {
        return itemHeight;
    }

    public void setItemHeight(Integer itemHeight) {
        this.itemHeight = itemHeight;
    }

    public boolean isSymbolKeepAspect() {
        return symbolKeepAspect;
    }

    public void setSymbolKeepAspect(boolean symbolKeepAspect) {
        this.symbolKeepAspect = symbolKeepAspect;
    }

    public Object getFormatter() {
        return formatter;
    }

    public void setFormatter(Object formatter) {
        this.formatter = formatter;
    }

    public boolean isSelectedMode() {
        return selectedMode;
    }

    public void setSelectedMode(boolean selectedMode) {
        this.selectedMode = selectedMode;
    }

    public Object getInactiveColor() {
        return inactiveColor;
    }

    public void setInactiveColor(Object inactiveColor) {
        this.inactiveColor = inactiveColor;
    }

    public Object getSelected() {
        return selected;
    }

    public void setSelected(Object selected) {
        this.selected = selected;
    }

    public Object getTextStyle() {
        return textStyle;
    }

    public void setTextStyle(Object textStyle) {
        this.textStyle = textStyle;
    }

    public Object getTooltip() {
        return tooltip;
    }

    public void setTooltip(Object tooltip) {
        this.tooltip = tooltip;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public List getData() {
        return data;
    }

    public void setData(List data) {
        this.data = data;
    }

    public Object getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(Object backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public Object getBorderColor() {
        return borderColor;
    }

    public void setBorderColor(Object borderColor) {
        this.borderColor = borderColor;
    }

    public Integer getBorderWidth() {
        return borderWidth;
    }

    public void setBorderWidth(Integer borderWidth) {
        this.borderWidth = borderWidth;
    }

    public Object getBorderRadius() {
        return borderRadius;
    }

    public void setBorderRadius(Object borderRadius) {
        this.borderRadius = borderRadius;
    }

    public Integer getShadowBlur() {
        return shadowBlur;
    }

    public void setShadowBlur(Integer shadowBlur) {
        this.shadowBlur = shadowBlur;
    }

    public Object getShadowColor() {
        return shadowColor;
    }

    public void setShadowColor(Object shadowColor) {
        this.shadowColor = shadowColor;
    }

    public Integer getShadowOffsetX() {
        return shadowOffsetX;
    }

    public void setShadowOffsetX(Integer shadowOffsetX) {
        this.shadowOffsetX = shadowOffsetX;
    }

    public Integer getShadowOffsetY() {
        return shadowOffsetY;
    }

    public void setShadowOffsetY(Integer shadowOffsetY) {
        this.shadowOffsetY = shadowOffsetY;
    }

    public Integer getScrollDataIndex() {
        return scrollDataIndex;
    }

    public void setScrollDataIndex(Integer scrollDataIndex) {
        this.scrollDataIndex = scrollDataIndex;
    }

    public Integer getPageButtonItemGap() {
        return pageButtonItemGap;
    }

    public void setPageButtonItemGap(Integer pageButtonItemGap) {
        this.pageButtonItemGap = pageButtonItemGap;
    }

    public Integer getPageButtonGap() {
        return pageButtonGap;
    }

    public void setPageButtonGap(Integer pageButtonGap) {
        this.pageButtonGap = pageButtonGap;
    }

    public String getPageButtonPosition() {
        return pageButtonPosition;
    }

    public void setPageButtonPosition(String pageButtonPosition) {
        this.pageButtonPosition = pageButtonPosition;
    }

    public Object getPageFormatter() {
        return pageFormatter;
    }

    public void setPageFormatter(Object pageFormatter) {
        this.pageFormatter = pageFormatter;
    }

    public Object getPageIcons() {
        return pageIcons;
    }

    public void setPageIcons(Object pageIcons) {
        this.pageIcons = pageIcons;
    }

    public String getPageIconColor() {
        return pageIconColor;
    }

    public void setPageIconColor(String pageIconColor) {
        this.pageIconColor = pageIconColor;
    }

    public String getPageIconInactiveColor() {
        return pageIconInactiveColor;
    }

    public void setPageIconInactiveColor(String pageIconInactiveColor) {
        this.pageIconInactiveColor = pageIconInactiveColor;
    }

    public Object getPageIconSize() {
        return pageIconSize;
    }

    public void setPageIconSize(Object pageIconSize) {
        this.pageIconSize = pageIconSize;
    }

    public Object getPageTextStyle() {
        return pageTextStyle;
    }

    public void setPageTextStyle(Object pageTextStyle) {
        this.pageTextStyle = pageTextStyle;
    }

    public boolean isAnimation() {
        return animation;
    }

    public void setAnimation(boolean animation) {
        this.animation = animation;
    }

    public Integer getAnimationDurationUpdate() {
        return animationDurationUpdate;
    }

    public void setAnimationDurationUpdate(Integer animationDurationUpdate) {
        this.animationDurationUpdate = animationDurationUpdate;
    }

    public Object getEmphasis() {
        return emphasis;
    }

    public void setEmphasis(Object emphasis) {
        this.emphasis = emphasis;
    }

    public Object getSelector() {
        return selector;
    }

    public void setSelector(Object selector) {
        this.selector = selector;
    }

    public Object getSelectorLabel() {
        return selectorLabel;
    }

    public void setSelectorLabel(Object selectorLabel) {
        this.selectorLabel = selectorLabel;
    }

    public String getSelectorPosition() {
        return selectorPosition;
    }

    public void setSelectorPosition(String selectorPosition) {
        this.selectorPosition = selectorPosition;
    }

    public Integer getSelectorItemGap() {
        return selectorItemGap;
    }

    public void setSelectorItemGap(Integer selectorItemGap) {
        this.selectorItemGap = selectorItemGap;
    }

    public Integer getSelectorButtonGap() {
        return selectorButtonGap;
    }

    public void setSelectorButtonGap(Integer selectorButtonGap) {
        this.selectorButtonGap = selectorButtonGap;
    }
}
