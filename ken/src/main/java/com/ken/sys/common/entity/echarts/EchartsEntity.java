
package com.ken.sys.common.entity.echarts;

import java.math.BigDecimal;

/**
 * <ul>
 * <li>Title:     -EchartsEntity</li>
 * <li>Description: TODO </li>
 * <li>Copyright: Copyright (c) 2018</li>
 * <li>微信:tianji_vip</li>
 * </ul>
 *
 * @author swc
 * @version
 * @date 2020/3/19 0019 上午 9:22
 */
public class EchartsEntity {

    private String name;

    private String level;

    private Integer nums1;

    private Integer nums2;

    private BigDecimal amount;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public Integer getNums1() {
        return nums1;
    }

    public void setNums1(Integer nums1) {
        this.nums1 = nums1;
    }

    public Integer getNums2() {
        return nums2;
    }

    public void setNums2(Integer nums2) {
        this.nums2 = nums2;
    }
}
