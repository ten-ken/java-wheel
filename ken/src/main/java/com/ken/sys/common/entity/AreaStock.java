package com.ken.sys.common.entity;

import java.util.List;

/**
 * <ul>
 * <li>title;//areastock</li>
 * <li>description;//todo </li>
 * <li>copyright;//copyright (c) 2018</li>
 * <li>company;//http://www.jiangqiaotech.com/</li>
 * </ul>
 *
 * @author swc
 *
 * @date 2020/7/1 0001 上午 10:57
 */
public class AreaStock {

    private String area_code;//"c01"

    private String  area_length;//null

    private String  area_name;//"普通货架区_搁板式货架区1"

    private String  area_type;//"c"

    private String  area_width;//null

    private String  coordinate_x;//null

    private String  coordinate_y;//null

    private String  id;//"8a0af66a6ac47e4f016ad94c37700e81"

    private Integer max_col;//4

    private Integer max_layer;//4

    private Integer  max_row;//6

    private String  pick_location;//null

    private String stock_in_location;//null

    private String stock_out_location;//null

    private List<Storage> storages;// [{id;//"8a0af66a6ac47e4f016ad95ae7750f65", code;//"c01-010101",…},…]

    private String warehouse_id;//"8a0a854b63d2b8bf0163d2c89d2b0009"


    public String getArea_code() {
        return area_code;
    }

    public void setArea_code(String area_code) {
        this.area_code = area_code;
    }

    public String getArea_length() {
        return area_length;
    }

    public void setArea_length(String area_length) {
        this.area_length = area_length;
    }

    public String getArea_name() {
        return area_name;
    }

    public void setArea_name(String area_name) {
        this.area_name = area_name;
    }

    public String getArea_type() {
        return area_type;
    }

    public void setArea_type(String area_type) {
        this.area_type = area_type;
    }

    public String getArea_width() {
        return area_width;
    }

    public void setArea_width(String area_width) {
        this.area_width = area_width;
    }

    public String getCoordinate_x() {
        return coordinate_x;
    }

    public void setCoordinate_x(String coordinate_x) {
        this.coordinate_x = coordinate_x;
    }

    public String getCoordinate_y() {
        return coordinate_y;
    }

    public void setCoordinate_y(String coordinate_y) {
        this.coordinate_y = coordinate_y;
    }

    public Integer getMax_col() {
        return max_col;
    }

    public void setMax_col(Integer max_col) {
        this.max_col = max_col;
    }

    public Integer getMax_layer() {
        return max_layer;
    }

    public void setMax_layer(Integer max_layer) {
        this.max_layer = max_layer;
    }

    public Integer getMax_row() {
        return max_row;
    }

    public void setMax_row(Integer max_row) {
        this.max_row = max_row;
    }

    public String getPick_location() {
        return pick_location;
    }

    public void setPick_location(String pick_location) {
        this.pick_location = pick_location;
    }

    public String getStock_in_location() {
        return stock_in_location;
    }

    public void setStock_in_location(String stock_in_location) {
        this.stock_in_location = stock_in_location;
    }

    public String getStock_out_location() {
        return stock_out_location;
    }

    public void setStock_out_location(String stock_out_location) {
        this.stock_out_location = stock_out_location;
    }

    public List<Storage> getStorages() {
        return storages;
    }

    public void setStorages(List<Storage> storages) {
        this.storages = storages;
    }

    public String getWarehouse_id() {
        return warehouse_id;
    }

    public void setWarehouse_id(String warehouse_id) {
        this.warehouse_id = warehouse_id;
    }
}
