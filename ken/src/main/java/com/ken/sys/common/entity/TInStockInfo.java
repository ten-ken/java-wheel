package com.ken.sys.common.entity;

import com.ken.sys.common.anno.JsonAlias;
import com.ken.sys.common.anno.KTableField;
import com.ken.sys.common.anno.KTableName;

import java.math.BigDecimal;
import java.util.Date;

//用于生成数据库表 和根据数据源插入和更新表信息
@KTableName(value = "t_stock_info",comment = "入库信息")
public class TInStockInfo {

//    @JsonAlias("id")
    @KTableField(isPrimary = true,isNullAble = false,autoIncrement = true,comment = "主键")
    private Long id;

    @JsonAlias("curdate")
    @KTableField(comment = "使用时间",alias = "user_date",jdbcType = "datetime")
    private Date useDate;

    @JsonAlias("nums")
    @KTableField(comment = "入库数量",alias = "in_stock_nums")
    private BigDecimal inStockNums;

    @JsonAlias("remark")
    @KTableField(comment = "备注",alias = "remark",length = 255)
    private String remark;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getUseDate() {
        return useDate;
    }

    public void setUseDate(Date useDate) {
        this.useDate = useDate;
    }

    public BigDecimal getInStockNums() {
        return inStockNums;
    }

    public void setInStockNums(BigDecimal inStockNums) {
        this.inStockNums = inStockNums;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}