package com.ken.sys.common.entity.echarts.series;

/**
 *
 * @author swc
 * @version
 * @date 2020/3/20 22:18
 */
public class FunnelSeries extends EchartBaseSeries{

 private String type ="funnel";

// private String id;

 private Number min = 0;

 //string or number
 private Object left = 80;

 //string or number
 private Object right = 80;

 //string or number
 private Object top = 60;

 //string or number
 private Object bottom = 60;

 //string or number
 private Object width = "auto";

 //string or number
 private Object height = "auto";

 private Number max = 100;

 private String  minSize = "0%";

 private String maxSize = "100%" ;

 private String  sort ="descending";

 private Number  gap = 0 ;

 private boolean  legendHoverLink = true;

 private String  funnelAlign = "center";

 public Number getMin() {
  return min;
 }

 public void setMin(Number min) {
  this.min = min;
 }

 public Number getMax() {
  return max;
 }

 public void setMax(Number max) {
  this.max = max;
 }

 public String getMinSize() {
  return minSize;
 }

 public void setMinSize(String minSize) {
  this.minSize = minSize;
 }

 public String getMaxSize() {
  return maxSize;
 }

 public void setMaxSize(String maxSize) {
  this.maxSize = maxSize;
 }

 public String getSort() {
  return sort;
 }

 public void setSort(String sort) {
  this.sort = sort;
 }

 public Number getGap() {
  return gap;
 }

 public void setGap(Number gap) {
  this.gap = gap;
 }

 public boolean isLegendHoverLink() {
  return legendHoverLink;
 }

 public void setLegendHoverLink(boolean legendHoverLink) {
  this.legendHoverLink = legendHoverLink;
 }

 public String getFunnelAlign() {
  return funnelAlign;
 }

 public void setFunnelAlign(String funnelAlign) {
  this.funnelAlign = funnelAlign;
 }


 public Object getLeft() {
  return left;
 }

 public void setLeft(Object left) {
  this.left = left;
 }

 public Object getTop() {
  return top;
 }

 public void setTop(Object top) {
  this.top = top;
 }

 public Object getBottom() {
  return bottom;
 }

 public void setBottom(Object bottom) {
  this.bottom = bottom;
 }

 public Object getWidth() {
  return width;
 }

 public void setWidth(Object width) {
  this.width = width;
 }

 public Object getRight() {
  return right;
 }

 public void setRight(Object right) {
  this.right = right;
 }

 public Object getHeight() {
  return height;
 }

 public void setHeight(Object height) {
  this.height = height;
 }

 @Override
 public String getType() {
  return type;
 }

 @Override
 public void setType(String type) {
  this.type = type;
 }
}
