package com.ken.sys.common.anno;

import java.lang.annotation.*;

@Documented
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface KTableName {
    /**
     * 表名
     * @return
     */
    String value() default  "";

    /**
     * 是否覆盖本表
     * @return
     */
    boolean isCover() default true;

    /**
     * 表的注释
     * @return
     */
    String comment() default "";

}
