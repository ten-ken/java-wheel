package com.ken.sys.common.util;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.security.Key;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * <ul>
 * <li>Title: HtmlToMarkdownUtil</li>
 * <li>Description: html转markdown语法 </li>
 * </ul>
 *
 * @author swc
 * @date 2020/7/19 12:08
 */
public class HtmlToMarkdownUtil {


    /**
     * 功能描述: 将当前元素内容 转成markdown 语法的格式 存在问题 不能正常解析xml  这点木有办法 因为xml的标签可能和自定义的dom标签是一样的
     * @param element 存在xml文本还原低于75% 不存在xml文本还原度95%
     * @return: java.lang.String
     * @author: swc
     * @date: 2020/7/19 12:24
    */
    public static String toMarkdown(Element element,Map<String,String> htmlMappingMarkDown) {
        String htmlContent = element.outerHtml();
        Elements tables = element.getElementsByTag("table");
        Elements thEles =null;
        Elements tbodyEles =null;
        Elements trEles =null;

        Elements tdEles =null;

        StringBuffer firstHead =null;
        StringBuffer head =null;
        //表格
        StringBuffer tableSbf =null;

//        String tdPattern = "[<table]{6}[\\s|\\S]{100,800}[^<table>]{7}[\\s|\\S]{100,800}[><\\/table>]{9}";
        String tablePattern = "<table[\\s|\\S]{100,800}[^<table>]{7}[\\s|\\S]{100,800}<\\/table>";
//        String aPattern = "[<a]{2}[\\s|\\S]{10,350}(>)[^<a>]{3}[\\s|\\S]{0,50}[><\\/a>]{5}";
        String aPattern = "<a[^<]{1,350}>[^<>]{1,50}</a>";
//        String imgPattern = "[<img]{4}[\\s|\\S]{10,350}[/>]{1,2}";
        String imgPattern = "<img[^<]{1,350}(/)?>";

        //匹配html的标签 进行去除==>
//     (beans|xmlns|spring)
        String tagReg ="<[^<>]{1,150}>";


        for (int i = 0; i < tables.size(); i++) {
            tableSbf = new StringBuffer();

            thEles = tables.get(i).getElementsByTag("thead");
            firstHead =new StringBuffer();
            head =new StringBuffer();

            firstHead.append("|");
            firstHead.append(thEles.text().replaceAll("[\\s]{1,2}","|"));
            firstHead.append("|\n");
            tableSbf.append(firstHead);

            head.append(firstHead.toString().replaceAll("[^\\|^\\n]","---"));
            tableSbf.append(head);

            StringBuffer content =null;
            tbodyEles = tables.get(i).getElementsByTag("tbody");

            trEles =tbodyEles.get(0).getElementsByTag("tr");

            for (int q = 0;q < trEles.size(); q++) {
                content =new StringBuffer();
                content.append("|");
                tdEles = trEles.get(q).getElementsByTag("td");

                for (int ls = 0;ls < tdEles.size(); ls++) {
                    content.append(tdEles.get(ls).text());
                    content.append("|");
                }
                content.append("\n");
                tableSbf.append(content);
            }

            htmlContent = htmlContent.replaceFirst(tablePattern,tableSbf.toString());
        }

        //a标签
        Elements hrefs = element.getElementsByTag("a");
        for (int i = 0; i < hrefs.size(); i++) {
            htmlContent = htmlContent.replaceFirst(aPattern,"[{title}]({href})".replace("{href}",hrefs.get(i).attr("href"))
                    .replace("{title}",hrefs.get(i).text()));

        }
        //图片标签
        Elements srcs = element.getElementsByTag("img");
        for (int i = 0; i < srcs.size(); i++) {
            htmlContent = htmlContent.replaceFirst(imgPattern,"![{title}]({src})".replace("{src}",srcs.get(i).attr("src"))
                    .replace("{title}",""));

        }

        if(EmptyUtils.isNullOrEmpty(htmlMappingMarkDown)){
            htmlMappingMarkDown = htmlMappingMarkDown();
        }

//      Map<String, String> stringMap = htmlMappingMarkDown();
        Set<String> strings = htmlMappingMarkDown.keySet();
        for (String key:strings) {
            htmlContent = htmlContent.replace(key,htmlMappingMarkDown.get(key));
        }

        htmlContent = htmlContent.replaceAll(tagReg,"");

        return htmlContent;
    }



    /**
     * 功能描述: 各个标签对应关系
     * @param
     * @return: java.util.Map<java.lang.String,java.lang.String>
     * @author: swc
     * @date: 2020/7/22 0022 上午 9:16
    */
    public static Map<String,String> htmlMappingMarkDown(){
        Map<String,String> map =new HashMap<String,String>();
        //h系列
        map.put("<h1>","## ");
        map.put("<h2>","## ");
        map.put("<h3>","### ");
        map.put("<h4>","#### ");
        map.put("<h5>","##### ");

        //代码块
        map.put("<code>","```\n");
        map.put("</code>","```");

        map.put("<strong>","** ");
        map.put("</strong>","** ");

        map.put("&nbsp;"," ");

        map.put("<i>","_");
        map.put("</i>","_");

        //转义字符
        map.put("&lt;","<");
        map.put("&gt;",">");

        return map;
    }
}
