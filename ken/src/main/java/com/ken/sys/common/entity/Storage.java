package com.ken.sys.common.entity;

/**
 * <ul>
 * <li>Title;// Storage</li>
 * <li>Description;// TODO </li>
 * <li>Copyright;// Copyright (c) 2018</li>
 * <li>Company;// http;////www.jiangqiaotech.com/</li>
 * </ul>
 *
 * @author swc
 * @date 2020/7/1 0001 上午 10;//58
 */
public class Storage {
    private String area_type_id;// "8a0af66a6ac47e4f016ad950da9c0e88"

    private Integer capaicty_unit;// 1

    private String  code;// "g01-010101"

    private String col;// "01"

    private String container_id;// null

    private String cur_capacity;// 0

    private String deep_pos_type;// null

    private Integer height;// 999


    private String id;// "8a0af66a6ac47e4f016ad95e27f8109e"

    private Integer is_froze;// 0

    private String layer;// "01"

    private Integer length;// 999

    private String opposite_position;// null

    private String  row;// "01"

    private String state;// "1"

    private Integer stock_area;// 1

    private Integer total_capaicty;// 999

    private String warehouse_id;// "8a0a854b63d2b8bf0163d2c89d2b0009"

    private Integer width;// 999

    public String getArea_type_id() {
        return area_type_id;
    }

    public void setArea_type_id(String area_type_id) {
        this.area_type_id = area_type_id;
    }

    public Integer getCapaicty_unit() {
        return capaicty_unit;
    }

    public void setCapaicty_unit(Integer capaicty_unit) {
        this.capaicty_unit = capaicty_unit;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCol() {
        return col;
    }

    public void setCol(String col) {
        this.col = col;
    }

    public String getContainer_id() {
        return container_id;
    }

    public void setContainer_id(String container_id) {
        this.container_id = container_id;
    }

    public String getCur_capacity() {
        return cur_capacity;
    }

    public void setCur_capacity(String cur_capacity) {
        this.cur_capacity = cur_capacity;
    }

    public String getDeep_pos_type() {
        return deep_pos_type;
    }

    public void setDeep_pos_type(String deep_pos_type) {
        this.deep_pos_type = deep_pos_type;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getIs_froze() {
        return is_froze;
    }

    public void setIs_froze(Integer is_froze) {
        this.is_froze = is_froze;
    }

    public String getLayer() {
        return layer;
    }

    public void setLayer(String layer) {
        this.layer = layer;
    }

    public Integer getLength() {
        return length;
    }

    public void setLength(Integer length) {
        this.length = length;
    }

    public String getOpposite_position() {
        return opposite_position;
    }

    public void setOpposite_position(String opposite_position) {
        this.opposite_position = opposite_position;
    }

    public String getRow() {
        return row;
    }

    public void setRow(String row) {
        this.row = row;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Integer getStock_area() {
        return stock_area;
    }

    public void setStock_area(Integer stock_area) {
        this.stock_area = stock_area;
    }

    public Integer getTotal_capaicty() {
        return total_capaicty;
    }

    public void setTotal_capaicty(Integer total_capaicty) {
        this.total_capaicty = total_capaicty;
    }

    public String getWarehouse_id() {
        return warehouse_id;
    }

    public void setWarehouse_id(String warehouse_id) {
        this.warehouse_id = warehouse_id;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }
}
