package com.ken.sys.common.entity;

import java.math.BigDecimal;

//k线图数据
public class KData {

    private String curDate;

    //开盘(open)，
    private BigDecimal openValue;

    //收盘(close)，
    private BigDecimal closeValue;

    //最低(lowest)
    private BigDecimal lowestValue;

    //最高(highest)
    private BigDecimal highestValue;

    public KData(String curDate, BigDecimal openValue, BigDecimal closeValue, BigDecimal lowestValue, BigDecimal highestValue) {
        this.curDate = curDate;
        this.openValue = openValue;
        this.closeValue = closeValue;
        this.lowestValue = lowestValue;
        this.highestValue = highestValue;
    }

    public String getCurDate() {
        return curDate;
    }

    public void setCurDate(String curDate) {
        this.curDate = curDate;
    }

    public BigDecimal getOpenValue() {
        return openValue;
    }

    public void setOpenValue(BigDecimal openValue) {
        this.openValue = openValue;
    }

    public BigDecimal getCloseValue() {
        return closeValue;
    }

    public void setCloseValue(BigDecimal closeValue) {
        this.closeValue = closeValue;
    }

    public BigDecimal getLowestValue() {
        return lowestValue;
    }

    public void setLowestValue(BigDecimal lowestValue) {
        this.lowestValue = lowestValue;
    }

    public BigDecimal getHighestValue() {
        return highestValue;
    }

    public void setHighestValue(BigDecimal highestValue) {
        this.highestValue = highestValue;
    }
}
