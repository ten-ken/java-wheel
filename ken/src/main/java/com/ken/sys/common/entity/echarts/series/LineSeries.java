
package com.ken.sys.common.entity.echarts.series;

import com.ken.sys.common.entity.echarts.series.property.SeriesAreaStyle;
import com.ken.sys.common.entity.echarts.series.property.SeriesLineStyle;

/**
 * <ul>
 * <li>Title:     -LineSeries</li>
 * <li>Description: TODO </li>
 * <li>Copyright: Copyright (c) 2018</li>
 * <li>微信:tianji_vip</li>
 * </ul>
 *
 * @author swc
 * @version
 * @date 2020/3/19 0019 上午 9:33
 */
public class LineSeries extends EchartBaseSeries{

    private String type="line";

    private SeriesLineStyle lineStyle;

    //设置区域【一般针对的是折线图】---针对所有折线区域
    private SeriesAreaStyle areaStyle;

    public SeriesLineStyle getLineStyle() {
        return lineStyle;
    }

    public void setLineStyle(SeriesLineStyle lineStyle) {
        this.lineStyle = lineStyle;
    }

    public SeriesAreaStyle getAreaStyle() {
        return areaStyle;
    }

    public void setAreaStyle(SeriesAreaStyle areaStyle) {
        this.areaStyle = areaStyle;
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public void setType(String type) {
        this.type = type;
    }
}
