/******************************************************************************
 *
 * 作者（author）：ken
 * 微信（weChat）：mlchao1992
 * 个人博客（website）：
 *
 ******************************************************************************
 * 注意：尊重原创
 *****************************************************************************/
package com.ken.sys.common.entity;

import com.ken.sys.common.anno.KTableField;
import com.ken.sys.common.anno.KTableName;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * <ul>
 * <li>Title: ken-Area</li>
 * <li>Description: TODO </li>
 * <li>Copyright: Copyright (c) 2018</li>
 * </ul>
 *
 * @author ken
 * @version V1.0
 * @date 2019/9/21 13:50
 */
@KTableName("sys_area")
public class Area implements Serializable {
    @KTableField(isPrimary = true)
    private Long id;

//    @KTableField(alias = "pid")
    private String pid;

    @KTableField(alias = "area_name")
    private String name;

    @KTableField(alias = "area_code")
    private String code;

    @KTableField(exist = false)
    private String active;

    @KTableField(exist = false)
    private List<Area> childList;

//    @KTableField(alias = "nums")
    private BigDecimal nums;

    //时间
    @KTableField(alias = "create_date")
    private Date doDate;


    public Area(  String code,String pid,String name) {
        this.pid = pid;
        this.name = name;
        this.code = code;
    }

    public Area() {
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public List<Area> getChildList() {
        return childList;
    }

    public void setChildList(List<Area> childList) {
        this.childList = childList;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public BigDecimal getNums() {
        return nums;
    }

    public void setNums(BigDecimal nums) {
        this.nums = nums;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDoDate() {
        return doDate;
    }

    public void setDoDate(Date doDate) {
        this.doDate = doDate;
    }
}
