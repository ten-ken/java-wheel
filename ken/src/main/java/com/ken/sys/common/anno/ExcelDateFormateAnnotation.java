
package com.ken.sys.common.anno;

import java.lang.annotation.*;

/**
 * <ul>
 * <li>Title:     -ExportExcelAnnotation</li>
 * <li>Description: TODO </li>
 * <li>Copyright: Copyright (c) 2018</li>
 * <li>微信:tianji_vip</li>
 * </ul>
 *
 * @author swc
 * @version
 * @date 2019/9/3 0003 下午 15:44
 */
@Documented
@Target({ ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface ExcelDateFormateAnnotation {
    String fieldName() default ""; //字段名称
    String dateFormate() default ""; // 验证规则---时间格式可以写YYYYMMDD
}