package com.ken.sys.common.strategy.echarts;

import com.ken.sys.common.entity.echarts.axis.EchartsXYAxis;
import com.ken.sys.common.entity.echarts.dataset.EchartsDataset;
import com.ken.sys.common.entity.echarts.dto.HandEchartsDto;
import com.ken.sys.common.entity.echarts.legend.EchartsLegend;
import com.ken.sys.common.entity.echarts.option.EchartsOption;
import com.ken.sys.common.entity.echarts.series.CandlestickSeries;
import com.ken.sys.common.entity.echarts.series.EchartBaseSeries;
import com.ken.sys.common.entity.echarts.series.property.SeriesAreaStyle;
import com.ken.sys.common.entity.echarts.series.property.SeriesItemStyle;
import com.ken.sys.common.entity.echarts.series.property.SeriesLabel;
import com.ken.sys.common.entity.echarts.series.property.SeriesLineStyle;
import com.ken.sys.common.ifs.IEchartSeries;
import com.ken.sys.common.strategy.echarts.base.BaseStrategy;
import com.ken.sys.common.strategy.echarts.base.EchartsStrategy;
import com.ken.sys.common.util.EmptyUtils;
import com.ken.sys.common.util.ReflectUtils;

import java.util.*;

/**
 * <ul>
 * <li>Title: K线图策略-CandlestickStrategy</li>
 * <li>Description: TODO </li>
 * <li>Copyright: Copyright (c) 2018</li>
 * </ul>
 *
 * @author swc
 * @date 2020/7/6 0006 下午 15:12
 */
public class CandlestickStrategy extends BaseStrategy implements EchartsStrategy {

    @Override
    public <T> EchartsOption createOption(IEchartSeries iEchartSeries, List<T> list, HandEchartsDto handEchartsDto) {
        String xyAxisName = handEchartsDto.getxAxisDataName();
        Object[] legends = handEchartsDto.getLegends();
        Object[] fields =  handEchartsDto.getFields();

        EchartsOption option =new EchartsOption();
        List<Object> xAxisDataList =new ArrayList<Object>();

        //接收反射字段的值
        Object value =null;

        //整个数据值
        List seriesTotalValue =new ArrayList<>();

        List seriesDataValue =null;
        try {
            for(T t:list){

                if(!EmptyUtils.isNullOrEmpty(xyAxisName)){
                    //x轴信息
                    xAxisDataList.add(ReflectUtils.getFieldValue(t,xyAxisName));
                }

                seriesDataValue =new ArrayList<>();

                for(int ind=0;ind<fields.length;ind++){
                    value = ReflectUtils.getFieldValue(t,(String)fields[ind]);
                    seriesDataValue.add(value);
                }
                seriesTotalValue.add(seriesDataValue);
            }


            List<EchartBaseSeries> serieList =new ArrayList<EchartBaseSeries>();
            CandlestickSeries serie =null;

            for(int ind=0;ind<legends.length;ind++){
                serie =new CandlestickSeries();

                if(!EmptyUtils.isNullOrEmpty(legends)){
                    serie.setName(legends[ind].toString());
                }

                serie.setData(seriesTotalValue);
                serieList.add(serie);

                if(!EmptyUtils.isNullOrEmpty(iEchartSeries)){
                    iEchartSeries.setSeriesDetailProp(serie,new SeriesLabel(),new SeriesItemStyle(),new SeriesAreaStyle(),new SeriesLineStyle());
                }

            }

            if(!EmptyUtils.isNullOrEmpty(legends)){
                EchartsLegend legend = new EchartsLegend();
                legend.setData(Arrays.asList(legends));
                option.setLegend(legend);
            }

            EchartsXYAxis xAxis = new EchartsXYAxis();
            xAxis.setData(xAxisDataList);
            option.setxAxis(xAxis);

            //默认使用series ---data
            if(handEchartsDto.getCreateSeriesData()){
                option.setSeries(serieList);
            }

            //使用dataset---source
            if(handEchartsDto.getCreateDatasetSource()){
                EchartsDataset dataset = new EchartsDataset();
                dataset.setSource(seriesTotalValue);
                option.setDataset(dataset);
            }

        }catch (Exception e){
            //实际换成日志打印
            e.printStackTrace();
        }
        return option;
    }

    @Override
    public <T> List<EchartsOption> createMulOption(IEchartSeries iEchartSeries, List<T> list, HandEchartsDto handEchartsDto) {
        return null;
    }




}
