
package com.ken.sys.common.message;

import com.alibaba.fastjson.JSONObject;

/**
 * <ul>
 * <li>Title:     -Message</li>
 * <li>Description: TODO </li>
 * <li>Copyright: Copyright (c) 2018</li>
 * <li>微信:tianji_vip</li>
 * </ul>
 *
 * @author swc
 * @version
 * @date 2019/9/2 0002 下午 13:05
 */
public interface Message {
    JSONObject buildJSON();

    void putInJSON(JSONObject var1);
}
