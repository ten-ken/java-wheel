
package com.ken.sys.common.entity.echarts.series;

/**
 * <ul>
 * <li>Title:     -GaugeSeries</li>
 * <li>Description: TODO </li>
 * <li>Copyright: Copyright (c) 2018</li>
 * <li>微信:tianji_vip</li>
 * </ul>
 *
 * @author swc
 * @version
 * @date 2020/3/21 0:11
 */
public class GaugeSeries  extends EchartBaseSeries{

    private String type ="gauge";

    private String detail ="";

    //number string
    private Object radius = "75%";

    private Number startAngle = 225;

    private Number  endAngle = -45;

    private boolean clockwise = true;

    private Number splitNumber = 10;



    private boolean silent = false;

    private boolean  animation = true;

    private Number animationThreshold = 2000 ;
    private Number  animationDuration = 1000 ;

    private String animationEasing = "cubicOut" ;

    //number or Function 初始动画的延迟，支持回调函数，可以通过每个数据返回不同的 delay 时间实现更戏剧的初始动画效果
    private Object animationDelay = 0 ;

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public Object getRadius() {
        return radius;
    }

    public void setRadius(Object radius) {
        this.radius = radius;
    }

    public Number getStartAngle() {
        return startAngle;
    }

    public void setStartAngle(Number startAngle) {
        this.startAngle = startAngle;
    }

    public Number getEndAngle() {
        return endAngle;
    }

    public void setEndAngle(Number endAngle) {
        this.endAngle = endAngle;
    }

    public boolean isClockwise() {
        return clockwise;
    }

    public void setClockwise(boolean clockwise) {
        this.clockwise = clockwise;
    }

    public Number getSplitNumber() {
        return splitNumber;
    }

    public void setSplitNumber(Number splitNumber) {
        this.splitNumber = splitNumber;
    }

    public boolean isSilent() {
        return silent;
    }

    public void setSilent(boolean silent) {
        this.silent = silent;
    }

    public boolean isAnimation() {
        return animation;
    }

    public void setAnimation(boolean animation) {
        this.animation = animation;
    }

    public Number getAnimationThreshold() {
        return animationThreshold;
    }

    public void setAnimationThreshold(Number animationThreshold) {
        this.animationThreshold = animationThreshold;
    }

    public Number getAnimationDuration() {
        return animationDuration;
    }

    public void setAnimationDuration(Number animationDuration) {
        this.animationDuration = animationDuration;
    }

    public String getAnimationEasing() {
        return animationEasing;
    }

    public void setAnimationEasing(String animationEasing) {
        this.animationEasing = animationEasing;
    }

    public Object getAnimationDelay() {
        return animationDelay;
    }

    public void setAnimationDelay(Object animationDelay) {
        this.animationDelay = animationDelay;
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public void setType(String type) {
        this.type = type;
    }
}
