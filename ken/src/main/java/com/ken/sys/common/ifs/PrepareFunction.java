package com.ken.sys.common.ifs;

import java.sql.PreparedStatement;

/**
 * <ul>
 * <li>Title: PrepareFunction</li>
 * <li>Description: TODO </li>
 * </ul>
 *
 * @author swc
 * @date 2021-12-09 10:40
 */
public interface PrepareFunction {

    <T> boolean  hand(T t, PreparedStatement pstmt);
}
