package com.ken.sys.common.util;


import com.ken.sys.common.entity.Table;
import com.ken.sys.common.entity.TableInfo;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

public class CompareInfoUtil {

	/**
	 * 对比pdm数据库和数据库的差异 结果
	 * @param excelfilePath (pdm的excel文件路径)
	 * @param saveFolder 保存路径（文件夹）
	 * @param model 针对中文注释的匹配  true精确匹配 false模糊匹配
	 */
	public static void handScript(String excelfilePath, String saveFolder, boolean model) {
		List<Table> dbList = ReadDBTable.getAllTableInfo();
		List<Table> pdmList =null;

		try {
			pdmList = ReadExcelUtil.getInfoByExclePdm(excelfilePath);
		} catch (Exception e) {
			e.printStackTrace();
		}
		StringBuffer sbf =new StringBuffer();

		if(pdmList.size()>0 && dbList.size()>0) {

			for(int i=0;i<dbList.size();i++) {
				Table dbMapList = dbList.get(i);

				for(int k=0;k<pdmList.size();k++) {
					Table pdmMapList = pdmList.get(k);

					if(dbMapList.getName().equalsIgnoreCase(pdmMapList.getName())){
						String key =dbMapList.getName();
						String cnName =pdmMapList.getCnName();

						List<TableInfo> dbListInfo = dbMapList.getList();
						List<TableInfo> pdmListInfo =pdmMapList.getList();
						if(dbListInfo.size()!=pdmListInfo.size()) {
							sbf.append(mathching(key,cnName,dbListInfo,pdmListInfo));
						}
						for(TableInfo tdb:dbListInfo) {
							for(TableInfo pdm:pdmListInfo) {
								if(tdb.getColumn_name().equals(pdm.getColumn_name())) {//字段名一致
									if(model) {
										if(tdb.getColumn_comment()!=null &&tdb.getColumn_comment().trim().equals(pdm.getColumn_comment())) {//中文注释
											sbf.append("中文名:"+cnName+"--"+key+"表"+tdb.getColumn_name()+"--数据库里中文注释:"+tdb.getColumn_comment()+"-pdm里中文注释:"+pdm.getColumn_comment()+"\n");
										}
									}else {
										if(tdb.getColumn_comment()!=null && !tdb.getColumn_comment().contains(pdm.getColumn_comment()) ) {//中文注释
											sbf.append("中文名:"+cnName+"--"+key+"表"+tdb.getColumn_name()+"--数据库里中文注释:"+tdb.getColumn_comment()+"-pdm里中文注释:"+pdm.getColumn_comment()+"\n");
										}
										if(pdm.getColumn_comment()!=null && !pdm.getColumn_comment().contains(tdb.getColumn_comment())) {//中文注释
											sbf.append("中文名:"+cnName+"--"+key+"表"+tdb.getColumn_name()+"--数据库里中文注释:"+tdb.getColumn_comment()+"-pdm里中文注释:"+pdm.getColumn_comment()+"\n");
										}
									}

									if(tdb.getColumn_type()!=null && !tdb.getColumn_type().equals(pdm.getColumn_type())) {//字段类型
										sbf.append("中文名:"+cnName+"--"+key+"表"+tdb.getColumn_name()+"--数据库里字段类型:"+tdb.getColumn_type()+"-pdm中字段类型:"+pdm.getColumn_type()+"\n");
									}
									if(tdb.getLength()!=pdm.getLength()) {//
										sbf.append("中文名:"+cnName+"--"+key+"表"+tdb.getColumn_name()+"--数据库里字段长度:"+tdb.getLength()+"-pdm中字段长度:"+pdm.getLength()+"\n");
									}
								}

							}
						}
					}

				}
			}
		}
		SimpleDateFormat sdf =new SimpleDateFormat("YYYY-MM-DD-HH-mm-ss");
		String temp = sdf.format(new Date());

		File file =new File(saveFolder);
		if(!file.exists()) {
			file.mkdirs();
		}

		String filePath = file.getPath()+"\\"+temp+".txt";

		File fl =new File(filePath);
		if(!fl.exists()) {
			try {
				fl.createNewFile();
				BufferedWriter writer = new BufferedWriter(new FileWriter(new File(filePath)));
				writer.write(sbf.toString().replace("\n", "\r\n"));
				writer.close();
				System.out.println("输入完成，路径为："+filePath);

			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		//System.out.println(sbf.toString());
	}

	public static String mathching(String table, String cnName, List<TableInfo> dbListInfo, List<TableInfo> pdmListInfo) {
		StringBuffer sbf =new StringBuffer();
		Map<String,Boolean> dbmap =new HashMap<>();
		Map<String,Boolean> pdmap =new HashMap<>();

		for(TableInfo ti:dbListInfo) {
			dbmap.put(ti.getColumn_name(), true);
		}
		for(TableInfo ta:pdmListInfo) {
			pdmap.put(ta.getColumn_name(), true);
		}

		if(dbListInfo.size()>pdmListInfo.size()) {
			Set<String> key= dbmap.keySet();
			for(String str:key) {
				if(!pdmap.containsKey(str)) {
					sbf.append("中文名:"+cnName+"--"+table+"表,"+"pdm文件缺少字段--"+str+"\n");
				}
			}
		}

		if(dbListInfo.size()<pdmListInfo.size()) {
			Set<String> key= pdmap.keySet();
			for(String str:key) {
				if(!dbmap.containsKey(str)) {
					sbf.append("中文名:"+cnName+"--"+"表"+table+"表,"+"数据库表字段缺少-"+str+"\n");
				}
			}
		}

		return sbf.toString();
	}

}
