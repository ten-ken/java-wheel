# java-wheel
java工具类/轮子
文档描述：https://gitee.com/ten-ken/java-wheel-doc
在线版本：http://ten-ken.gitee.io/java-wheel-doc/docs/index.html

![输入图片说明](https://images.gitee.com/uploads/images/2021/0312/165727_83920123_5556017.png "屏幕截图.png")

javadoc 文档使用命令

（单个文件 第一步 使用cmd转到当前java文件的上一级目录）

1.非UTF-8的文件格式

javadoc -d  -author -version  TestB.java


2.UTF-8的文件格式
javadoc -encoding UTF-8 -d  -author -version  XX.java


（cmd命令需要转到根目录下使用）
3. 将文件夹下所有的java文件都生成文档

javadoc -encoding UTF-8 -d  -author -version  * // 相对的根目录夹是-author

javadoc -encoding UTF-8 -d  kenApi  -version api  * // 相对的根目录夹是kenApi

【上述所有的-author -version 可以忽略不写 -author一般是生成在当前目录下的-author下 】


//重点的注解
1.@link：{@link 包名.类名#方法名(参数类型)} 用于快速链接到相关代码
2. @code： {@code text} 将文本标记为code
3.@since 从以下版本开始
4.@param 一般类中支持泛型时会通过@param来解释泛型的类型
5.@value 用于标注在常量上，{@value} 用于表示常量的值 /** 默认数量 {@value} */