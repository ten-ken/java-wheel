package com.ken.echarts.common.entity.echarts.series.property;

public class SeriesItemStyle {

    //图形的颜色。
    private Object color;

    private String borderColor = "#000";

    private Number borderWidth =0;

    private String borderType ="solid";

    private Number barBorderRadius = 0 ;

    private Number  shadowBlur =0;

    private Number shadowOffsetX = 0 ;

    private Number shadowOffsetY = 0 ;

    //图形透明度。支持从 0 到 1 的数字，为 0 时不绘制该图形。
    private Number  opacity = 1 ;


    public String getBorderColor() {
        return borderColor;
    }

    public void setBorderColor(String borderColor) {
        this.borderColor = borderColor;
    }

    public Number getBorderWidth() {
        return borderWidth;
    }

    public void setBorderWidth(Number borderWidth) {
        this.borderWidth = borderWidth;
    }

    public String getBorderType() {
        return borderType;
    }

    public void setBorderType(String borderType) {
        this.borderType = borderType;
    }

    public Number getBarBorderRadius() {
        return barBorderRadius;
    }

    public void setBarBorderRadius(Number barBorderRadius) {
        this.barBorderRadius = barBorderRadius;
    }

    public Number getShadowBlur() {
        return shadowBlur;
    }

    public void setShadowBlur(Number shadowBlur) {
        this.shadowBlur = shadowBlur;
    }

    public Number getShadowOffsetX() {
        return shadowOffsetX;
    }

    public void setShadowOffsetX(Number shadowOffsetX) {
        this.shadowOffsetX = shadowOffsetX;
    }

    public Number getShadowOffsetY() {
        return shadowOffsetY;
    }

    public void setShadowOffsetY(Number shadowOffsetY) {
        this.shadowOffsetY = shadowOffsetY;
    }

    public Object getColor() {
        return color;
    }

    public void setColor(Object color) {
        this.color = color;
    }

    public Number getOpacity() {
        return opacity;
    }

    public void setOpacity(Number opacity) {
        this.opacity = opacity;
    }
}
