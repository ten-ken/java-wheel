
package com.ken.echarts.common.entity.echarts.series;

/**
 * <ul>
 * <li>Title:     -PieSeries</li>
 * <li>Description: TODO </li>
 * <li>Copyright: Copyright (c) 2018</li>
 * <li>微信:tianji_vip</li>
 * </ul>
 *
 * @author swc
 * @version
 * @date 2020/3/19 0019 上午 9:33
 */
public class PieSeries extends EchartBaseSeries {

    private String type ="pie";

    //饼图的中心（圆心）坐标，数组的第一项是横坐标，第二项是纵坐标。
    private String[] center ={"50%","50%"};

    //饼图的半径
    private String[] radius ={"0","75%"};


    public String[] getCenter() {
        return center;
    }

    public void setCenter(String[] center) {
        this.center = center;
    }

    public String[] getRadius() {
        return radius;
    }

    public void setRadius(String[] radius) {
        this.radius = radius;
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public void setType(String type) {
        this.type = type;
    }
}
