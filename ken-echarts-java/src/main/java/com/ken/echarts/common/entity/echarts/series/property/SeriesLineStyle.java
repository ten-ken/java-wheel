
package com.ken.echarts.common.entity.echarts.series.property;

/**
 * <ul>
 * <li>Title:     -SeriesLineStyle</li>
 * <li>Description: TODO </li>
 * <li>Copyright: Copyright (c) 2018</li>
 * <li>微信:tianji_vip</li>
 * </ul>
 *
 * @author swc
 * @version
 * @date 2020/3/19 0019 上午 9:37
 */
public class SeriesLineStyle {

    //默认是string
    private Object color = "#000" ;

    private Number width = 2;

    private String type ="solid";

    //实例
//    {
//        shadowColor: 'rgba(0, 0, 0, 0.5)',
//                shadowBlur: 10
//    }
    private Object shadowBlur;

    //默认是string 阴影颜色。支持的格式同color
    private Object shadowColor;

    private Number shadowOffsetX = 0 ;

    private Number shadowOffsetY = 0;

    //图形透明度。支持从 0 到 1 的数字，为 0 时不绘制该图形。
    private Number  opacity;

    public Object getColor() {
        return color;
    }

    public void setColor(Object color) {
        this.color = color;
    }

    public Number getWidth() {
        return width;
    }

    public void setWidth(Number width) {
        this.width = width;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Object getShadowBlur() {
        return shadowBlur;
    }

    public void setShadowBlur(Object shadowBlur) {
        this.shadowBlur = shadowBlur;
    }

    public Object getShadowColor() {
        return shadowColor;
    }

    public void setShadowColor(Object shadowColor) {
        this.shadowColor = shadowColor;
    }

    public Number getShadowOffsetX() {
        return shadowOffsetX;
    }

    public void setShadowOffsetX(Number shadowOffsetX) {
        this.shadowOffsetX = shadowOffsetX;
    }

    public Number getShadowOffsetY() {
        return shadowOffsetY;
    }

    public void setShadowOffsetY(Number shadowOffsetY) {
        this.shadowOffsetY = shadowOffsetY;
    }

    public Number getOpacity() {
        return opacity;
    }

    public void setOpacity(Number opacity) {
        this.opacity = opacity;
    }
}
