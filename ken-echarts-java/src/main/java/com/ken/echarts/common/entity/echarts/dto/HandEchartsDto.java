/******************************************************************************
 *
 *
 *
 ******************************************************************************
 *
 *****************************************************************************/
package com.ken.echarts.common.entity.echarts.dto;

import java.util.List;

/**
 * <ul>
 * <li>Title: HandEchartsDto</li>
 * <li>Description: TODO </li>
 * <li>Copyright: Copyright (c) 2018</li>
 * <li>微信:tianji_vip</li>
 * </ul>
 *
 * @author swc
 *
 * @date 2020/7/2 0002 下午 13:44
 */
public class HandEchartsDto {

    //    该系列使用的坐标系，可选：
//            'cartesian2d'
//    使用二维的直角坐标系（也称笛卡尔坐标系），通过 xAxisIndex, yAxisIndex指定相应的坐标轴组件。
//            'geo'
//    使用地理坐标系，通过 geoIndex 指定相应的地理坐标系组件。
//          ‘calendar’
//    使用日历坐标系
    private String coordinateSystem ="cartesian2d";

    //标题[主-列名]
    private String titleFieldName;

    //子对象的属性名[列名]
    private String subFieldName;

    //维度x轴[列名]
    private String xDimension;

    //维度y轴[列名]
    private String yDimension;

    //x轴需要展示的信息[列名]
    private String xAxisDataName;

    //y轴需要展示的信息[列名]
    private String yAxisDataName;

    //需要展示数据的列的属性名[时间列]
    private String showDateField;

    //需要展示数据的列的属性名[列名]
    private String showValField;

    //使用的日历坐标系的 index，在单个图表实例中存在多个日历坐标系的时候有用。
    private int calendarIndex;

    //legend的值
    private String[] legends;

    //一般为legend 一一对应的属性项的值
    private String[] fields;

    //一般为fields 一一对应的列的 单个serie的data里面的value值 一般为数组
    private List[] saveList;


    //是否需要x轴
    private boolean requireXAxis =true;

    //是否合并series项
//    "calender".equalsIgnoreCase(this.coordinateSystem)?true:false
    private boolean mergeSeries = false;

    //是否创建dataset下的source
    private boolean  createDatasetSource = false;

    //是否创建series下的data
    private boolean  createSeriesData = true;


    //自定义策略区分 ==》用于自定义的策略（针对覆写父类方法）
    private String  strategyKbn;


    public String getCoordinateSystem() {
        return coordinateSystem;
    }

    public void setCoordinateSystem(String coordinateSystem) {
        this.coordinateSystem = coordinateSystem;
    }

    public String getTitleFieldName() {
        return titleFieldName;
    }

    public void setTitleFieldName(String titleFieldName) {
        this.titleFieldName = titleFieldName;
    }

    public String getSubFieldName() {
        return subFieldName;
    }

    public void setSubFieldName(String subFieldName) {
        this.subFieldName = subFieldName;
    }

    public String getxDimension() {
        return xDimension;
    }

    public void setxDimension(String xDimension) {
        this.xDimension = xDimension;
    }

    public String getyDimension() {
        return yDimension;
    }

    public void setyDimension(String yDimension) {
        this.yDimension = yDimension;
    }

    public String getxAxisDataName() {
        return xAxisDataName;
    }

    public void setxAxisDataName(String xAxisDataName) {
        this.xAxisDataName = xAxisDataName;
    }

    public String getyAxisDataName() {
        return yAxisDataName;
    }

    public void setyAxisDataName(String yAxisDataName) {
        this.yAxisDataName = yAxisDataName;
    }

    public String getShowValField() {
        return showValField;
    }

    public void setShowValField(String showValField) {
        this.showValField = showValField;
    }

    public String getShowDateField() {
        return showDateField;
    }

    public void setShowDateField(String showDateField) {
        this.showDateField = showDateField;
    }

    public int getCalendarIndex() {
        return calendarIndex;
    }

    public void setCalendarIndex(int calendarIndex) {
        this.calendarIndex = calendarIndex;
    }

    public String[] getLegends() {
        return legends;
    }

    public void setLegends(String[] legends) {
        this.legends = legends;
    }

    public String[] getFields() {
        return fields;
    }

    public void setFields(String[] fields) {
        this.fields = fields;
    }

    public List[] getSaveList() {
        return saveList;
    }

    public void setSaveList(List[] saveList) {
        this.saveList = saveList;
    }

    public boolean getRequireXAxis() {
        return requireXAxis;
    }

    public void setRequireXAxis(boolean requireXAxis) {
        this.requireXAxis = requireXAxis;
    }

    public boolean getMergeSeries() {
        return mergeSeries;
    }

    public void setMergeSeries(boolean mergeSeries) {
        this.mergeSeries = mergeSeries;
    }

    public boolean getCreateDatasetSource() {
        return createDatasetSource;
    }

    public void setCreateDatasetSource(boolean createDatasetSource) {
        this.createDatasetSource = createDatasetSource;
    }

    public boolean getCreateSeriesData() {
        return createSeriesData;
    }

    public void setCreateSeriesData(boolean createSeriesData) {
        this.createSeriesData = createSeriesData;
    }

    public String getStrategyKbn() {
        return strategyKbn;
    }

    public void setStrategyKbn(String strategyKbn) {
        this.strategyKbn = strategyKbn;
    }
}
