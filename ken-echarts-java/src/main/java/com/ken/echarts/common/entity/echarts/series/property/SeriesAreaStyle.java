
package com.ken.echarts.common.entity.echarts.series.property;

/**
 * <ul>
 * <li>Title:     -SeriesAreaStyle</li>
 * <li>Description: Echarts 配置项option 里面 series里面属性 areaStyle </li>
 * <li>Copyright: Copyright (c) 2018</li>
 * <li>微信:tianji_vip</li>
 * </ul>
 *
 * @author swc
 * @version
 * @date 2020/3/15 19:56
 */
public class SeriesAreaStyle {
    //填充的颜色
    private String color;
    //图形区域的起始位置
    private String origin;
    //阴影水平方向上的偏移距离
    private Number  shadowOffsetX;

    //阴影垂直方向上的偏移距离
    private Number shadowOffsetY;

    //图形阴影的模糊大小。该属性配合 shadowColor,shadowOffsetX, shadowOffsetY 一起设置图形的阴影效果。
    private String shadowBlur;

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public Number getShadowOffsetX() {
        return shadowOffsetX;
    }

    public void setShadowOffsetX(Number shadowOffsetX) {
        this.shadowOffsetX = shadowOffsetX;
    }

    public Number getShadowOffsetY() {
        return shadowOffsetY;
    }

    public void setShadowOffsetY(Number shadowOffsetY) {
        this.shadowOffsetY = shadowOffsetY;
    }

    public String getShadowBlur() {
        return shadowBlur;
    }

    public void setShadowBlur(String shadowBlur) {
        this.shadowBlur = shadowBlur;
    }
}
