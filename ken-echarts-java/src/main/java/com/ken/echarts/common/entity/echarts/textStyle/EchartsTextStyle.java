/******************************************************************************
 *
 *
 *
 ******************************************************************************
 *
 *****************************************************************************/
package com.ken.echarts.common.entity.echarts.textStyle;

/**
 * <ul>
 * <li>Title=EchartsTextStyle</li>
 * <li>Description=TODO </li>
 * <li>Copyright=Copyright (c) 2018</li>
 * <li>Company=http://www.jiangqiaotech.com/</li>
 * </ul>
 *
 * @author swc
 *
 * @date 2020/7/3 0003 下午 14:41
 */
public class EchartsTextStyle {
    //Color
    private Object color="#fff";

    private String fontStyle="normal";

    //string | number
    private Object fontWeight;

    private String fontFamily="sans-serif";

    private Integer fontSize=12;

    private Integer lineHeight ;

    //string | number
    private Object width ;

    //string | number
    private Object  height ;

    //Color
    private Object textBorderColor="transparent";

    private Integer textBorderWidth=0;

    //Color
    private Object textShadowColor="transparent";

    private Integer textShadowBlur=0;

    private Integer textShadowOffsetX=0;

    private Integer  textShadowOffsetY=0;

    public Object getColor() {
        return color;
    }

    public void setColor(Object color) {
        this.color = color;
    }

    public String getFontStyle() {
        return fontStyle;
    }

    public void setFontStyle(String fontStyle) {
        this.fontStyle = fontStyle;
    }

    public Object getFontWeight() {
        return fontWeight;
    }

    public void setFontWeight(Object fontWeight) {
        this.fontWeight = fontWeight;
    }

    public String getFontFamily() {
        return fontFamily;
    }

    public void setFontFamily(String fontFamily) {
        this.fontFamily = fontFamily;
    }

    public Integer getFontSize() {
        return fontSize;
    }

    public void setFontSize(Integer fontSize) {
        this.fontSize = fontSize;
    }

    public Integer getLineHeight() {
        return lineHeight;
    }

    public void setLineHeight(Integer lineHeight) {
        this.lineHeight = lineHeight;
    }

    public Object getWidth() {
        return width;
    }

    public void setWidth(Object width) {
        this.width = width;
    }

    public Object getHeight() {
        return height;
    }

    public void setHeight(Object height) {
        this.height = height;
    }

    public Object getTextBorderColor() {
        return textBorderColor;
    }

    public void setTextBorderColor(Object textBorderColor) {
        this.textBorderColor = textBorderColor;
    }

    public Integer getTextBorderWidth() {
        return textBorderWidth;
    }

    public void setTextBorderWidth(Integer textBorderWidth) {
        this.textBorderWidth = textBorderWidth;
    }

    public Object getTextShadowColor() {
        return textShadowColor;
    }

    public void setTextShadowColor(Object textShadowColor) {
        this.textShadowColor = textShadowColor;
    }

    public Integer getTextShadowBlur() {
        return textShadowBlur;
    }

    public void setTextShadowBlur(Integer textShadowBlur) {
        this.textShadowBlur = textShadowBlur;
    }

    public Integer getTextShadowOffsetX() {
        return textShadowOffsetX;
    }

    public void setTextShadowOffsetX(Integer textShadowOffsetX) {
        this.textShadowOffsetX = textShadowOffsetX;
    }

    public Integer getTextShadowOffsetY() {
        return textShadowOffsetY;
    }

    public void setTextShadowOffsetY(Integer textShadowOffsetY) {
        this.textShadowOffsetY = textShadowOffsetY;
    }
}
