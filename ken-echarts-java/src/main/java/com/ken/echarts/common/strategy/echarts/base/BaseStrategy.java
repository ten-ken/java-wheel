package com.ken.echarts.common.strategy.echarts.base;

import com.ken.echarts.common.entity.echarts.axis.EchartsXYAxis;
import com.ken.echarts.common.entity.echarts.dto.HandEchartsDto;
import com.ken.echarts.common.entity.echarts.legend.EchartsLegend;
import com.ken.echarts.common.entity.echarts.option.EchartsOption;
import com.ken.echarts.common.entity.echarts.series.BarSeries;
import com.ken.echarts.common.entity.echarts.series.EchartBaseSeries;
import com.ken.echarts.common.entity.echarts.series.HeatMapSeries;
import com.ken.echarts.common.entity.echarts.series.data.SeriesData;
import com.ken.echarts.common.entity.echarts.series.property.SeriesAreaStyle;
import com.ken.echarts.common.entity.echarts.series.property.SeriesItemStyle;
import com.ken.echarts.common.entity.echarts.series.property.SeriesLabel;
import com.ken.echarts.common.entity.echarts.series.property.SeriesLineStyle;
import com.ken.echarts.common.entity.echarts.title.EchartsTitle;
import com.ken.echarts.common.ifs.IEchartSeries;
import com.ken.echarts.common.util.EmptyUtils;
import com.ken.echarts.common.util.ReflectUtils;

import java.math.BigDecimal;
import java.util.*;

/**
 * <ul>
 * <li>Title: BaseStrategy</li>
 * <li>Description: TODO </li>
 * </ul>
 *
 * @author swc
 *
 * @date 2020/7/3 0003 下午 15:57
 */
public class BaseStrategy {

    //针对line和bar图像
    public <T> EchartsOption createOptionForLineAndBar(IEchartSeries iEchartSeries,Class<? extends EchartBaseSeries> seriesClass, List<T> list, HandEchartsDto handEchartsDto){
        String xyAxisName = handEchartsDto.getxAxisDataName();

        Object[] legends = handEchartsDto.getLegends();

        Object[] fields =  handEchartsDto.getFields();

        List[] saveList = handEchartsDto.getSaveList();

        EchartsOption option =new EchartsOption();
        List<Object> xAxisDataList =new ArrayList<Object>();
        Map<String,List<Object>> map =new HashMap<String,List<Object>>();
        Number value =null;
        SeriesData data=null;
        String nameValue =null;
       try {
           for(T t:list){
               nameValue = ReflectUtils.getFieldValue(t,xyAxisName).toString();
               xAxisDataList.add(nameValue);

               for(int ind=0;ind<fields.length;ind++){
                   value = (Number) ReflectUtils.getFieldValue(t,(String)fields[ind]);
                   value = EmptyUtils.isNullOrEmpty(value)?0:value;
                   data =new SeriesData();
                   data.setValue(value);
                   //设置单个数据 样式
                   if(!EmptyUtils.isNullOrEmpty(iEchartSeries)){
                       iEchartSeries.setSeriesDataProp(data);
                   }
                   saveList[ind].add(data);
               }
           }

           //多个series
           List<EchartBaseSeries> mulseries =new ArrayList<EchartBaseSeries>();

           commonSetSeries(iEchartSeries, seriesClass, legends, fields, mulseries, saveList);

           EchartsLegend legend = new EchartsLegend();
           legend.setData(Arrays.asList(legends));

           EchartsXYAxis xAxis = new EchartsXYAxis();
           xAxis.setData(xAxisDataList);

           option.setLegend(legend);
           option.setxAxis(xAxis);
           option.setSeries(mulseries);
       }catch (Exception e){
            //实际换成日志打印
           e.printStackTrace();
       }
        return option;
    }

    //针对漏斗图、仪器图 和饼图图像
    public <T> EchartsOption createOptionFGP(IEchartSeries iEchartSeries, Class<? extends EchartBaseSeries> seriesClass, List<T> list, HandEchartsDto handEchartsDto){
        EchartsOption option =new EchartsOption();
        String xyAxisName = handEchartsDto.getxAxisDataName();

        Object[] legends = handEchartsDto.getLegends();

        Object[] fields =  handEchartsDto.getFields();

        List[] saveList = handEchartsDto.getSaveList();

        boolean requireXAxis = handEchartsDto.getRequireXAxis();

        List<Object> xAxisDataList =new ArrayList<Object>();
        Map<String,List<Object>> map =new HashMap<String,List<Object>>();
        Number value =null;
        SeriesData data=null;
        String xAxisDataValue =null;

        try {
            for(T t:list){
                xAxisDataValue = ReflectUtils.getFieldValue(t,xyAxisName).toString();
                for(int ind=0;ind<fields.length;ind++){
                    value = (Number) ReflectUtils.getFieldValue(t,(String)fields[ind]);
                    value = EmptyUtils.isNullOrEmpty(value)?0:value;

                    data =new SeriesData();
                    data.setValue(value);

                    data.setName(xAxisDataValue);
                    xAxisDataList.add(xAxisDataValue);

                    //设置单个数据 样式
                    if(!EmptyUtils.isNullOrEmpty(iEchartSeries)){
                        iEchartSeries.setSeriesDataProp(data);
                    }
                    saveList[ind].add(data);
                }
            }

            List<EchartBaseSeries> mulseries =new ArrayList<EchartBaseSeries>();
            commonSetSeries(iEchartSeries, seriesClass, legends, fields, mulseries, saveList);

            EchartsLegend legend =new EchartsLegend();
            legend.setData(Arrays.asList(legends));

            EchartsXYAxis xAxis =new EchartsXYAxis();
            xAxis.setData(xAxisDataList);

            //需要x轴
            if(requireXAxis){
                option.setxAxis(xAxis);
            }
            option.setLegend(legend);
            option.setSeries(mulseries);
        }catch (Exception e){
            e.printStackTrace();
        }
        return option;
    }





    /**
     * 功能描述:  创建热力图的option
     * @param iEchartSeries 接口
     * @param t  对象
    //     * @param titleFieldName
    //     * @param subFieldName 对象里面需要处理的list数据
    //     * @param xDimension 维度x轴
    //     * @param yDimension 维度y
    //     * @param xAxisDataName x轴需要展示的信息
    //     * @param yAxisDataName y轴需要展示的信息
    //     * @param showValField 需要展示数据的列
     * @return: java.util.Map<java.lang.String,java.lang.Object>
     * @author: swc
     * @date: 2020/7/2 0002 下午 12:52
     */
    public static <T> EchartsOption createHeatMapOption(IEchartSeries iEchartSeries, T t, HandEchartsDto handEchartsDto) {
        EchartsOption option =new EchartsOption();
        List<EchartBaseSeries> seriesList =new ArrayList<EchartBaseSeries>();

        String titleFieldName = handEchartsDto.getTitleFieldName();

        String subFieldName = handEchartsDto.getSubFieldName();

        String xDimension = handEchartsDto.getxDimension();

        String yDimension =  handEchartsDto.getyDimension();

        String xAxisDataName = handEchartsDto.getxAxisDataName();

        String yAxisDataName =  handEchartsDto.getyAxisDataName();

        String showValField =  handEchartsDto.getShowValField();

        String showDateField =  handEchartsDto.getShowDateField();

        String coordinateSystem =  handEchartsDto.getCoordinateSystem();

        Map<String,Object> map =new HashMap<String,Object>();


        //标题
        String titleName =null;
        if(!EmptyUtils.isNullOrEmpty(titleFieldName)){
            titleName =  ReflectUtils.getFieldValue(t,titleFieldName).toString();
        }

        List subList  = (List)ReflectUtils.getFieldValue(t,subFieldName);

        Set<String> xAxisData =new TreeSet<String>();

        Set<String> yAxisData =new TreeSet<String>();

        //数据值---单个数据
        List seriesDataValue ;

        EchartBaseSeries echartBaseSeries = new HeatMapSeries();

        if("calendar".equals(coordinateSystem)){
            ((HeatMapSeries)echartBaseSeries).setCoordinateSystem("calendar");
            ((HeatMapSeries)echartBaseSeries).setCalendarIndex(handEchartsDto.getCalendarIndex());
        }


        List<SeriesData> seriesDataList = new ArrayList<SeriesData>();
        SeriesData seriesData =null;

        String xAxisValue =null;
        String yAxisValue =null;
        // 维度X
        String xDim = null;
        // 维度y
        String yDim = null;
        for(int i=0;i<subList.size();i++){
            seriesData =new SeriesData();

            if(!EmptyUtils.isNullOrEmpty(xDimension)){
                // 维度X
                xDim =  ReflectUtils.getFieldValue(subList.get(i),xDimension).toString();
            }

            if(!EmptyUtils.isNullOrEmpty(xDimension)){
                // 维度y
                yDim =  ReflectUtils.getFieldValue(subList.get(i),yDimension).toString();
            }


            //x轴的值
            if(!EmptyUtils.isNullOrEmpty(xAxisDataName)){
                xAxisValue =  ReflectUtils.getFieldValue(subList.get(i),xAxisDataName).toString();
                xAxisData.add(xAxisValue);
            }

            //y轴的值
            if(!EmptyUtils.isNullOrEmpty(yAxisDataName)){
                yAxisValue =  ReflectUtils.getFieldValue(subList.get(i),yAxisDataName).toString();
                yAxisData.add(yAxisValue);
            }

            seriesDataValue =new ArrayList();

            //目前支持cartesian2d出现x-y维度
            if("cartesian2d".equals(coordinateSystem)){
                seriesDataValue.add(xDim);
                seriesDataValue.add(yDim);
            }

            //coordinateSystem为calendar series里面的data-[value]为时间+某项值的数组
            if("calendar".equals(coordinateSystem)){
                Object showDate =EmptyUtils.isNullOrEmpty(showDateField)?0:ReflectUtils.getFieldValue(subList.get(i),showDateField);
                seriesDataValue.add(showDate);
            }


            Object showVal =EmptyUtils.isNullOrEmpty(showValField)?0:ReflectUtils.getFieldValue(subList.get(i),showValField);
            seriesDataValue.add(showVal);

            seriesDataValue.add(subList.get(i));

            //设置值
            seriesData.setValue(seriesDataValue);
//            seriesData.setVisualMap(false);

            if(!EmptyUtils.isNullOrEmpty(iEchartSeries)){
                iEchartSeries.setSeriesProp(echartBaseSeries);
                iEchartSeries.setSeriesDataProp(seriesData);
            }

            seriesDataList.add(seriesData);
            echartBaseSeries.setData(seriesDataList);
        }

        seriesList.add(echartBaseSeries);
        EchartsTitle title =new EchartsTitle();
        title.setText(titleName);

        EchartsXYAxis xAxis =new EchartsXYAxis();
        xAxis.setData(new ArrayList(xAxisData));
        option.setxAxis(xAxis);

        EchartsXYAxis yAxis =new EchartsXYAxis();
        yAxis.setData(new ArrayList(yAxisData));
        option.setyAxis(yAxis);

        option.setSeries(seriesList);

        return option;
    }


    /**
     * 功能描述:  如果是cartesian2d  应该是单个
     * @param iEchartSeries 接口
     * @param list 对象
    //     * @param titleFieldName
    //     * @param subFieldName 对象里面需要处理的list数据
    //     * @param xDimension 维度x轴
    //     * @param yDimension 维度y
    //     * @param xAxisDataName x轴需要展示的信息
    //     * @param yAxisDataName y轴需要展示的信息
    //     * @param showValField 需要展示数据的列
     * @return: java.util.Map<java.lang.String,java.lang.Object>
     * @author: swc
     * @date: 2020/7/2 0002 下午 12:52
     */
    public static <T> List<EchartsOption> createHeatMapOption(IEchartSeries iEchartSeries, List<T> list, HandEchartsDto handEchartsDto) {
        List<EchartsOption> optionList =new ArrayList<EchartsOption>();
//        String coordinateSystem =  handEchartsDto.getCoordinateSystem();

        boolean mergeSeries =  handEchartsDto.getMergeSeries();

        //拼接不同年份日历的series的值
        List<EchartBaseSeries> seriesList =new ArrayList<EchartBaseSeries>();
        EchartsOption option = null;


        for(int i=0;i<list.size();i++){
            handEchartsDto.setCalendarIndex(i);
            //优先看是否合并series项内容
            if(mergeSeries){
                option = createHeatMapOption(iEchartSeries, list.get(i), handEchartsDto);
                seriesList.addAll(option.getSeries());
            }else{
                optionList.add(createHeatMapOption(iEchartSeries,list.get(i),handEchartsDto));
            }
//// 如果coordinateSystem 是 cartesian2d  默认为 多个[option]热力图
//                if("cartesian2d".equalsIgnoreCase(coordinateSystem)){
//                    optionList.add(createHeatMapOption(iEchartSeries,list.get(i),handEchartsDto));
//                }
//                //如果coordinateSystem 是 calendar  默认为 日历 则是一个option的热力图  series的值为多个 而不是一个
//                if("calendar".equalsIgnoreCase(coordinateSystem)){
//                    option = createHeatMapOption(iEchartSeries, list.get(i), handEchartsDto);
//                    seriesList.addAll(option.getSeries());
//                }
        }

        if(mergeSeries){
            option.setSeries(seriesList);
            optionList.add(option);
        }
//        if("calendar".equalsIgnoreCase(coordinateSystem)){
//            option.setSeries(seriesList);
//            optionList.add(option);
//        }

        return optionList;
    }



    //公告的设置多个series的方法
    private void commonSetSeries(IEchartSeries iEchartSeries, Class<? extends EchartBaseSeries> seriesClass, Object[] legends, Object[] fields, List<EchartBaseSeries> mulseries, List[] saveList) throws InstantiationException, IllegalAccessException {
        int xlevel1;
        BigDecimal barMaxWidth =new BigDecimal("12");

        EchartBaseSeries echartBaseSeries;
        for (int ind = 0; ind < fields.length; ind++) {
            //获取指定的series
            echartBaseSeries = seriesClass.newInstance();
            echartBaseSeries.setName((String) legends[ind]);
            echartBaseSeries.setData(saveList[ind]);

            xlevel1 = legends.length* saveList[ind].size();

            //默认超过4个进行设置
            if(echartBaseSeries instanceof BarSeries){

                if(xlevel1>=4 && xlevel1<=12){
                    barMaxWidth = new BigDecimal(String.valueOf(100/xlevel1*1.05)).setScale(1,BigDecimal.ROUND_DOWN);
                }

                if(xlevel1<4 && xlevel1>0){
                    barMaxWidth = new BigDecimal("20");
                }
                ((BarSeries) echartBaseSeries).setBarMaxWidth(barMaxWidth.toString()+"%");
            }

            //设置整体样式[如一条折线或条状图的样式]
            if (!EmptyUtils.isNullOrEmpty(iEchartSeries)) {
                iEchartSeries.setSeriesProp(echartBaseSeries);
            }
            mulseries.add(echartBaseSeries);
        }
    }



}
