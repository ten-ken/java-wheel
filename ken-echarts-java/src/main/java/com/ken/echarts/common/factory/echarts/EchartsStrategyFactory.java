/******************************************************************************
 *
 *
 *
 ******************************************************************************
 *
 *****************************************************************************/
package com.ken.echarts.common.factory.echarts;

import com.ken.echarts.common.constant.EchartsConstant;
import com.ken.echarts.common.strategy.echarts.*;
import com.ken.echarts.common.strategy.echarts.base.EchartsStrategy;

import java.util.HashMap;
import java.util.Map;

/**
 * <ul>
 * <li>Title: EchartsStrategyFactory</li>
 * <li>Description: TODO </li>
 * <li>Copyright: Copyright (c) 2018</li>
 * <li>微信:tianji_vip</li>
 * </ul>
 *
 * @author swc
 *
 * @date 2020/7/3 0003 下午 15:07
 */
public class EchartsStrategyFactory {

    public  static Map<String,EchartsStrategy> hashMap;

    static {
        hashMap = new HashMap<String,EchartsStrategy>();
        hashMap.put(EchartsConstant.LINE,new EchartsLineStrategy());
        hashMap.put(EchartsConstant.BAR,new EchartsBarStrategy());
        hashMap.put(EchartsConstant.PIE,new EchartsPieStrategy());
        hashMap.put(EchartsConstant.FUNNEL,new EchartsFunnelStrategy());
        hashMap.put(EchartsConstant.GAUGE,new EchartsGaugeStrategy());
        hashMap.put(EchartsConstant.HEATMAP,new EchartsHeatMapStrategy());
        hashMap.put(EchartsConstant.CANDLESTICK,new CandlestickStrategy());
    }

    public static EchartsStrategy getEchartsStrategy(String type) {
        return hashMap.get(type);
    }
}
