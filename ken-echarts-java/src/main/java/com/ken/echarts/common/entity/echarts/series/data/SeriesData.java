
package com.ken.echarts.common.entity.echarts.series.data;

import com.ken.echarts.common.entity.echarts.series.property.SeriesAreaStyle;
import com.ken.echarts.common.entity.echarts.series.property.SeriesItemStyle;
import com.ken.echarts.common.entity.echarts.series.property.SeriesLabel;

/**
 * <ul>
 * <li>Title:     -SeriesData</li>
 * <li>Description: TODO </li>
 * <li>Copyright: Copyright (c) 2018</li>
 * <li>微信:tianji_vip</li>
 * </ul>
 *
 * @author swc
 * @version
 * @date 2020/3/19 0019 上午 9:20
 */
public class SeriesData {

    private String name;

    //可以是数字或数组  数组对象
    private Object value;

    //设置区域【一般针对的是折线图】
    private SeriesAreaStyle areaStyle;

    //设置图形上的文本标签【条状图、折线图】
    private SeriesLabel label;

    //设置图形样式【条状图、折线图】
    private SeriesItemStyle itemStyle;

    //
    private Object visualMap;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public SeriesAreaStyle getAreaStyle() {
        return areaStyle;
    }

    public void setAreaStyle(SeriesAreaStyle areaStyle) {
        this.areaStyle = areaStyle;
    }

    public SeriesLabel getLabel() {
        return label;
    }

    public void setLabel(SeriesLabel label) {
        this.label = label;
    }

    public SeriesItemStyle getItemStyle() {
        return itemStyle;
    }

    public void setItemStyle(SeriesItemStyle itemStyle) {
        this.itemStyle = itemStyle;
    }

    public Object getVisualMap() {
        return visualMap;
    }

    public void setVisualMap(Object visualMap) {
        this.visualMap = visualMap;
    }
}
