package com.ken.echarts.common.util;
import com.ken.echarts.common.entity.echarts.dto.HandEchartsDto;
import com.ken.echarts.common.entity.echarts.option.EchartsOption;
import com.ken.echarts.common.factory.echarts.EchartsStrategyFactory;
import com.ken.echarts.common.ifs.IEchartSeries;
import com.ken.echarts.common.strategy.echarts.base.EchartsStrategy;
import com.ken.echarts.common.strategycontext.echarts.EchartStrategyContext;

import java.util.*;

public class EChartsUtil {

    /**
     * 功能描述: 
     * @param type 图形类别
     * @param iEchartSeries 外部接口 用于重写参数 或 样式等
     * @param list  数据源
     * @param handEchartsDto  需要处理的相应字段【list或list的子集里面】
     * @return: com.ken.sys.common.entity.echarts.option.EchartsOption
     * @param strategy  自定义的策略
     * @author: swc
     * @date: 2020/7/4 0:48
    */ 
    private static <T>EchartsOption creatOption(String type, IEchartSeries iEchartSeries, List<T> list, HandEchartsDto handEchartsDto, EchartsStrategy strategy) {
        //获取匹配的策略
        if(EmptyUtils.isNullOrEmpty(strategy)){
            strategy =EchartsStrategyFactory.getEchartsStrategy(type);
        }
        EchartStrategyContext context = new EchartStrategyContext(strategy);
        return context.createOption(iEchartSeries,list,handEchartsDto);
    }

    public static <T>EchartsOption creatOption(String type, List<T> list, HandEchartsDto handEchartsDto) {
        return creatOption(type,null,list,handEchartsDto,null);
    }

    public static <T>EchartsOption creatOption(String type, List<T> list, HandEchartsDto handEchartsDto,EchartsStrategy strategy) {
        return creatOption(type,null,list,handEchartsDto,strategy);
    }

    public static <T>EchartsOption creatOption(String type,IEchartSeries iEchartSeries, List<T> list, HandEchartsDto handEchartsDto) {
        return creatOption(type,iEchartSeries,list,handEchartsDto,null);
    }

    /**
     * 功能描述:
     * @param type 图形类别
     * @param iEchartSeries 外部接口 用于重写参数 或 样式等
     * @param list  数据源
     * @param handEchartsDto  需要处理的相应字段【list或list的子集里面】
     * @param strategy  自定义的策略
     * @return: com.ken.sys.common.entity.echarts.option.EchartsOption
     * @author: swc
     * @date: 2020/7/4 0:48
     */
    private static <T>List<EchartsOption> creatMulOptionForHeatMap(String type, IEchartSeries iEchartSeries, List<T> list, HandEchartsDto handEchartsDto,EchartsStrategy strategy) {
        //获取匹配的策略
        if(EmptyUtils.isNullOrEmpty(strategy)){
            strategy =EchartsStrategyFactory.getEchartsStrategy(type);
        }
        EchartStrategyContext context = new EchartStrategyContext(strategy);
        return context.createMulOption(iEchartSeries,list,handEchartsDto);
    }

    public static <T>List<EchartsOption> creatMulOptionForHeatMap(String type, IEchartSeries iEchartSeries, List<T> list, HandEchartsDto handEchartsDto) {
        return creatMulOptionForHeatMap(type,iEchartSeries,list,handEchartsDto,null);
    }


    public static <T>List<EchartsOption> creatMulOptionForHeatMap(String type, List<T> list, HandEchartsDto handEchartsDto, EchartsStrategy strategy) {
        return creatMulOptionForHeatMap(type,null,list,handEchartsDto,strategy);
    }

    public static <T>List<EchartsOption> creatMulOptionForHeatMap(String type, List<T> list, HandEchartsDto handEchartsDto) {
        return creatMulOptionForHeatMap(type,null,list,handEchartsDto,null);
    }

}
