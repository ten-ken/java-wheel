package com.ken.echarts.common.util;


/**
 * <ul>
 * <li>Title: 匠桥ERP系统-Test</li>
 * <li>Description: TODO </li>
 * <li>Copyright: Copyright (c) 2018</li>
 * <li>Company: http://www.jiangqiaotech.com/</li>
 * </ul>
 *
 * @author swc
 * @version 匠桥ERP系统V1.0
 * @date 2020/7/7 0007 下午 17:11
 */
public class Test {

    public static void main(String[] args) {
        System.out.println(test5());
    }

    public static int test5() {
        int b = 5;

        try {
            return b+10;
        } catch (Exception e) {
//            return b+15;
        } finally {
            return b+50;
        }
//        return b;
    }
}
