package com.ken.echarts.common.strategy.echarts.base;

import com.ken.echarts.common.entity.echarts.dto.HandEchartsDto;
import com.ken.echarts.common.entity.echarts.option.EchartsOption;
import com.ken.echarts.common.ifs.IEchartSeries;

import java.util.List;

/**
 * <ul>
 * <li>Title: EchartsStrategy</li>
 * <li>Description: TODO </li>
 * </ul>
 * @author swc
 * @date 2020/7/3 0003 下午 13:40
 */
public interface EchartsStrategy {
//    //创建option
    <T> EchartsOption createOption(IEchartSeries iEchartSeries, List<T> list, HandEchartsDto handEchartsDto);

    //会创建多个option
    <T>List<EchartsOption> createMulOption(IEchartSeries iEchartSeries, List<T> list, HandEchartsDto handEchartsDto);

}
