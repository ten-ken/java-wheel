/******************************************************************************
 *
 *
 *
 ******************************************************************************
 *
 *****************************************************************************/
package com.ken.echarts.common.entity.echarts.title;

/**
 * <ul>
 * <li>Title= EchartsTitle</li>
 * <li>Description= TODO </li>
 * <li>Copyright= Copyright (c) 2018</li>
 * <li>Company= http=//www.jiangqiaotech.com/</li>
 * </ul>
 *
 * @author swc
 *
 * @date 2020/7/3 0003 下午 13=54
 */
public class EchartsTitle {

    private String id  ;

    //是否显示标题组件
    private boolean show= true ;

    //主标题文本，支持使用 \n 换行。
    private String text= "" ;

    private String link= "" ;

    private String target= "blank" ;

    private Object textStyle;

    private String subtext= "" ;

    private String sublink= "" ;

    private String subtarget= "blank" ;

    private String  subtextStyle;
    //可选值："auto"、"left"、"right"、"center"。
    private String  textAlign= "auto" ;

    private String textVerticalAlign= "auto" ;

    private boolean triggerEvent= false ;

    //number /Array
    //5 or [5,10,2,20]
    private Object padding= 5 ;

    private Integer itemGap= 10 ;

    private Integer zlevel= 0 ;

    private Integer z= 2 ;

    //string | number
    private Object left= "auto" ;

    //string | number
    private Object top= "auto" ;

    //string | number
    private Object right= "auto" ;

    //string | number
    private Object bottom= "auto" ;

    //Color
    private Object backgroundColor= "transparent" ;

    //Color
    private Object borderColor= "#ccc" ;

    private Integer borderWidth= 0 ;

    private Integer  borderRadius= 0 ;

    private Integer shadowBlur ;

    //Color
    private Object  shadowColor ;

    private Integer  shadowOffsetX= 0 ;

    private Integer  shadowOffsetY= 0 ;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isShow() {
        return show;
    }

    public void setShow(boolean show) {
        this.show = show;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public Object getTextStyle() {
        return textStyle;
    }

    public void setTextStyle(Object textStyle) {
        this.textStyle = textStyle;
    }

    public String getSubtext() {
        return subtext;
    }

    public void setSubtext(String subtext) {
        this.subtext = subtext;
    }

    public String getSublink() {
        return sublink;
    }

    public void setSublink(String sublink) {
        this.sublink = sublink;
    }

    public String getSubtarget() {
        return subtarget;
    }

    public void setSubtarget(String subtarget) {
        this.subtarget = subtarget;
    }

    public String getSubtextStyle() {
        return subtextStyle;
    }

    public void setSubtextStyle(String subtextStyle) {
        this.subtextStyle = subtextStyle;
    }

    public String getTextAlign() {
        return textAlign;
    }

    public void setTextAlign(String textAlign) {
        this.textAlign = textAlign;
    }

    public String getTextVerticalAlign() {
        return textVerticalAlign;
    }

    public void setTextVerticalAlign(String textVerticalAlign) {
        this.textVerticalAlign = textVerticalAlign;
    }

    public boolean isTriggerEvent() {
        return triggerEvent;
    }

    public void setTriggerEvent(boolean triggerEvent) {
        this.triggerEvent = triggerEvent;
    }

    public Object getPadding() {
        return padding;
    }

    public void setPadding(Object padding) {
        this.padding = padding;
    }

    public Integer getItemGap() {
        return itemGap;
    }

    public void setItemGap(Integer itemGap) {
        this.itemGap = itemGap;
    }

    public Integer getZlevel() {
        return zlevel;
    }

    public void setZlevel(Integer zlevel) {
        this.zlevel = zlevel;
    }

    public Integer getZ() {
        return z;
    }

    public void setZ(Integer z) {
        this.z = z;
    }

    public Object getLeft() {
        return left;
    }

    public void setLeft(Object left) {
        this.left = left;
    }

    public Object getTop() {
        return top;
    }

    public void setTop(Object top) {
        this.top = top;
    }

    public Object getRight() {
        return right;
    }

    public void setRight(Object right) {
        this.right = right;
    }

    public Object getBottom() {
        return bottom;
    }

    public void setBottom(Object bottom) {
        this.bottom = bottom;
    }

    public Object getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(Object backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public Object getBorderColor() {
        return borderColor;
    }

    public void setBorderColor(Object borderColor) {
        this.borderColor = borderColor;
    }

    public Integer getBorderWidth() {
        return borderWidth;
    }

    public void setBorderWidth(Integer borderWidth) {
        this.borderWidth = borderWidth;
    }

    public Integer getBorderRadius() {
        return borderRadius;
    }

    public void setBorderRadius(Integer borderRadius) {
        this.borderRadius = borderRadius;
    }

    public Integer getShadowBlur() {
        return shadowBlur;
    }

    public void setShadowBlur(Integer shadowBlur) {
        this.shadowBlur = shadowBlur;
    }

    public Object getShadowColor() {
        return shadowColor;
    }

    public void setShadowColor(Object shadowColor) {
        this.shadowColor = shadowColor;
    }

    public Integer getShadowOffsetX() {
        return shadowOffsetX;
    }

    public void setShadowOffsetX(Integer shadowOffsetX) {
        this.shadowOffsetX = shadowOffsetX;
    }

    public Integer getShadowOffsetY() {
        return shadowOffsetY;
    }

    public void setShadowOffsetY(Integer shadowOffsetY) {
        this.shadowOffsetY = shadowOffsetY;
    }
}
