package com.ken.echarts.common.strategycontext.echarts;

import com.ken.echarts.common.entity.echarts.dto.HandEchartsDto;
import com.ken.echarts.common.entity.echarts.option.EchartsOption;
import com.ken.echarts.common.ifs.IEchartSeries;
import com.ken.echarts.common.strategy.echarts.base.EchartsStrategy;

import java.util.List;

/**
 * <ul>
 * <li>Title: EchartStrategyContext</li>
 * </ul>
 *
 * @author swc
 *
 * @date 2020/7/3 0003 下午 14:54
 */
public class EchartStrategyContext {

    //echart 的生成策略
    private EchartsStrategy echartsStrategy;


    public <T>EchartsOption createOption(IEchartSeries iEchartSeries, List<T> list,HandEchartsDto echartsDto){
        if (echartsStrategy != null) {
           return echartsStrategy.createOption(iEchartSeries,list,echartsDto);
        }
        return null;
    }

    //会创建多个option
    public <T> List<EchartsOption> createMulOption(IEchartSeries iEchartSeries, List<T> list,HandEchartsDto echartsDto){
        if (echartsStrategy != null) {
            return echartsStrategy.createMulOption(iEchartSeries,list,echartsDto);
        }
        return null;
    }


    public EchartStrategyContext(EchartsStrategy echartsStrategy) {
        this.echartsStrategy = echartsStrategy;
    }

    private EchartStrategyContext() {
    }

}
