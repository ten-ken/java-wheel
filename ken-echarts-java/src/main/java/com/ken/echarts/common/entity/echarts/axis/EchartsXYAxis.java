package com.ken.echarts.common.entity.echarts.axis;

import java.util.List;

/**
 * <ul>
 * <li>Title= EchartsXYAxis</li>
 * <li>Description= TODO </li>
 * <li>Copyright= Copyright (c) 2018</li>
 * <li>Company= http=//www.jiangqiaotech.com/</li>
 * </ul>
 *
 * @author swc
 *
 * @date 2020/7/3 0003 下午 14=26
 */
public class EchartsXYAxis {

    private String id ;

    private boolean  show = true ;

    private Integer gridIndex= 0 ;

    private String position  ;

    private Integer offset= 0 ;

    private String  type= "category" ;

    private String name  ;

    private String nameLocation= "end" ;

    //Object
    private Object nameTextStyle ;

    private Integer nameGap= 15 ;

    private Integer  nameRotate  ;

    private boolean inverse= false ;

    //boolean  |  Array
    private  Object  boundaryGap  ;

    //    number | string | Function
    private  Object  min  ;

    //    number | string | Function
    private  Object  max  ;


    private boolean scale= false ;


    private Integer  splitNumber= 5 ;

    private Integer minInterval= 0 ;

    private Integer maxInterval  ;

    private Integer interval  ;

    private Integer logBase= 10 ;

    private boolean  silent= false ;

    private boolean  triggerEvent= false ;

    //Object
    private Object axisLine ;

    //Object
    private Object axisTick ;

    //Object
    private Object minorTick ;

    //Object
    private Object axisLabel ;

    //Object
    private Object splitLine ;

    //Object
    private Object minorSplitLine ;

    //Object
    private Object splitArea ;

    //Array
    private List data ;

    private Object axisPointer ;

    private Integer  zlevel= 0 ;

    private Integer z= 0 ;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isShow() {
        return show;
    }

    public void setShow(boolean show) {
        this.show = show;
    }

    public Integer getGridIndex() {
        return gridIndex;
    }

    public void setGridIndex(Integer gridIndex) {
        this.gridIndex = gridIndex;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public Integer getOffset() {
        return offset;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNameLocation() {
        return nameLocation;
    }

    public void setNameLocation(String nameLocation) {
        this.nameLocation = nameLocation;
    }

    public Object getNameTextStyle() {
        return nameTextStyle;
    }

    public void setNameTextStyle(Object nameTextStyle) {
        this.nameTextStyle = nameTextStyle;
    }

    public Integer getNameGap() {
        return nameGap;
    }

    public void setNameGap(Integer nameGap) {
        this.nameGap = nameGap;
    }

    public Integer getNameRotate() {
        return nameRotate;
    }

    public void setNameRotate(Integer nameRotate) {
        this.nameRotate = nameRotate;
    }

    public boolean isInverse() {
        return inverse;
    }

    public void setInverse(boolean inverse) {
        this.inverse = inverse;
    }

    public Object getBoundaryGap() {
        return boundaryGap;
    }

    public void setBoundaryGap(Object boundaryGap) {
        this.boundaryGap = boundaryGap;
    }

    public Object getMin() {
        return min;
    }

    public void setMin(Object min) {
        this.min = min;
    }

    public Object getMax() {
        return max;
    }

    public void setMax(Object max) {
        this.max = max;
    }

    public boolean isScale() {
        return scale;
    }

    public void setScale(boolean scale) {
        this.scale = scale;
    }

    public Integer getSplitNumber() {
        return splitNumber;
    }

    public void setSplitNumber(Integer splitNumber) {
        this.splitNumber = splitNumber;
    }

    public Integer getMinInterval() {
        return minInterval;
    }

    public void setMinInterval(Integer minInterval) {
        this.minInterval = minInterval;
    }

    public Integer getMaxInterval() {
        return maxInterval;
    }

    public void setMaxInterval(Integer maxInterval) {
        this.maxInterval = maxInterval;
    }

    public Integer getInterval() {
        return interval;
    }

    public void setInterval(Integer interval) {
        this.interval = interval;
    }

    public Integer getLogBase() {
        return logBase;
    }

    public void setLogBase(Integer logBase) {
        this.logBase = logBase;
    }

    public boolean isSilent() {
        return silent;
    }

    public void setSilent(boolean silent) {
        this.silent = silent;
    }

    public boolean isTriggerEvent() {
        return triggerEvent;
    }

    public void setTriggerEvent(boolean triggerEvent) {
        this.triggerEvent = triggerEvent;
    }

    public Object getAxisLine() {
        return axisLine;
    }

    public void setAxisLine(Object axisLine) {
        this.axisLine = axisLine;
    }

    public Object getAxisTick() {
        return axisTick;
    }

    public void setAxisTick(Object axisTick) {
        this.axisTick = axisTick;
    }

    public Object getMinorTick() {
        return minorTick;
    }

    public void setMinorTick(Object minorTick) {
        this.minorTick = minorTick;
    }

    public Object getAxisLabel() {
        return axisLabel;
    }

    public void setAxisLabel(Object axisLabel) {
        this.axisLabel = axisLabel;
    }

    public Object getSplitLine() {
        return splitLine;
    }

    public void setSplitLine(Object splitLine) {
        this.splitLine = splitLine;
    }

    public Object getMinorSplitLine() {
        return minorSplitLine;
    }

    public void setMinorSplitLine(Object minorSplitLine) {
        this.minorSplitLine = minorSplitLine;
    }

    public Object getSplitArea() {
        return splitArea;
    }

    public void setSplitArea(Object splitArea) {
        this.splitArea = splitArea;
    }

    public List getData() {
        return data;
    }

    public void setData(List data) {
        this.data = data;
    }

    public Object getAxisPointer() {
        return axisPointer;
    }

    public void setAxisPointer(Object axisPointer) {
        this.axisPointer = axisPointer;
    }

    public Integer getZlevel() {
        return zlevel;
    }

    public void setZlevel(Integer zlevel) {
        this.zlevel = zlevel;
    }

    public Integer getZ() {
        return z;
    }

    public void setZ(Integer z) {
        this.z = z;
    }
}
