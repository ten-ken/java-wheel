package com.ken.echarts.common.entity.echarts.series;

import java.util.List;

/**
 * <ul>
 * <li>Title= K线图-CandlestickSeries</li>
 * <li>Description= TODO </li>
 * </ul>
 *
 * @author swc
 * @date 2020/7/6 0006 下午 14=32
 */
public class CandlestickSeries extends  EchartBaseSeries{

    //图表类型
    private String type ="candlestick";

    private String coordinateSystem = "cartesian2d" ;

    private Integer xAxisIndex= 0 ;

    private Integer yAxisIndex= 0 ;

    private boolean legendHoverLink= true ;

    private boolean hoverAnimation= true ;

    private String layout  ;

    private Integer barWidth  ;

    //number | string
    private Object  barMinWidth  ;

    //number | string
    private Object  barMaxWidth  ;

    private boolean large= true ;

    private Integer largeThreshold= 600 ;

    private Integer  progressive= 3000 ;

    private Integer progressiveThreshold= 10000 ;

    private String progressiveChunkMode= "mod" ;

    //Array
    private List dimensions  ;

    //Object
    private Object encode  ;

    private boolean clip = true;

    private Integer zlevel = 0;

    private Integer z= 2 ;

    private boolean silent= false ;

    //    number  | Function
    private Object animationDuration = 300 ;

    private String  animationEasing= "linear" ;

    //    number  | Function
    private Object  animationDelay= 0 ;

    //Object 本系列特定的 tooltip 设定。
    private Object tooltip;

    @Override
    public String getType() {
        return type;
    }

    @Override
    public void setType(String type) {
        this.type = type;
    }

    public String getCoordinateSystem() {
        return coordinateSystem;
    }

    public void setCoordinateSystem(String coordinateSystem) {
        this.coordinateSystem = coordinateSystem;
    }

    public Integer getxAxisIndex() {
        return xAxisIndex;
    }

    public void setxAxisIndex(Integer xAxisIndex) {
        this.xAxisIndex = xAxisIndex;
    }

    public Integer getyAxisIndex() {
        return yAxisIndex;
    }

    public void setyAxisIndex(Integer yAxisIndex) {
        this.yAxisIndex = yAxisIndex;
    }

    public boolean isLegendHoverLink() {
        return legendHoverLink;
    }

    public void setLegendHoverLink(boolean legendHoverLink) {
        this.legendHoverLink = legendHoverLink;
    }

    public boolean isHoverAnimation() {
        return hoverAnimation;
    }

    public void setHoverAnimation(boolean hoverAnimation) {
        this.hoverAnimation = hoverAnimation;
    }

    public String getLayout() {
        return layout;
    }

    public void setLayout(String layout) {
        this.layout = layout;
    }

    public Integer getBarWidth() {
        return barWidth;
    }

    public void setBarWidth(Integer barWidth) {
        this.barWidth = barWidth;
    }

    public Object getBarMinWidth() {
        return barMinWidth;
    }

    public void setBarMinWidth(Object barMinWidth) {
        this.barMinWidth = barMinWidth;
    }

    public Object getBarMaxWidth() {
        return barMaxWidth;
    }

    public void setBarMaxWidth(Object barMaxWidth) {
        this.barMaxWidth = barMaxWidth;
    }

    public boolean isLarge() {
        return large;
    }

    public void setLarge(boolean large) {
        this.large = large;
    }

    public Integer getLargeThreshold() {
        return largeThreshold;
    }

    public void setLargeThreshold(Integer largeThreshold) {
        this.largeThreshold = largeThreshold;
    }

    public Integer getProgressive() {
        return progressive;
    }

    public void setProgressive(Integer progressive) {
        this.progressive = progressive;
    }

    public Integer getProgressiveThreshold() {
        return progressiveThreshold;
    }

    public void setProgressiveThreshold(Integer progressiveThreshold) {
        this.progressiveThreshold = progressiveThreshold;
    }

    public String getProgressiveChunkMode() {
        return progressiveChunkMode;
    }

    public void setProgressiveChunkMode(String progressiveChunkMode) {
        this.progressiveChunkMode = progressiveChunkMode;
    }

    public List getDimensions() {
        return dimensions;
    }

    public void setDimensions(List dimensions) {
        this.dimensions = dimensions;
    }

    public Object getEncode() {
        return encode;
    }

    public void setEncode(Object encode) {
        this.encode = encode;
    }

    public boolean isClip() {
        return clip;
    }

    public void setClip(boolean clip) {
        this.clip = clip;
    }

    public Integer getZlevel() {
        return zlevel;
    }

    public void setZlevel(Integer zlevel) {
        this.zlevel = zlevel;
    }

    public Integer getZ() {
        return z;
    }

    public void setZ(Integer z) {
        this.z = z;
    }

    public boolean getSilent() {
        return silent;
    }

    public void setSilent(boolean silent) {
        this.silent = silent;
    }

    public Object getAnimationDuration() {
        return animationDuration;
    }

    public void setAnimationDuration(Object animationDuration) {
        this.animationDuration = animationDuration;
    }

    public String getAnimationEasing() {
        return animationEasing;
    }

    public void setAnimationEasing(String animationEasing) {
        this.animationEasing = animationEasing;
    }

    public Object getAnimationDelay() {
        return animationDelay;
    }

    public void setAnimationDelay(Object animationDelay) {
        this.animationDelay = animationDelay;
    }

    public Object getTooltip() {
        return tooltip;
    }

    public void setTooltip(Object tooltip) {
        this.tooltip = tooltip;
    }
}
