package com.ken.echarts.common.entity.echarts.dataset;

import java.util.List;

/**
 * <ul>
 * <li>Title: EchartsDataset</li>
 * <li>Description: TODO </li>
 * <li>Copyright: Copyright (c) 2018</li>
 * <li>微信:tianji_vip</li>
 * </ul>
 *
 * @author swc
 *
 * @date 2020/7/3 0003 下午 14:47
 */
public class EchartsDataset {
    private String id;

    //Array | Object
    private Object source;

    //Array
    private List dimensions;

    private  Boolean sourceHeader;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Object getSource() {
        return source;
    }

    public void setSource(Object source) {
        this.source = source;
    }

    public List getDimensions() {
        return dimensions;
    }

    public void setDimensions(List dimensions) {
        this.dimensions = dimensions;
    }

    public Boolean getSourceHeader() {
        return sourceHeader;
    }

    public void setSourceHeader(Boolean sourceHeader) {
        this.sourceHeader = sourceHeader;
    }
}
