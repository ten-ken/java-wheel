package com.ken.echarts.common.ifs;
import com.ken.echarts.common.entity.echarts.series.EchartBaseSeries;
import com.ken.echarts.common.entity.echarts.series.data.SeriesData;
import com.ken.echarts.common.entity.echarts.series.property.SeriesAreaStyle;
import com.ken.echarts.common.entity.echarts.series.property.SeriesItemStyle;
import com.ken.echarts.common.entity.echarts.series.property.SeriesLabel;
import com.ken.echarts.common.entity.echarts.series.property.SeriesLineStyle;

/**
 * <ul>
 * <li>Title: IEchartSeries 接口</li>
 * <li>Description: TODO </li>
 * </ul>
 *
 * @author swc
 * @version
 * @date 2020/3/15 20:53
 */
public interface IEchartSeries {
    //设置series里面的 label  itemStyle areaStyle 属性值
    //, SeriesLabel label, SeriesItemStyle itemStyle, SeriesAreaStyle areaStyle, SeriesLineStyle lineStyle
    void setSeriesProp(EchartBaseSeries series);

    //设置series--data里面的 label  itemStyle areaStyle 属性值
    //, SeriesLabel label, SeriesItemStyle itemStyle, SeriesAreaStyle areaStyle, SeriesLineStyle lineStyle
    void setSeriesDataProp(SeriesData seriesData) ;
}
