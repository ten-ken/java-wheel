
package com.ken.echarts.common.entity.echarts.series;


/**
 * <ul>
 * <li>Title:     -BarSeries</li>
 * <li>Description: TODO </li>
 * <li>Copyright: Copyright (c) 2018</li>
 * <li>微信:tianji_vip</li>
 * </ul>
 *
 * @author swc
 * @version
 * @date 2020/3/19 0019 上午 9:30
 */
public class BarSeries extends EchartBaseSeries {

    private String type ="bar";

    //number or string 不设置时为"自适应"
    private Object barWidth;

    //number or string
    private Object barMaxWidth;

    //number or string
    private Object barMinWidth;

    //number or string
    private Object barMinHeight;

    //不同系列的柱间距离，为百分比（如 '30%'，表示柱子宽度的 30%）。
    private String barGap;

    //'20%'同一系列的柱间距离
    private String barCategoryGap;

    public Object getBarWidth() {
        return barWidth;
    }

    public void setBarWidth(Object barWidth) {
        this.barWidth = barWidth;
    }

    public Object getBarMaxWidth() {
        return barMaxWidth;
    }

    public void setBarMaxWidth(Object barMaxWidth) {
        this.barMaxWidth = barMaxWidth;
    }

    public Object getBarMinWidth() {
        return barMinWidth;
    }

    public void setBarMinWidth(Object barMinWidth) {
        this.barMinWidth = barMinWidth;
    }

    public Object getBarMinHeight() {
        return barMinHeight;
    }

    public void setBarMinHeight(Object barMinHeight) {
        this.barMinHeight = barMinHeight;
    }

    public String getBarGap() {
        return barGap;
    }

    public void setBarGap(String barGap) {
        this.barGap = barGap;
    }

    public String getBarCategoryGap() {
        return barCategoryGap;
    }

    public void setBarCategoryGap(String barCategoryGap) {
        this.barCategoryGap = barCategoryGap;
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public void setType(String type) {
        this.type = type;
    }
}
