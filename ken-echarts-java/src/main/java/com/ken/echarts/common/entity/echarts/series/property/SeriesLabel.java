
package com.ken.echarts.common.entity.echarts.series.property;

/**
 * <ul>
 * <li>Description: Series的Label 属性</li>
 * @author swc
 */
public class SeriesLabel {

    private boolean show;

    //其实这个属性可以是字符串 也可以是数组 默认为字符串
    private String position ="inside";

    private String color ="auto";

    private Number distance = 5 ;

    //标签旋转。从 -90 度到 90 度。正值是逆时针 默认是不旋转
    private Number  rotate =0;

    // 是否对文字进行偏移 默认不偏移
    private int[] offset =new int[]{0,0};

    private String fontStyle ="normal";

    private String  fontWeight = "normal";

    private String fontFamily = "sans-serif";

    private Number fontSize = 12 ;

    private String align ="center";

    private String backgroundColor = "transparent";

    private Number borderWidth = 0;

    private Number borderRadius = 0;

    private Number  padding = 0;

    private String shadowColor ="transparent";

    private Number shadowBlur = 0;

    private Number shadowOffsetX = 0 ;

    private Number shadowOffsetY = 0 ;

    private String textBorderColor = "transparent";

    private Number textBorderWidth = 0 ;

    private String textShadowColor = "transparent";

    private Number textShadowBlur = 0 ;

    private Number textShadowOffsetX =  0 ;

    private Number textShadowOffsetY= 0;

    public boolean isShow() {
        return show;
    }

    public void setShow(boolean show) {
        this.show = show;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public Number getDistance() {
        return distance;
    }

    public void setDistance(Number distance) {
        this.distance = distance;
    }

    public Number getRotate() {
        return rotate;
    }

    public void setRotate(Number rotate) {
        this.rotate = rotate;
    }

    public int[] getOffset() {
        return offset;
    }

    public void setOffset(int[] offset) {
        this.offset = offset;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getFontStyle() {
        return fontStyle;
    }

    public void setFontStyle(String fontStyle) {
        this.fontStyle = fontStyle;
    }

    public String getFontWeight() {
        return fontWeight;
    }

    public void setFontWeight(String fontWeight) {
        this.fontWeight = fontWeight;
    }

    public String getFontFamily() {
        return fontFamily;
    }

    public void setFontFamily(String fontFamily) {
        this.fontFamily = fontFamily;
    }

    public Number getFontSize() {
        return fontSize;
    }

    public void setFontSize(Number fontSize) {
        this.fontSize = fontSize;
    }

    public String getAlign() {
        return align;
    }

    public void setAlign(String align) {
        this.align = align;
    }

    public String getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(String backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public Number getBorderWidth() {
        return borderWidth;
    }

    public void setBorderWidth(Number borderWidth) {
        this.borderWidth = borderWidth;
    }

    public Number getBorderRadius() {
        return borderRadius;
    }

    public void setBorderRadius(Number borderRadius) {
        this.borderRadius = borderRadius;
    }

    public Number getPadding() {
        return padding;
    }

    public void setPadding(Number padding) {
        this.padding = padding;
    }

    public String getShadowColor() {
        return shadowColor;
    }

    public void setShadowColor(String shadowColor) {
        this.shadowColor = shadowColor;
    }

    public Number getShadowBlur() {
        return shadowBlur;
    }

    public void setShadowBlur(Number shadowBlur) {
        this.shadowBlur = shadowBlur;
    }

    public Number getShadowOffsetX() {
        return shadowOffsetX;
    }

    public void setShadowOffsetX(Number shadowOffsetX) {
        this.shadowOffsetX = shadowOffsetX;
    }

    public Number getShadowOffsetY() {
        return shadowOffsetY;
    }

    public void setShadowOffsetY(Number shadowOffsetY) {
        this.shadowOffsetY = shadowOffsetY;
    }

    public String getTextBorderColor() {
        return textBorderColor;
    }

    public void setTextBorderColor(String textBorderColor) {
        this.textBorderColor = textBorderColor;
    }

    public Number getTextBorderWidth() {
        return textBorderWidth;
    }

    public void setTextBorderWidth(Number textBorderWidth) {
        this.textBorderWidth = textBorderWidth;
    }

    public String getTextShadowColor() {
        return textShadowColor;
    }

    public void setTextShadowColor(String textShadowColor) {
        this.textShadowColor = textShadowColor;
    }

    public Number getTextShadowBlur() {
        return textShadowBlur;
    }

    public void setTextShadowBlur(Number textShadowBlur) {
        this.textShadowBlur = textShadowBlur;
    }

    public Number getTextShadowOffsetX() {
        return textShadowOffsetX;
    }

    public void setTextShadowOffsetX(Number textShadowOffsetX) {
        this.textShadowOffsetX = textShadowOffsetX;
    }

    public Number getTextShadowOffsetY() {
        return textShadowOffsetY;
    }

    public void setTextShadowOffsetY(Number textShadowOffsetY) {
        this.textShadowOffsetY = textShadowOffsetY;
    }
}
