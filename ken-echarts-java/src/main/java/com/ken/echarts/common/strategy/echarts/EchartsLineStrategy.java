package com.ken.echarts.common.strategy.echarts;

import com.ken.echarts.common.entity.echarts.dto.HandEchartsDto;
import com.ken.echarts.common.entity.echarts.option.EchartsOption;
import com.ken.echarts.common.entity.echarts.series.LineSeries;
import com.ken.echarts.common.ifs.IEchartSeries;
import com.ken.echarts.common.strategy.echarts.base.BaseStrategy;
import com.ken.echarts.common.strategy.echarts.base.EchartsStrategy;

import java.util.List;

/**
 * <ul>
 * <li>Title: EchartsLineStrategy</li>
 * </ul>
 * @author swc
 * @date 2020/7/3 0003 下午 15:02
 */
public class EchartsLineStrategy extends BaseStrategy implements EchartsStrategy {

    @Override
    public <T>EchartsOption createOption(IEchartSeries iEchartSeries, List<T> list, HandEchartsDto handEchartsDto){
        return super.createOptionForLineAndBar(iEchartSeries,LineSeries.class,list,handEchartsDto);
    }

    @Override
    public <T> List<EchartsOption> createMulOption(IEchartSeries iEchartSeries, List<T> list, HandEchartsDto handEchartsDto) {
        return null;
    }

}
