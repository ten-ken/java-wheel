////import com.alibaba.fastjson.JSON;
////import com.ken.sys.common.constant.EchartsConstant;
////import com.ken.sys.common.entity.echarts.EchartsEntity;
////import com.ken.sys.common.entity.echarts.dto.HandEchartsDto;
////import com.ken.sys.common.entity.echarts.option.EchartsOption;
////import com.ken.sys.common.entity.echarts.series.BarSeries;
////import com.ken.sys.common.entity.echarts.series.EchartBaseSeries;
////import com.ken.sys.common.entity.echarts.series.data.SeriesData;
////import com.ken.sys.common.entity.echarts.series.property.SeriesAreaStyle;
////import com.ken.sys.common.entity.echarts.series.property.SeriesItemStyle;
////import com.ken.sys.common.entity.echarts.series.property.SeriesLabel;
////import com.ken.sys.common.entity.echarts.series.property.SeriesLineStyle;
////import com.ken.sys.common.ifs.IEchartSeries;
////import com.ken.sys.common.util.EChartsUtil;
////import org.junit.Test;
////
////import java.math.BigDecimal;
////import java.util.ArrayList;
////import java.util.List;
////
/////**
//// * <ul>
//// * <li>Title: -TestStrategyEcharts</li>
//// * <li>Description: 策略版的echarts </li>
//// * <li>Copyright: Copyright (c) 2018</li>
//// * <li>Company: http://www.jiangqiaotech.com/</li>
//// * </ul>
//// *
//// * @author swc
//// * @version
//// * @date 2020/3/15 21:08
//// */
////public class TestStrategyEcharts {
//
//public List<EchartsEntity> getData(){
//        List<EchartsEntity> list=new ArrayList<EchartsEntity>();//充当数据源
//        EchartsEntity entity =null;
//        //模拟的数据
//        for(int k=1;k<4;k++){
//        entity =new EchartsEntity();
//        entity.setName("測試00"+k);
//        entity.setLevel("lv"+k+1);
//        entity.setNums1(k*2+1);
//        entity.setNums2(k);
//        list.add(entity);
//        }
//        return list;
//        }
//
////柱状图 折线图
//@Test
//public void barOrLine()throws Exception{
//        String type=EchartsConstant.LINE;//BAR
//        List<EchartsEntity> list= getData();
//
//        String[] legends ={"总金额","实付金额"};
//        String[] fields ={"nums1","nums2"};
//
//        List[] saveList =new List[]{new ArrayList<BigDecimal>(),new ArrayList<BigDecimal>()};
//
//        HandEchartsDto dto =new HandEchartsDto();
//
//        dto.setxAxisDataName("name");
//        dto.setLegends(legends);
//        dto.setFields(fields);
//        dto.setSaveList(saveList);
//
//        EchartsOption option = EChartsUtil.creatOption(type, new IEchartSeries() {
//@Override
//public void setSeriesDetailProp(EchartBaseSeries series, SeriesLabel label, SeriesItemStyle itemStyle, SeriesAreaStyle areaStyle, SeriesLineStyle lineStyle) {
////                if(series instanceof BarSeries){
////                    ((BarSeries) series).setBarMaxWidth("60%");
////                }
//        }
//@Override
//public void setSeriesDataProp(SeriesData seriesData, SeriesLabel label, SeriesItemStyle itemStyle, SeriesAreaStyle areaStyle, SeriesLineStyle lineStyle) {
//
//        }
//        }, list, dto);
//        cn.hutool.json.JSONObject jsonObject =new cn.hutool.json.JSONObject(option);
//        System.out.println(jsonObject.toString());
//        }
//
//
//
////饼图
//@Test
//public void pie()throws Exception{
//        String type=EchartsConstant.PIE;
//        List<EchartsEntity> list= getData();
//
//        String[] legends ={"总金额"};
//        String[] fields ={"nums1"};
//
//        List[] saveList =new List[]{new ArrayList<BigDecimal>()};
//
//        HandEchartsDto dto =new HandEchartsDto();
//
//        dto.setxAxisDataName("name");
//        dto.setLegends(legends);
//        dto.setFields(fields);
//        dto.setSaveList(saveList);
//        dto.setRequireXAxis(false);
//
//        EchartsOption option = EChartsUtil.creatOption(type, new IEchartSeries() {
//@Override
//public void setSeriesDetailProp(EchartBaseSeries series, SeriesLabel label, SeriesItemStyle itemStyle, SeriesAreaStyle areaStyle, SeriesLineStyle lineStyle) {
//        }
//@Override
//public void setSeriesDataProp(SeriesData seriesData, SeriesLabel label, SeriesItemStyle itemStyle, SeriesAreaStyle areaStyle, SeriesLineStyle lineStyle) {
////                if(seriesData.getValue().intValue()>10 && seriesData.getValue().intValue()<=20){
////                    label.setShow(true);
////                    label.setPosition("top");
////                    seriesData.setLabel(label);
////                }
//        }
//        }, list, dto);
//        cn.hutool.json.JSONObject jsonObject =new cn.hutool.json.JSONObject(option);
//        System.out.println(jsonObject.toString());
//        }
//
//
//@Test
//public void funnel()throws Exception{
//        //注意取值 看看是百分比（一般是值乘以100加上%）
//        //"展现", "点击", "访问", "咨询"
//        String type=EchartsConstant.FUNNEL;
//        //EchartsConstant.BAR;
//        List<EchartsEntity> list=new ArrayList<EchartsEntity>();//充当数据源
//        EchartsEntity entity1 =new EchartsEntity();
//        entity1.setName("展现");
//        entity1.setNums1(40);
//        list.add(entity1);
//
//        EchartsEntity entity2 =new EchartsEntity();
//        entity2.setName("点击");
//        entity2.setNums1(20);
//        list.add(entity2);
//
//        EchartsEntity entity3 =new EchartsEntity();
//        entity3.setName("访问");
//        entity3.setNums1(80);
//        list.add(entity3);
//
//        EchartsEntity entity4 =new EchartsEntity();
//        entity4.setName("咨询");
//        entity4.setNums1(60);
//        list.add(entity4);
//
//        String[] legends ={"总金额"};
//        String[] fields ={"nums1"};
//
////        List<BigDecimal> l1 =new ArrayList<BigDecimal>();
//        List[] saveList =new List[]{new ArrayList<BigDecimal>()};
//
//        HandEchartsDto dto =new HandEchartsDto();
//
//        dto.setxAxisDataName("name");
//        dto.setLegends(legends);
//        dto.setFields(fields);
//        dto.setSaveList(saveList);
//        dto.setRequireXAxis(false);
//
//        EchartsOption option = EChartsUtil.creatOption(type, new IEchartSeries() {
//@Override
//public void setSeriesDetailProp(EchartBaseSeries series, SeriesLabel label, SeriesItemStyle itemStyle, SeriesAreaStyle areaStyle, SeriesLineStyle lineStyle) {
//        }
//@Override
//public void setSeriesDataProp(SeriesData seriesData, SeriesLabel label, SeriesItemStyle itemStyle, SeriesAreaStyle areaStyle, SeriesLineStyle lineStyle) {
//        }
//        }, list, dto);
//
//        cn.hutool.json.JSONObject jsonObject =new cn.hutool.json.JSONObject(option);
//        System.out.println(jsonObject.toString());
//        }
//
////饼图
//@Test
//public void gauge()throws Exception{
//        //注意取值 看看是百分比（一般是值乘以100加上%）
//        //"展现", "点击", "访问", "咨询"
//        String type=EchartsConstant.GAUGE;
//        //EchartsConstant.BAR;
//        List<EchartsEntity> list=new ArrayList<EchartsEntity>();//充当数据源
//        EchartsEntity entity1 =new EchartsEntity();
//        entity1.setName("车速");
//        entity1.setNums1(40);
//        list.add(entity1);
//
//        String[] legends ={"速度"};
//        String[] fields ={"nums1"};
//
//        //        List<BigDecimal> l1 =new ArrayList<BigDecimal>();
//        List[] saveList =new List[]{new ArrayList<BigDecimal>()};
//
//        HandEchartsDto dto =new HandEchartsDto();
//
//        dto.setxAxisDataName("name");
//        dto.setLegends(legends);
//        dto.setFields(fields);
//        dto.setSaveList(saveList);
//        dto.setRequireXAxis(false);
//
//
//
//        EchartsOption option = EChartsUtil.creatOption(type, new IEchartSeries() {
//@Override
//public void setSeriesDetailProp(EchartBaseSeries series, SeriesLabel label, SeriesItemStyle itemStyle, SeriesAreaStyle areaStyle, SeriesLineStyle lineStyle) {
//        series.setName("王牌车速");
//        }
//@Override
//public void setSeriesDataProp(SeriesData seriesData, SeriesLabel label, SeriesItemStyle itemStyle, SeriesAreaStyle areaStyle, SeriesLineStyle lineStyle) {
//        }
//        }, list, dto);
//
//
//        cn.hutool.json.JSONObject jsonObject =new cn.hutool.json.JSONObject(option);
//        System.out.println(jsonObject.toString());
//        }
//
//
//
////热力图  仅支持【coordinateSystem】=》 cartesian2d 和  calendar【支持多年度】
//@Test
//public void heatMapFor2D()throws Exception{
//        //数据格式 应该是 一个集合里面 有个 对象主体里面可以设置标题  对象的属性【】 是集合list 需要处理的数据  这个数据里面 能标记x y轴
//        String type = EchartsConstant.HEATMAP;
//        StringBuffer sbf =new StringBuffer();
//        sbf.append(TestData.areaStorage);
//        List<AreaStock> areaStocks = JSON.parseArray(sbf.toString(), AreaStock.class);
//
//        HandEchartsDto handEchartsDto =new HandEchartsDto();
//        handEchartsDto.setTitleFieldName("area_name");
//        handEchartsDto.setSubFieldName("storages");
//        handEchartsDto.setxDimension("col");
//        handEchartsDto.setyDimension("layer");
//        handEchartsDto.setxAxisDataName("col");
//        handEchartsDto.setyAxisDataName("layer");
////        handEchartsDto.setShowValField("warehouse_id");//主要这个需要实际值  Number类型  其他类型 展示不出数据
//
//
//        List<EchartsOption> options= EChartsUtil.creatMulOptionForHeatMap(type, new IEchartSeries() {
//@Override
//public void setSeriesDetailProp(EchartBaseSeries series, SeriesLabel label, SeriesItemStyle itemStyle, SeriesAreaStyle areaStyle, SeriesLineStyle lineStyle) {
//
//        }
//@Override
//public void setSeriesDataProp(SeriesData seriesData, SeriesLabel label, SeriesItemStyle itemStyle, SeriesAreaStyle areaStyle, SeriesLineStyle lineStyle) {
//        //做实际的业务处理【如设置正确的坐标值】
//        List<Object> list =(List<Object>)seriesData.getValue();
//        list.set(0,Integer.parseInt(list.get(0).toString())-1);
//        list.set(1,Integer.parseInt(list.get(1).toString())-1);
//        //设置默认为false
//        seriesData.setVisualMap(false);
//        //设置样式【伪代码 需要按实际的情况设置颜色】
////                String colorJson ="{\"type\":\"radial\",\"x\":0.85,\"y\":0.18,\"r\":0.1,\"colorStops\":[{\"offset\":0,\"color\":\"#2E8B57\"},{\"offset\":0.9,\"color\":\"#2E8B57\"},{\"offset\":1,\"color\":\"#2E8B57\"}],\"globalCoord\":false}";
//
//        List dataList = (List)seriesData.getValue();
//
//        //这里肯定是根据业务来的 不是随便转换的
//        Storage storage= (Storage)dataList.get(dataList.size()-1);
//
//        if(storage.getTotal_capaicty()<50){
//        itemStyle.setColor("red");
//        }else if(storage.getTotal_capaicty()<500 && storage.getTotal_capaicty()>=50){
//        itemStyle.setColor("orange");
//        }else{
//        itemStyle.setColor("green");
//        }
//
////                itemStyle.setColor(JSON.parse(colorJson));
//        itemStyle.setBorderColor("#ffffff");
//        itemStyle.setBorderWidth(1);
//        itemStyle.setOpacity(1);
//        seriesData.setItemStyle(itemStyle);
//        }
//        },areaStocks,handEchartsDto);
//
//
//        cn.hutool.json.JSONObject jsonObject =new cn.hutool.json.JSONObject(options);
//        System.out.println(jsonObject.toString());
//        }
//
//
////热力图  仅支持【coordinateSystem】=》 cartesian2d 和  calendar【支持多年度】
//@Test
//public void heatMapForCalendar()throws Exception{
//        //数据格式 应该是 一个集合里面 有个 对象主体里面可以设置标题  对象的属性【】 是集合list 需要处理的数据
//        // 这个数据里面 能标记时间信息 和 值 信息 分别使用showValField 和 showDateField 的值来反射注入值
//        String type = EchartsConstant.HEATMAP;
//
//        List<CalendarMain> calendarMainData = TestData.createCalendarData();
//
//        HandEchartsDto handEchartsDto =new HandEchartsDto();
////      handEchartsDto.setTitleFieldName("area_name");
//        handEchartsDto.setSubFieldName("dataList");
//
//        //注意需要设置这个
//        handEchartsDto.setCoordinateSystem("calendar");
//        handEchartsDto.setShowValField("value");
//
//        //注意需要设置这个
//        handEchartsDto.setShowDateField("curDateStr");
//
//
//        List<EchartsOption> options= EChartsUtil.creatMulOptionForHeatMap(type, new IEchartSeries() {
//@Override
//public void setSeriesDetailProp(EchartBaseSeries series, SeriesLabel label, SeriesItemStyle itemStyle, SeriesAreaStyle areaStyle, SeriesLineStyle lineStyle) {
//        }
//@Override
//public void setSeriesDataProp(SeriesData seriesData, SeriesLabel label, SeriesItemStyle itemStyle, SeriesAreaStyle areaStyle, SeriesLineStyle lineStyle) {
//        //做实际的业务处理【如移除最后一项的值】
//        List<Object> list =(List<Object>)seriesData.getValue();
//        list.remove(list.size()-1);
//        }
//        },calendarMainData,handEchartsDto);
//
//
//        cn.hutool.json.JSONObject jsonObject =new cn.hutool.json.JSONObject(options);
//        System.out.println(jsonObject.toString());
//        }
//
//
////测试自定义的策略
//@Test
//public void testDefined(){
//        String type=EchartsConstant.LINE;
//        List<EchartsEntity> list= getData();
//
//        String[] legends ={"总金额"};
//        String[] fields ={"nums1"};
//
//        List[] saveList =new List[]{new ArrayList<BigDecimal>()};
//
//        HandEchartsDto dto =new HandEchartsDto();
//
//        dto.setxAxisDataName("name");
//        dto.setLegends(legends);
//        dto.setFields(fields);
//        dto.setSaveList(saveList);
//
//        EchartsOption option = EChartsUtil.creatOption(type, list, dto,new OverWriteLineStrategyEcharts());
//        cn.hutool.json.JSONObject jsonObject =new cn.hutool.json.JSONObject(option);
//        System.out.println(jsonObject.toString());
//
////         java 写js  都会变成字符串 不建议这种写法
////        cn.hutool.json.JSONObject jsonObject =new cn.hutool.json.JSONObject();
////        jsonObject.put("formatter","function test(val){console.log(val)}");
////        System.out.println(jsonObject.toString());
//
//        /**********js部分*********/
////        var data = {"formatter":eval('('+"function test(val){console.log(val)}"+')')};
////        data.formatter(88);
//        }
//
//
////测试K线图
//@Test
//public void testCandlestick(){
//        String type=EchartsConstant.CANDLESTICK;
//        List<KData> list= TestData.getKdata();
//        HandEchartsDto dto =new HandEchartsDto();
//
//        dto.setxAxisDataName("curDate");
//        dto.setLegends(new String[]{"日K", "MA5", "MA10", "MA20", "MA30"});
//        dto.setFields(new String[]{"openValue","closeValue","lowestValue","highestValue"});
//
////        dto.setCreateDatasetSource(true);//数据源放在dataset上
////        dto.setCreateSeriesData(false);//数据源不放在series上
//
//        EchartsOption option = EChartsUtil.creatOption(type, new IEchartSeries() {
//@Override
//public void setSeriesDetailProp(EchartBaseSeries series, SeriesLabel label, SeriesItemStyle itemStyle, SeriesAreaStyle areaStyle, SeriesLineStyle lineStyle) {
//
//
//        }
//
//@Override
//public void setSeriesDataProp(SeriesData seriesData, SeriesLabel label, SeriesItemStyle itemStyle, SeriesAreaStyle areaStyle, SeriesLineStyle lineStyle) {
//
//        }
//        }, list, dto);
//        cn.hutool.json.JSONObject jsonObject =new cn.hutool.json.JSONObject(option);
//        System.out.println(jsonObject.toString());
//        }
////}
